package com.howlingwolf.world;

// Java imports
import java.awt.Graphics2D;
import java.awt.Color;

// Core imports
import com.howlingwolf.util.HWExtraTypes.InteractableSide;

// Event imports
import com.howlingwolf.event.HWEventManager;
import com.howlingwolf.event.HWMessageEvent;

// Graphics imports
import com.howlingwolf.graphics.HWSprite;

// Physics imports
import com.howlingwolf.util.math.HWVec2;
import com.howlingwolf.physics.modules.HWPhysicsShape;

// Math imports
import com.howlingwolf.util.math.HWRectangle;

/**
 * Models behaviour for an object that can be interacted with.
 * 
 * @author Dylan Sawyer
 */
public class HWMessageObject extends HWAreaObject
{
	private HWDisplayMessage[] m_Interactions;
	
	// Constants for array access
	private static final int SIDE_TOP = 0;
	private static final int SIDE_LEFT = 1;
	private static final int SIDE_RIGHT = 2;
	private static final int SIDE_BOTTOM = 3;
	private static final int SIDE_ANY = 4;
	
	// Sentinel value for an invalid side
	private static final int SIDE_INVALID = -1;
	
	private static final int DEFAULT_WIDTH = 32;
	private static final int DEFAULT_HEIGHT = 32;
	
	/**
	 * Creates a new, empty MessageObject that is nowhere.
	 */
	public HWMessageObject()
	{
		this(0, 0);
	}
	
	/**
	 * Creates a new MessageObject that is at the given location with a
	 * default size.
	 * 
	 * @param x GetX position of the MessageObject
	 * @param y GetY position of the MessageObject
	 */
	public HWMessageObject(int x, int y)
	{
		this(x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT, null);
	}
	
	/**
	 * Creates a new MessageObject that is at the given location with the
	 * given size.
	 * 
	 * @param x GetX position of the MessageObject
	 * @param y GetY position of the MessageObject
	 * @param width The width of the MessageObject
	 * @param height The height of the MessageObject
	 * @param p_sprite The image that represents the MessageObject 
	 */
	public HWMessageObject(int x, int y, int width, int height, HWSprite p_sprite)
	{
		super(p_sprite, new HWPhysicsShape(new HWRectangle(x, y, width, height), null, false), new HWVec2());
		m_Interactions = new HWDisplayMessage[5];
	}
	
	/**
	 * Creates a new MessageObject that is at the given location.
	 * 
	 * @param pos The central position of the object
	 */
	public HWMessageObject(HWVec2 pos)
	{
		this(pos.GetXInt(), pos.GetYInt());
	}
	
	/**
	 * Creates a new HWMessageObject within the world.
	 * 
	 * @param p_sprite	The sprite for the object
	 * @param p_shape	The physical bounds of the object
	 * @param p_offset	The offset between the physical bounds and the sprite
	 */
	public HWMessageObject(HWSprite p_sprite, HWPhysicsShape p_shape, HWVec2 p_offset)
	{
		super(p_sprite, p_shape, p_offset);
		m_Interactions = new HWDisplayMessage[5];
	}
	
	/**
	 * Gets a copy of this HWMessageObject.
	 * 
	 * @return An exact duplicate of this object
	 */
	@Override
	public HWMessageObject Clone()
	{
		HWMessageObject obj = new HWMessageObject(GetSprite(), GetPhysicsShape(), GetOffset());
		
		for (int i = 0; i < 5; i+=1)
		{
			obj.AddInteraction(m_Interactions[i], i);
		}
		
		obj.Init();
		
		return obj;
	}
	
	/**
	 * Converts an InteractableSide into an int value for use with the array.
	 * 
	 * @param side The InteractableSide whose int value is desired
	 * @return	   The int value of the InteractableSide
	 */
	private int ConvertSideToInt(InteractableSide side)
	{
		switch (side)
		{
			case TOP:
				return SIDE_TOP;
			case RIGHT:
				return SIDE_RIGHT;
			case LEFT:
				return SIDE_LEFT;
			case BOTTOM:
				return SIDE_BOTTOM;
			case ANY:
				return SIDE_ANY;
			default:
				return SIDE_INVALID;
		}
	}
	
	/**
	 * Adds a new HWDisplayMessage to the HWMessageObject on a given side. If
 there is already an HWDisplayMessage on the given side, it is overwritten.
	 * 
	 * @param message	The message for the interaction
	 * @param side		The side the interaction is available
	 */
	public void AddInteraction(String message, InteractableSide side)
	{
		int sideInt = ConvertSideToInt(side);
		if (sideInt != SIDE_INVALID)
		{
			m_Interactions[sideInt] = new HWDisplayMessage(message);
		}
	}
	
	/**
	 * Adds a new HWDisplayMessage to the HWMessageObject on a given side. If
 there is already an HWDisplayMessage on the given side, it is overwritten.
	 * 
	 * @param message	The message to be displayed
	 * @param side		The side the interaction is available
	 */
	public void AddInteraction(HWDisplayMessage message, InteractableSide side)
	{
		int sideInt = ConvertSideToInt(side);
		if (sideInt != SIDE_INVALID)
		{
			m_Interactions[sideInt] = message.Clone();
		}
	}
	
	private void AddInteraction(HWDisplayMessage message, int side)
	{
		if (side != SIDE_INVALID)
		{
			if (message != null)
			{
				m_Interactions[side] = message.Clone();
			}
		}
	}
	
	/**
	 * Gets the interaction for the given side. If there is no particular
	 * interaction for the side, but there is one for the ANY_SIDE, then that
	 * one is returned.
	 * 
	 * @param side The side from which to get the interaction
	 * @return The interaction available to the given side, null if there is no
	 *		   interaction available or the side is outside the available values
	 */
	public HWDisplayMessage GetInteraction(InteractableSide side)
	{
		int sideInt = ConvertSideToInt(side);
		if (sideInt != SIDE_INVALID)
		{
			// Return the ANY_SIDE interaction if there isn't a particular side
			// interaction
			if (m_Interactions[sideInt] == null)
			{
				return m_Interactions[SIDE_ANY];
			}

			// Return the given side interaction
			return m_Interactions[sideInt];
		}
		
		return null;
	}
	
	
	@Override
	public String GetIdentifier()
	{
		return "";
	}
	
	@Override
	public void PerformInteraction(InteractableSide side)
	{
		HWDisplayMessage message = GetInteraction(side);
		
		if (message != null)
		{
			HWEventManager.SharedManager().AddEvent(new HWMessageEvent(message.GetMessages()));
		}
	}
	
	@Override
	public void DrawHitbox(Graphics2D graphics)
	{
		graphics.setColor(Color.CYAN);
		super.DrawHitbox(graphics);
	}
}