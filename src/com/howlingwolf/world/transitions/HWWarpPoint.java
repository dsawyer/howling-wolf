package com.howlingwolf.world.transitions;

// Math imports
import com.howlingwolf.util.math.HWVec2;
import com.howlingwolf.util.math.HWPolygon;

/**
 * Container for information for going from one Area to another.
 * 
 * @author Dylan Sawyer
 */
public class HWWarpPoint extends HWTransition
{
	private final HWVec2 m_DestinationPosition;
	
	/**
	 * Creates a new warp point.
	 * 
	 * @param p_shape	The activation hit box for this transition
	 * @param p_dest	The destination for this warp
	 * @param p_desLoc	The location where the warp connects to
	 */
	public HWWarpPoint(HWPolygon p_shape, String p_dest, HWVec2 p_desLoc)
	{
		SetShape(p_shape.Clone());
		SetDestination(p_dest);
		m_DestinationPosition = p_desLoc.Clone();
	}
	
	/**
	 * Gets a copy of this HWWarpPoint.
	 * 
	 * @return An exact duplicate of this HWWarpPoint
	 */
	@Override
	public HWWarpPoint Clone()
	{
		return new HWWarpPoint(GetShape(), GetDestination(), m_DestinationPosition);
	}
	
	/**
	 * Gets the location to which this HWWarpPoint connects.
	 * 
	 * @return The location where the one passing through lands
	 */
	public HWVec2 GetDestinationPosition()
	{
		return m_DestinationPosition;
	}
	
	/**
	 * Sets the location to which this transition connects.
	 * 
	 * @param pos	The target location
	 */
	public void SetDestinationPosition(HWVec2 pos)
	{
		m_DestinationPosition.Set(pos);
	}
}