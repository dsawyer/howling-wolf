package com.howlingwolf.world.transitions;

// Math imports
import com.howlingwolf.util.math.HWPolygon;

// Java imports
import java.awt.Graphics2D;

/**
 * Super class for all types of transitions.
 * 
 * @author Dylan Sawyer
 */
public abstract class HWTransition
{
	/**
	 * The identifier for the level to which the transition is going.
	 */
	private String m_sDestination;
	
	/**
	 * The activation hit box for the transition.
	 */
	private HWPolygon m_Shape;
	
	/**
	 * Creates a copy of the HWTransition. Subclasses must override.
	 * 
	 * @return A copy of the HWTransition
	 */
	public abstract HWTransition Clone();
	
	/**
	 * Gets the identifier for the destination area.
	 * 
	 * @return The destination's identifier
	 */
	public String GetDestination()
	{
		return m_sDestination;
	}
	
	/**
	 * Sets the name of the destination of this transition.
	 * 
	 * @param dest	The name of this transition's destination
	 */
	public void SetDestination(String dest)
	{
		m_sDestination = dest;
	}
	
	/**
	 * Gets the activation hit box for this HWTransition.
	 * 
	 * @return The hit box needed to be entered to trigger
	 */
	public HWPolygon GetShape()
	{
		return m_Shape;
	}
	
	/**
	 * Sets the collision detection shape for this transition.
	 * 
	 * @param shape	The new shape for detection
	 */
	public void SetShape(HWPolygon shape)
	{
		m_Shape = shape;
	}
	
	/**
	 * Draws the hit box for the transition.
	 * 
	 * @param graphics The graphics context onto which the hit box is drawn
	 */
	public void Draw(Graphics2D graphics)
	{
		m_Shape.Draw(graphics);
	}
}