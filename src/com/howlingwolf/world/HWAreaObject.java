package com.howlingwolf.world;

// Java imports
import java.awt.Graphics2D;

//Scene imports
import com.howlingwolf.scene.HWSceneObject;

// Core imports
import com.howlingwolf.util.HWExtraTypes.InteractableSide;

// Graphics imports
import com.howlingwolf.graphics.HWSprite;
import com.howlingwolf.graphics.HWLightSource;

// Physics imports
import com.howlingwolf.physics.modules.HWPhysicsBody;
import com.howlingwolf.physics.modules.HWPhysicsShape;

// Math imports
import com.howlingwolf.util.math.HWVec2;
import com.howlingwolf.util.math.HWMathFunctions;
import com.howlingwolf.util.math.HWBoundingBox;

/**
 * An abstract class that defines basic functionality and interface to all the
 * types of objects that will be in an Area.
 * 
 * @author Dylan Sawyer
 */
public abstract class HWAreaObject implements HWSceneObject, HWPhysicsBody, HWLightSource
{
	// The sprite used to display the object
	private HWSprite m_Sprite;
	// The physical bounds of the object
	//private HWHitbox m_Hitbox;
	// The difference between the physical bounds' position and
	// the position of the sprite
	private HWVec2 m_SpriteOffset;
	
	private HWPhysicsShape m_PhysicsComponent;
	
	/**
	 * Creates a new empty area object.
	 */
	public HWAreaObject()
	{
		this(new HWSprite(), new HWPhysicsShape(null, false), new HWVec2());
	}
	
	/**
	 * Creates a new area object with the given pieces.
	 * 
	 * @param p_sprite	The sprite used by the object
	 * @param p_shape	The physical information for the object
	 * @param p_offset	The offset between the physical shape and the sprite
	 */
	public HWAreaObject(HWSprite p_sprite, HWPhysicsShape p_shape, HWVec2 p_offset)
	{
		m_Sprite = p_sprite.Clone();
		m_PhysicsComponent = p_shape.Clone();
		m_SpriteOffset = p_offset.Clone();
		
		if (p_shape.GetShape() != null)
		{
			SetPosition(m_PhysicsComponent.GetShape().GetPosition());
		}
	}
	
	/**
	 * Creates an exact duplicate of the area object.
	 * 
	 * @return	A copy of this area object
	 */
	public abstract HWAreaObject Clone();
	
	@Override
	public abstract String GetIdentifier();
	
	/**
	 * Initialises anything this object may need.
	 */
	public void Init()
	{
		m_PhysicsComponent.SetParent(this);
		AddListeners();
	}
	
	/**
	 * Adds whatever listeners need to be added to avoid leaking.
	 */
	public void AddListeners()
	{
		// Do nothing, override in sub classes
	}
	
	/**
	 * Removes the listeners from this object.
	 */
	public void RemoveListeners()
	{
		// Do nothing, override in sub classes
	}
	
	/**
	 * Checks to see if a given point is interacting with this object.
	 * 
	 * @param vec	The point trying to interact with this object
	 * @return		True if there was an interaction, false otherwise
	 */
	public boolean DidInteract(HWVec2 vec)
	{
		return m_PhysicsComponent.GetShape().ContainsPoint(vec);
	}
	
	/**
	 * Processes an interaction if it is available.
	 * 
	 * @param side	The side the interaction takes place
	 */
	public void PerformInteraction(InteractableSide side) {}
	
	/**
	 * Sets the position of the object. The position is used as-is by the
	 * physics shape, and is offset for the sprite.
	 * 
	 * @param vec	The new position of the object
	 */
	public final void SetPosition(HWVec2 vec)
	{
		HWVec2 dist = HWMathFunctions.SubtractVectors(vec, m_PhysicsComponent.GetShape().GetPosition());
		m_PhysicsComponent.Move(dist);
		m_Sprite.SetPosition(HWMathFunctions.AddVectors(vec, m_SpriteOffset));
	}
	
	/**
	 * Sets the position of the object. The position is used as-is by the
	 * physics shape, and is offset for the sprite.
	 * 
	 * @param x	The new x position of the object
	 * @param y	The new y position of the object
	 */
	public final void SetPosition(float x, float y)
	{
		SetPosition(new HWVec2(x, y));
	}
	
	/**
	 * Gets the position of the object.
	 * 
	 * @return	The position (upper left corner) of the object's physical shape
	 */
	public HWVec2 GetPosition()
	{
		return m_PhysicsComponent.GetShape().GetPosition();
	}
	
	@Override
	public HWPhysicsShape GetPhysicsShape()
	{
		return m_PhysicsComponent;
	}
	
	/**
	 * Sets the physics shape used by the object.
	 * 
	 * @param shape	The new physics shape to be used by the object
	 */
	public void SetPhysicsShape(HWPhysicsShape shape)
	{
		m_PhysicsComponent = shape;
	}
	
	/**
	 * Finds the physical centre of the shape. This is just an exceedingly rough
	 * method to get the centre of the bounding box for the object's physical
	 * shape.
	 * 
	 * @return	The centre of the object's physical bounding box
	 */
	public HWVec2 FindPhysicalCentre()
	{
		return m_PhysicsComponent.GetShape().GetAABB().FindCentre();
	}
	
	/**
	 * Gets the object's sprite.
	 * 
	 * @return	The sprite that is used by the object
	 */
	public HWSprite GetSprite()
	{
		return m_Sprite;
	}
	
	/**
	 * Gets the bounding box for the sprite
	 * @return
	 */
	public HWBoundingBox GetSpriteBounds()
	{
		return new HWBoundingBox(m_Sprite.GetPosition().GetX(), m_Sprite.GetPosition().GetY(),
								 m_Sprite.GetWidth(), m_Sprite.GetHeight());
	}
	
	/**
	 * Gets the offset between the physical origin and the sprite's origin.
	 * 
	 * @return	The distance between the upper left corner of the object's
	 *			physical bounding box and the upper left corner of the object's
	 *			sprite.
	 */
	public HWVec2 GetOffset()
	{
		return m_SpriteOffset;
	}
	
	/**
	 * Sets the offset between the physical origin and the sprite's origin.
	 * 
	 * @param offset	The new distance between the upper left corner of the
	 *					object's physical bounding box and the upper left corner
	 *					of the object's sprite.
	 */
	public void SetOffset(HWVec2 offset)
	{
		m_SpriteOffset = offset;
	}
	
	@Override
	public void CollidedWith(HWPhysicsBody body, HWVec2 depth)
	{
		// Do nothing
	}
	
	@Override
	public void StaticCollisionWith(HWPhysicsShape shape, HWVec2 depth)
	{
		// Do nothing
	}
	
	@Override
	public void Draw(Graphics2D graphics)
	{
		m_Sprite.Draw(graphics);
	}
	
	/**
	 * Draws the bounding box around the object's physical component.
	 * 
	 * @param graphics	The graphics context being drawn upon
	 */
	public void DrawHitbox(Graphics2D graphics)
	{
		m_PhysicsComponent.GetShape().Draw(graphics);
	}
	
	@Override
	public void Update(long p_lElapsedTime)
	{
		m_Sprite.Update(p_lElapsedTime);
	}
	
	//**************************************************************************
	// HWLightSource methods
	//**************************************************************************

	@Override
	public float GetEffectiveDistance()
	{
		return 0;
	}

	@Override
	public float GetLightStrength()
	{
		return 0;
	}

	@Override
	public boolean IsActiveLightSource()
	{
		return false;
	}
}