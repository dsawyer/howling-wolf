package com.howlingwolf.world;

// Java imports
import java.util.ArrayList;

/**
 * Provides message data for displaying in a window.
 * 
 * @author Dylan Sawyer
 */
public class HWDisplayMessage
{
	// All messages that appear with the interaction
	private final ArrayList<String> m_Messages;
	
	/**
	 * Creates a new, blank DisplayMessage object.
	 */
	public HWDisplayMessage()
	{
		m_Messages = new ArrayList<>();
	}

	/**
	 * Creates a new DisplayMessage object that will only display a message.
	 * 
	 * @param message 
	 */
	public HWDisplayMessage(String message)
	{
		m_Messages = new ArrayList<>();
		m_Messages.add(message);
	}
	
	/**
	 * Creates a new DisplayMessage object that will only display a set of messages.
	 * 
	 * @param messages
	 */
	public HWDisplayMessage(ArrayList<String> messages)
	{
		m_Messages = new ArrayList<>();
		m_Messages.addAll(messages);
	}
	
	/**
	 * Returns a duplicate of the current interaction.
	 * 
	 * @return A copy of this interaction
	 */
	public HWDisplayMessage Clone()
	{
		return new HWDisplayMessage(m_Messages);
	}
	
	/**
	 * Adds a message to the list of messages that is used with this HWDisplayMessage.
	 * 
	 * @param message The message to add to the list
	 */
	public void AddMessage(String message)
	{
		m_Messages.add(message);
	}
	
	/**
	 * Removes the message at the given index.
	 * 
	 * @param index The index of the message to be removed
	 */
	public void RemoveMessage(int index)
	{
		m_Messages.remove(index);
	}
	
	/**
	 * Gets all the messages for this interaction.
	 * 
	 * @return An ArrayList of all the messages used by this interaction
	 */
	public ArrayList<String> GetMessages()
	{
		return m_Messages;
	}
}
