package com.howlingwolf.world;

// Java imports
import java.util.Comparator;

/**
 * This is just used to be able to take advantage of Java's native sorting
 * methods. It provides an interface for sorting area objects based on their
 * position.
 * 
 * @author Dylan Sawyer
 */
public class HWAreaObjectComparator implements Comparator<HWAreaObject>
{
	@Override
	public int compare(HWAreaObject aObject1, HWAreaObject aObject2)
	{
		// If object1 is behind the object2
		if (aObject1.GetPosition().GetY() < aObject2.GetPosition().GetY())
		{
			return -1;
		}
		// If the object2 is behind object1
		else if (aObject1.GetPosition().GetY() > aObject2.GetPosition().GetY())
		{
			return 1;
		}
		else // Both objects are on the same y axis
		{
			// Is object1 is to the left
			if (aObject1.GetPosition().GetX() < aObject2.GetPosition().GetX())
			{
				return -1;
			}
			// If object1 is to the right
			else if (aObject1.GetPosition().GetX() > aObject2.GetPosition().GetX())
			{
				return 1;
			}
		}
		
		return 0;
	}
}
