package com.howlingwolf.util;

/**
 * Container class for a variety of enums used in the engine.
 * 
 * @author Dylan Sawyer
 */
public abstract class HWExtraTypes
{
	/**
	 * Enum for the sides that can be interacted with on an InteractableObject.
	 */
	public enum InteractableSide
	{
		/**
		 * The top side of the object.
		 */
		TOP,
		/**
		 * The right side of the object.
		 */
		RIGHT,
		/**
		 * The left side of the object.
		 */
		LEFT,
		/**
		 * The bottom side of the object.
		 */
		BOTTOM,
		/**
		 * Any side of the object.
		 */
		ANY
	}
	
	/**
	 * Enum for the types of collisions a Tile object can have.
	 * May add a hit box to the Tile Data, which would be grabbed if the tile
	 * is a solid one (would allow for different sizes).
	 */
	public enum CollisionType
	{
		/**
		 * The tile can walked upon.
		 */
		PASSABLE,
		/**
		 * The tile is completely solid.
		 */
		SOLID
	}
	
	/**
	 * Enum used for directional information
	 */
	public enum Direction
	{

		/**
		 * Towards negative Y
		 */
		UP,

		/**
		 * Towards positive Y
		 */
		DOWN,

		/**
		 * Towards negative X
		 */
		LEFT,

		/**
		 * Towards positive X
		 */
		RIGHT
	}
	
	/**
	 * Identifier for a sound effect.
	 */
	public static final String SOUND_CURSOR = "cursor2";
	/**
	 * Identifier for a sound effect.
	 */
	public static final String SOUND_SELECT = "choice2";
	/**
	 * Identifier for a sound effect.
	 */
	public static final String SOUND_DIALOG = "cursor1";
	
	/**
	 * Identifier for a background music.
	 */
	public static final String BGM_FIELD1 = "uga_buga";
}