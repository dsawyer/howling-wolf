package com.howlingwolf.util;

// Java imports
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

// Graphics imports
import com.howlingwolf.graphics.tileEngine.HWTileset;

// Physics imports
import com.howlingwolf.util.math.HWVec2;

/**
 * A collection of extra mathematical methods used by the game engine.
 * <p>
 * Also includes some Image manipulation methods.
 * 
 * @author Dylan Sawyer
 */
public final class HWExtraFunctions
{
    //*************************************************************************
    // Methods
    //*************************************************************************
    
	// Private constructor, so there is no implicit one. Is never called.
    private HWExtraFunctions()
    { throw new AssertionError(); }

    // Functions for switching between pixel and tile systems
    
	/**
	 * Converts a set of coordinates from Tile space to Pixel space.
	 * 
	 * @param x The GetX coordinate in Tile space
	 * @param y The y coordinate in Tile space
	 * @return A {@link Vector2DI} object representing the coordinates in Pixel space
	 */
	public static HWVec2 TilesToPixels(int x, int y)
    {
        return new HWVec2(x * HWTileset.TILE_WIDTH, y * HWTileset.TILE_HEIGHT);
    }
	
	/**
	 * Converts a set of coordinates from Tile space to Pixel space.
	 * 
	 * @param point The coordinates in Tile space
	 * @return A {@link Vector2DI} object representing the coordinates in Pixel space
	 */
	public static HWVec2 TilesToPixels(HWVec2 point)
	{
		return TilesToPixels(point.GetXInt(), point.GetYInt());
	}
	
	/**
	 * Converts a set of coordinates from Pixel space to Tile space
	 * 
	 * @param x The GetX coordinate in Pixel space
	 * @param y The y coordinate in Pixel space
	 * @return A {@link HWVec2} object representing the coordinates in Tile space
	 */
	public static HWVec2 PixelsToTiles(float x, float y)
    {
        int newX = (int)Math.floor(x / HWTileset.TILE_WIDTH);
		int newY = (int)Math.floor(y / HWTileset.TILE_HEIGHT);
		
		return new HWVec2(newX, newY);
    }
	
	/**
	 * Converts a set of coordinates from Pixel space to Tile space.
	 * 
	 * @param point The coordinates in Pixel space
	 * @return A {@link Vector2DI} object representing the coordinates in Tile space
	 */
	public static HWVec2 PixelsToTiles(HWVec2 point)
	{
		return PixelsToTiles(point.GetX(), point.GetY());
	}

    /**
	 * Forces a value to be within the given bounds.
	 * <p>
	 * If it is greater than the given maximum, the maximum value of the bounds
	 * is returned.
	 * <p>
	 * If it is less than the given minimum, the minimum value of the bounds is
	 * returned.
	 * 
	 * @param value The value to check
	 * @param min The minimum value of the bounds
	 * @param max The maximum value of the bounds
	 * @return The now clamped value
	 */
	public static int Clamp(int value, int min, int max)
    {
            // If it is lower than the minimum value
            if (value < min)
            {
                    return min;
            }

            // If it is greater than the maximum value
            if (value > max)
            {
                    return max;
            }

            return value;
    }

    /**
	 * Forces a value to be within the given bounds.
	 * <p>
	 * If it is greater than the given maximum, the maximum value of the bounds
	 * is returned.
	 * <p>
	 * If it is less than the given minimum, the minimum value of the bounds is
	 * returned.
	 * 
	 * @param value The value to check
	 * @param min The minimum value of the bounds
	 * @param max The maximum value of the bounds
	 * @return The now clamped value
	 */
	public static float Clamp(float value, float min, float max)
    {
            // If it is lower than the minimum value
            if (value < min)
            {
                    return min;
            }

            // If it is greater than the maximum value
            if (value > max)
            {
                    return max;
            }

            return value;
    }
	
	public static final byte[] GetColourBytesFromARGBInt(int p_iARGB)
	{
		return new byte[] { (byte)((p_iARGB >>> 24) ),
							(byte)((p_iARGB >>> 16) ),
							(byte)((p_iARGB >>> 8) ),
							(byte)((p_iARGB) ) };
	}
	
	/**
	 * Creates a scaled instance of the given image. Far more efficient than
	 * using Image.getScaledImage.
	 * 
	 * @param image  The source image to be scaled
	 * @param width  The width of the scaled instance
	 * @param height The height of the scaled instance
	 * @return A scaled BufferedImage
	 */
	public static BufferedImage CreateScaledImage(BufferedImage image, int width, int height)
	{
		BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D scaledGraphics = scaledImage.createGraphics();
		// Uncomment this to add interpolation
		//scaledGraphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		// Draws the source image to the given rectangle, scaling if need be
		scaledGraphics.drawImage(image, 0, 0, width, height, null);
		scaledGraphics.dispose();
		return scaledImage;
	}
}