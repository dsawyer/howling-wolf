package com.howlingwolf.util.xml;

// Java imports
import java.util.Scanner;
import java.awt.image.BufferedImage;

// Org imports
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

// Core imports
import com.howlingwolf.managers.HWResourceManager;

// Graphics imports
import com.howlingwolf.graphics.HWSpritesheet;
import com.howlingwolf.graphics.HWAnimationFrame;

/**
 * Handles a sprite sheet's XML file parsing.
 * 
 * @author Dylan Sawyer
 */
public class HWSpritesheetHandler extends DefaultHandler
{
	// Containers for information pertaining to the currently parsed element
	private String m_sCurrentElementName;
	private String m_sCurrentElementValue;
	
	private static final String FILETYPE = ".png";
	private static final String FILEPATH = "images/spritesheets/";
	
	// Scanner for parsing the frame strings
	private Scanner intScanner;
	
	private HWSpritesheet spritesheet;
	
	// Info for each frame
	private String identifier, frame;
	private long duration;
	
	// The image of the spritesheet
	private BufferedImage image;
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException
	{
		m_sCurrentElementName = qName;
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		// Reset the current element information stored
		m_sCurrentElementName = "";
		m_sCurrentElementValue = "";
		
		if (qName.equalsIgnoreCase("animationFrame"))
		{
			int x, y, w, h;
			intScanner = new Scanner(frame);
			x = intScanner.nextInt();
			y = intScanner.nextInt();
			w = intScanner.nextInt();
			h = intScanner.nextInt();
			
			spritesheet.AddFrame(new HWAnimationFrame(identifier, image.getSubimage(x, y, w, h), duration));
		}
	}
	
	@Override
	public void characters(char ch[], int start, int length)
			throws SAXException
	{
		m_sCurrentElementValue = new String(ch, start, length);
		
		if (m_sCurrentElementName.equalsIgnoreCase("sheetName"))
		{
			spritesheet = new HWSpritesheet(m_sCurrentElementValue);
			image = HWResourceManager.SharedManager().LoadBufferedImage(FILEPATH + m_sCurrentElementValue + FILETYPE);
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("identifier"))
		{
			identifier = m_sCurrentElementValue;
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("frame"))
		{
			frame = m_sCurrentElementValue;
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("duration"))
		{
			duration = Long.parseLong(m_sCurrentElementValue);
		}
	}
	
	/**
	 * Gets the HWSpritesheet object that has been created by parsing the XML file.
	 * 
	 * @return The spritesheet
	 */
	public HWSpritesheet GetSpritesheet()
	{
		return spritesheet;
	}
}
