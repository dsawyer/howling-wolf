package com.howlingwolf.util.xml;

// Core imports
import com.howlingwolf.managers.HWResourceManager;
import com.howlingwolf.util.HWExtraTypes;
import com.howlingwolf.world.HWMessageObject;
import com.howlingwolf.world.HWDisplayMessage;

// Graphics imports
import com.howlingwolf.graphics.tileEngine.HWTile;
import com.howlingwolf.graphics.tileEngine.HWTiledLayer;
import com.howlingwolf.graphics.tileEngine.HWTileset;
import com.howlingwolf.graphics.HWSprite;

// Audio imports
import com.howlingwolf.audio.music.HWMidiFile;

// Java imports
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

// Org imports
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * General purpose class that can create a variety of objects from XML files.
 * Everything is grouped in one place for ease of access. Uses the DOM parser
 * for a more visual and easier to understand tree model compared to the SAX
 * parser.
 * 
 * @author Dylan Sawyer
 */
public class HWXMLReaderDOM
{
	private static final String FILETYPE_XML = ".xml";
	private static final String FILETYPE_IMAGE = ".png";
	
	private static final String FILEPATH_TILESET = "xml/tilesets/";
	private static final String FILEPATH_AREA = "xml/areas/";
	private static final String FILEPATH_MIDI = "xml/music/";
	
	// Helper method to create a Document object from the given file
	private static Document ParseFile(String p_sFilename)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document document = null;
		
		try
		{
			builder = factory.newDocumentBuilder();
			document = builder.parse(HWXMLReaderSAX.class.getClassLoader().getResourceAsStream(p_sFilename));
		}
		catch (ParserConfigurationException e)
		{
			System.out.println("Failed to load an appropriate Document Builder");
		}
		catch (SAXException e)
		{
			System.out.println("SAX Exception");
		}
		catch (IllegalArgumentException e)
		{
			System.out.println(p_sFilename + " does no exist");
		}
		catch (IOException e)
		{
			System.out.println("Failed to load file: " + p_sFilename);
		}
		
		return document;
	}
	
	/**
	 * Loads a WEHWTileset object using information stored in an XML file.
	 * 
	 * @param p_sFilename The name of the XML file
	 * @return			  The WEHWTileset object, full of HWTiles
	 */
	public static HWTileset LoadTileset(String p_sFilename)
	{
		Document doc = ParseFile(FILEPATH_TILESET + p_sFilename + FILETYPE_XML);
		
		if (doc != null)
		{
			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();
			
			String tilesetName;
			int tilesHigh, tilesWide, tileWidth, tileHeight;
			
			// Get the tileset name (identifier and image file name)
			tilesetName = root.getElementsByTagName("tilesetName").item(0).getTextContent();
			
			// Get the tileset width (in tiles)
			tilesWide = Integer.parseInt(root.getElementsByTagName("tilesWide").item(0).getTextContent());
			
			// Get the tileset height (in tiles)
			tilesHigh = Integer.parseInt(root.getElementsByTagName("tilesHigh").item(0).getTextContent());
			
			// Get the tile width
			tileWidth = Integer.parseInt(root.getElementsByTagName("tileWidth").item(0).getTextContent());
			
			// Get the tile height
			tileHeight = Integer.parseInt(root.getElementsByTagName("tileHeight").item(0).getTextContent());
			
			// Prepare the list of tiles
			HWTile[] tiles = new HWTile[tilesWide * tilesHigh];
			int tileCounter = 0;
			
			// Get the image used for the tile set
			BufferedImage tileImage = HWResourceManager.SharedManager().LoadBufferedImage(FILEPATH_TILESET + tilesetName + FILETYPE_IMAGE);
			
			// Get all the tile data
			NodeList tileData = root.getElementsByTagName("tile");
			
			for (int i = 0; i < tileData.getLength(); i+=1)
			{
				Node tileNode = tileData.item(i);
				
				if (tileNode.getNodeType() == Node.ELEMENT_NODE)
				{
					if (tileNode.getNodeName().equalsIgnoreCase("tile"))
					{
						HWTile tile = TileFromNode(tileNode, tileImage, tileWidth, tileHeight);
						tiles[tileCounter] = tile;
						tileCounter+=1;
					}
				}
			}
			
			// Create the tileset and return it
			return new HWTileset(tilesetName, tilesWide, tilesHigh, tileWidth, tileHeight, tiles);
		}
		
		return null;
	}
	
	/**
	 * Loads a HWMidiFile object based on the information stored within an XML
 file. Will be moved to HWXMLReaderDOM once I have the MIDIHandler class
 built.
	 * 
	 * @param p_sFilename The name of the XML file
	 * @return			  The HWMidiFile object, with all its necessary info
	 */
	public static HWMidiFile LoadMidi(String p_sFilename)
	{
		Document doc = ParseFile(FILEPATH_MIDI + p_sFilename + FILETYPE_XML);
		
		if (doc != null)
		{
			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();
			
			String filename;
			float bpm, loopBegin, loopEnd;
			int numLoops;
			
			// Get the filename
			filename = root.getElementsByTagName("filename").item(0).getTextContent();
			
			// Get the BPM
			bpm = Float.parseFloat(root.getElementsByTagName("bpm").item(0).getTextContent());
			
			// Get the loop's beginning point
			loopBegin = Float.parseFloat(root.getElementsByTagName("loopBegin").item(0).getTextContent());
			
			// Get the loop's end point
			loopEnd = Float.parseFloat(root.getElementsByTagName("loopEnd").item(0).getTextContent());
			
			// Get the number of loops
			numLoops = Integer.parseInt(root.getElementsByTagName("numLoops").item(0).getTextContent());
			
			// Create and return the new HWMidiFile object
			return new HWMidiFile(filename, bpm, loopBegin, loopEnd, numLoops);
		}
		
		return null;
	}
	
	//**************************************************************************
	// Helper methods
	//**************************************************************************
	
	// Helper method to create a HWTiledLayer from a given Node
	private static HWTiledLayer LayerFromNode(Node layerNode)
	{
		String identifier, tilesetName;
		int width, height, xOffset, yOffset;
		int[] tileValues;
		Scanner intScanner;
		
		Element data = (Element)layerNode;
		
		// Get the HWTiledLayer identifier
		identifier = data.getElementsByTagName("identifier").item(0).getTextContent();
		
		// Get the name of the tileset used for this layer
		tilesetName = data.getElementsByTagName("tilesetName").item(0).getTextContent();
		
		// Get the size of the layer (in tiles)
		width = Integer.parseInt(data.getElementsByTagName("widthInTiles").item(0).getTextContent());
		height = Integer.parseInt(data.getElementsByTagName("heightInTiles").item(0).getTextContent());
		
		// Get the offset of the layer
		xOffset = Integer.parseInt(data.getElementsByTagName("xoffset").item(0).getTextContent());
		yOffset = Integer.parseInt(data.getElementsByTagName("yoffset").item(0).getTextContent());
		
		int valueCounter = 0;
		tileValues = new int[width * height];
		
		// Get all the tile values
		Scanner lineScanner = new Scanner(data.getElementsByTagName("tileData").item(0).getTextContent());
		
		// Get each line, this gets rid of unwanted empty lines
		while (lineScanner.hasNext())
		{
			String line = lineScanner.nextLine();
			intScanner = new Scanner(line);
			
			// Get each value from the line
			while (intScanner.hasNext())
			{
				tileValues[valueCounter] = intScanner.nextInt();
				valueCounter += 1;
			}
		}
		
		// Return the new layer
		return new HWTiledLayer(tileValues, width, height, xOffset, yOffset, tilesetName, identifier);
	}
	
	// Helper method to create a HWMessageObject from a given Node
	private static HWMessageObject InteractibleFromNode(Node interactibleNode)
	{
		BufferedImage image, objectImage;
		int x, y, width, height, imageX, imageY, imageWidth, imageHeight;
		Scanner intScanner;
		
		Element data = (Element)interactibleNode;
		
		// Get the hitbox data
		intScanner = new Scanner(data.getElementsByTagName("hitbox").item(0).getTextContent());
		x = intScanner.nextInt();
		y = intScanner.nextInt();
		width = intScanner.nextInt();
		height = intScanner.nextInt();
		
		// Get the image
		String imagePath = FILEPATH_TILESET + data.getElementsByTagName("imageName").item(0).getTextContent() + FILETYPE_IMAGE;
		image = HWResourceManager.SharedManager().LoadBufferedImage(imagePath);
		
		// Get the image frame
		intScanner = new Scanner(data.getElementsByTagName("imageFrame").item(0).getTextContent());
		imageX = intScanner.nextInt();
		imageY = intScanner.nextInt();
		imageWidth = intScanner.nextInt();
		imageHeight = intScanner.nextInt();
		
		objectImage = image.getSubimage(imageX, imageY, imageWidth, imageHeight);
		
		// Create the interactable object
		HWMessageObject object = new HWMessageObject(x, y, width, height, new HWSprite());
		
		// Get all the interactions
		NodeList interactionList = data.getElementsByTagName("interaction");
		
		for (int i = 0; i < interactionList.getLength(); i+=1)
		{
			Node interactionNode = interactionList.item(i);
			
			if (interactionNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element interaction = (Element)interactionNode;
				HWDisplayMessage action = new HWDisplayMessage();
				String side = interaction.getElementsByTagName("side").item(0).getTextContent();
				
				NodeList messages = interaction.getElementsByTagName("message");
				
				// Get all the messages that are a part of the interaction
				for (int j = 0; j < messages.getLength(); j+=1)
				{
					Node messageNode = messages.item(j);
					
					if (messageNode.getNodeType() == Node.ELEMENT_NODE)
					{
						action.AddMessage(messageNode.getTextContent());
					}
				}
				
				// Set the interaction's available side based on the 
				if (side.equalsIgnoreCase("any"))
				{
					object.AddInteraction(action, HWExtraTypes.InteractableSide.ANY);
				}
				else if (side.equalsIgnoreCase("top"))
				{
					object.AddInteraction(action, HWExtraTypes.InteractableSide.TOP);
				}
				else if (side.equalsIgnoreCase("bottom"))
				{
					object.AddInteraction(action, HWExtraTypes.InteractableSide.BOTTOM);
				}
				else if (side.equalsIgnoreCase("left"))
				{
					object.AddInteraction(action, HWExtraTypes.InteractableSide.LEFT);
				}
				else if (side.equalsIgnoreCase("right"))
				{
					object.AddInteraction(action, HWExtraTypes.InteractableSide.RIGHT);
				}
			}
		}
		
		return object;
	}
	
	// Helper method that takes in a node and creates a HWTile from it
	private static HWTile TileFromNode(Node tileNode, BufferedImage tileImage, int tileWidth, int tileHeight)
	{
		ArrayList<BufferedImage> tileFrames;
		Scanner frameScanner;
		HWExtraTypes.CollisionType collisionType;
		
		Element data = (Element)tileNode;

		String collision = data.getElementsByTagName("collisionType").item(0).getTextContent();

		// Get the collision type of the tile
		if (collision.equalsIgnoreCase("PASSABLE"))
		{
			collisionType = HWExtraTypes.CollisionType.PASSABLE;
		}
		else if (collision.equalsIgnoreCase("SOLID"))
		{
			collisionType = HWExtraTypes.CollisionType.SOLID;
		}
		else // Default value
		{
			collisionType = HWExtraTypes.CollisionType.PASSABLE;
		}

		// Get all the frames used in animating it
		NodeList frames = data.getElementsByTagName("frame");
		tileFrames = new ArrayList<>();

		for (int j = 0; j < frames.getLength(); j+=1)
		{
			Node frameNode = frames.item(j);

			if (frameNode.getNodeType() == Node.ELEMENT_NODE)
			{
				// Get the positions of each frame
				frameScanner = new Scanner(frameNode.getTextContent());
				int x = frameScanner.nextInt();
				int y = frameScanner.nextInt();

				tileFrames.add(tileImage.getSubimage(x, y, tileWidth, tileHeight));
			}
		}

		return new HWTile(null, tileFrames);
	}
}
