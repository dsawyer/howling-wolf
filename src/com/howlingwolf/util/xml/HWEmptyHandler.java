package com.howlingwolf.util.xml;

// Org imports
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * An empty handler to use as a template when making new DefaultHandler
 * extensions.
 * 
 * @author Dylan Sawyer
 */
public class HWEmptyHandler extends DefaultHandler
{
	// Containers for information pertaining to the currently parsed element
	private String m_sCurrentElementName;
	private String m_sCurrentElementValue;
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException
	{
		System.out.print("<" + qName + ">");
		
		m_sCurrentElementName = qName;
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		System.out.print("</" + qName + ">");
		
		// Reset the current element information stored
		m_sCurrentElementName = "";
		m_sCurrentElementValue = "";
	}
	
	@Override
	public void characters(char ch[], int start, int length)
			throws SAXException
	{
		System.out.print(new String(ch, start, length));
		
		m_sCurrentElementValue = new String(ch, start, length);
	}
	
	@Override
	public void endDocument()
	{
		System.out.println();
	}
}
