package com.howlingwolf.util.xml;

// Java imports
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;

// Org imports
import org.xml.sax.SAXException;

// Graphics imports
import com.howlingwolf.graphics.tileEngine.HWTileset;
import com.howlingwolf.graphics.HWSpritesheet;

// Audio Imports
import com.howlingwolf.audio.music.HWMidiFile;

/**
 * General purpose class that can create a variety of objects from XML files.
 * Everything is grouped in one place for ease of access. Uses the SAX parser
 * for rapid loading of objects from XML.
 * 
 * @author Dylan Sawyer
 */
public class HWXMLReaderSAX
{
	// Filepaths and extensions to allow the use of just passing in the exact
	// name for the desired file, so long as the type of file is known.

	/**
	 * The file type extension used by XML files. This is public so that
	 * subclasses of this can still use the same variable.
	 */
	public static final String FILETYPE_XML = ".xml";
	
	private static final String FILEPATH_TILESET = "xml/tilesets/";
	private static final String FILEPATH_AREA = "xml/areas/";
	private static final String FILEPATH_MIDI = "xml/music/";
	private static final String FILEPATH_SPRITESHEET = "xml/spritesheets/";
	private static final String FILEPATH_WORLD = "xml/world/";
	
	/**
	 * Loads a HWTileset using the SAX parser.
	 * 
	 * @param p_sFilename The filename of the XML file
	 * @return A HWTileset object loaded from the XML file
	 */
	public static HWTileset LoadTileset(String p_sFilename)
	{
		try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			
			HWTilesetHandler handler = new HWTilesetHandler();
			
			String filepath = FILEPATH_TILESET + p_sFilename + FILETYPE_XML;
			
			// Parses all the way through the file, does not return until the
			// parse is complete
			saxParser.parse(HWXMLReaderSAX.class.getClassLoader().getResourceAsStream(filepath), handler);
			
			return handler.Tileset();
		}
		catch (ParserConfigurationException | SAXException | IOException e)
		{
			
		}
		
		return null;
	}
	
	/**
	 * Loads a HWMidiFile object based on the information stored within an XML
	 * file.
	 * 
	 * @param p_sFilename The name of the XML file
	 * @return			  The HWMidiFile object, with all its necessary info
	 */
	public static HWMidiFile LoadMidi(String p_sFilename)
	{
		try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			
			HWMidiFileHandler handler = new HWMidiFileHandler();
			
			String filepath = FILEPATH_MIDI + p_sFilename + FILETYPE_XML;
			
			// Parses all the way through the file, does not return until the
			// parse is complete
			saxParser.parse(HWXMLReaderSAX.class.getClassLoader().getResourceAsStream(filepath), handler);
			
			return handler.GetMidi();
		}
		catch (ParserConfigurationException | SAXException | IOException e)
		{
			
		}
		
		return null;
	}
	
	/**
	 * Loads a HWSpritesheet based on the information stored within the given
	 * XML file.
	 * 
	 * @param p_sFilename The name of the XML file to parse
	 * @return A HWSpritesheet object created by parsing the XML file
	 */
	public static HWSpritesheet LoadSpritesheet(String p_sFilename)
	{
		try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			
			HWSpritesheetHandler handler = new HWSpritesheetHandler();
			
			String filepath = FILEPATH_SPRITESHEET + p_sFilename + FILETYPE_XML;
			
			// Parses all the way through the file, does not return until the
			// parse is complete
			saxParser.parse(HWXMLReaderSAX.class.getClassLoader().getResourceAsStream(filepath), handler);
			
			return handler.GetSpritesheet();
		}
		catch (ParserConfigurationException | SAXException | IOException e)
		{
			System.out.println(e);
		}
		
		return null;
	}
}
