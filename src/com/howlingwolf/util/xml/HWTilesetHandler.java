package com.howlingwolf.util.xml;

// Core imports
import com.howlingwolf.managers.HWResourceManager;

// Graphics imports
import com.howlingwolf.graphics.tileEngine.HWTile;
import com.howlingwolf.graphics.tileEngine.HWTileset;

// Math imports
import com.howlingwolf.util.math.HWVec2;
import com.howlingwolf.util.math.HWPolygon;

// Java imports
import java.awt.image.BufferedImage;
import java.util.Scanner;
import java.util.ArrayList;

// Org imports
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;


/**
 * Creates a custom SAX parser handler for WEHWTilesets.
 * 
 * @author Dylan Sawyer
 */
public class HWTilesetHandler extends DefaultHandler
{
	private static final String FILETYPE = ".png";
	private static final String FILEPATH = "images/tilesets/";
	
	private Scanner intScanner;
	private Scanner floatScanner;
	
	// Containers for information pertaining to the currently parsed element
	private String m_sCurrentElementName;
	private String m_sCurrentElementValue;
	
	// Variables to hold on to information until the end of the <tileset> tag
	// is reached, so that a HWTileset object can then be made.
	private HWTile m_Tile;
	private String frameDuration;
	private HWTileset m_Tileset;
	
	private BufferedImage image;
	
	// Information required for the HWTileset constructor, in that order
	private String tilesetName;
	private int tilesWide;
	private int tilesHigh;
	private int tileWidth;
	private int tileHeight;
	private HWTile[] tiles;
	
	// Keeps track of how many tiles have been added to the array of tiles
	private int m_iTileCounter = 0;
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException
	{
		m_sCurrentElementName = qName;
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		// Reset the current element information stored
		m_sCurrentElementName = "";
		m_sCurrentElementValue = "";
		
		if (qName.equalsIgnoreCase("tile"))
		{
			tiles[m_iTileCounter] = m_Tile.Clone();
			m_Tile = null;
			frameDuration = null;
			m_iTileCounter += 1;
		}		
		else if (qName.equalsIgnoreCase("tileset"))
		{
			m_Tileset = new HWTileset(tilesetName, tilesWide, tilesHigh, tileWidth, tileHeight, tiles);
		}
	}
	
	@Override
	public void characters(char ch[], int start, int length)
			throws SAXException
	{
		m_sCurrentElementValue = new String(ch, start, length);
		
		if (m_sCurrentElementName.equalsIgnoreCase("tilesetName"))
		{
			tilesetName = m_sCurrentElementValue;
			image = HWResourceManager.SharedManager().LoadBufferedImage((FILEPATH + tilesetName + FILETYPE));
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("tilesWide"))
		{
			tilesWide = Integer.parseInt(m_sCurrentElementValue);
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("tilesHigh"))
		{
			tilesHigh = Integer.parseInt(m_sCurrentElementValue);
			tiles = new HWTile[tilesWide * tilesHigh];
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("tileWidth"))
		{
			tileWidth = Integer.parseInt(m_sCurrentElementValue);
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("tileHeight"))
		{
			tileHeight = Integer.parseInt(m_sCurrentElementValue);
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("tile"))
		{
			m_Tile = new HWTile();
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("collisionType"))
		{
			ArrayList<HWVec2> points = new ArrayList<>();
			float x, y;
			
			floatScanner = new Scanner(m_sCurrentElementValue);
			
			// Draw out all the points from the string
			// In general, there should be 3 or 4 points
			while(floatScanner.hasNextFloat())
			{
				x = floatScanner.nextFloat();
				y = floatScanner.nextFloat();
				
				points.add(new HWVec2(x, y));
			}
			
			if (points.isEmpty())
			{
				m_Tile.SetCollisionShape(null);
			}
			else
			{
				m_Tile.SetCollisionShape(new HWPolygon(points));
			}
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("frameDuration"))
		{
			frameDuration = m_sCurrentElementValue;
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("frame"))
		{
			intScanner = new Scanner(m_sCurrentElementValue);
			int x = intScanner.nextInt();
			int y = intScanner.nextInt();
			
			if (frameDuration != null)
			{
				intScanner = new Scanner(frameDuration);
				int duration = intScanner.nextInt();

				m_Tile.AddFrame(image.getSubimage(x, y, tileWidth, tileHeight), duration);
			}
			else
			{
				m_Tile.AddFrame(image.getSubimage(x, y, tileWidth, tileHeight));
			}
		}
	}
	
	/**
	 * Gets the created HWTileset object. *MUST* only be called after the parser
	 * has finished with the XML file.
	 * 
	 * @return The HWTileset object created by parsing the XML file
	 */
	public HWTileset Tileset()
	{
		return m_Tileset;
	}
}