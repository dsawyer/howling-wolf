package com.howlingwolf.util.xml;

// Audio imports
import com.howlingwolf.audio.music.HWMidiFile;

// Org imports
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Creates a custom SAX parser handler for HWMidiFiles.
 * 
 * @author Dylan Sawyer
 */
public class HWMidiFileHandler extends DefaultHandler
{
	// Containers for information pertaining to the currently parsed element
	private String m_sCurrentElementName;
	private String m_sCurrentElementValue;
	
	// The HWMidiFile created by parsing an XML file
	private HWMidiFile midiFile;
	
	// The information needed to create a HWMidiFile
	private String filename;
	private float bpm, loopBegin, loopEnd;
	private int numLoops;
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException
	{
		m_sCurrentElementName = qName;
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		// Reset the current element information stored
		m_sCurrentElementName = "";
		m_sCurrentElementValue = "";
		
		if (qName.equalsIgnoreCase("midiFile"))
		{
			midiFile = new HWMidiFile(filename, bpm, loopBegin, loopEnd, numLoops);
		}
	}
	
	@Override
	public void characters(char ch[], int start, int length)
			throws SAXException
	{
		m_sCurrentElementValue = new String(ch, start, length);
		
		if (m_sCurrentElementName.equalsIgnoreCase("filename"))
		{
			filename = m_sCurrentElementValue;
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("bpm"))
		{
			bpm = Float.parseFloat(m_sCurrentElementValue);
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("loopBegin"))
		{
			loopBegin = Float.parseFloat(m_sCurrentElementValue);
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("loopEnd"))
		{
			loopEnd = Float.parseFloat(m_sCurrentElementValue);
		}
		else if (m_sCurrentElementName.equalsIgnoreCase("numLoops"))
		{
			numLoops = Integer.parseInt(m_sCurrentElementValue);
		}
	}
	
	/**
	 * Gets the HWMidiFile object created from parsing the XML file.
	 * 
	 * @return A ready to go HWMidiFile object
	 */
	public HWMidiFile GetMidi()
	{
		return midiFile;
	}
}
