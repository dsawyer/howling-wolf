package com.howlingwolf.util;

import java.util.LinkedList;

/**
 * A thread pool is a limited number of threads used to run various tasks.
 * @author Dylan
 */
public class HWThreadPool extends ThreadGroup
{
	//*************************************************************************
	// Member Variables
    //*************************************************************************
    private boolean m_bIsAlive;
	private LinkedList<Runnable> m_lTaskQueue;
	private int m_iThreadID;
	private static int m_iThreadPoolID;


	//*************************************************************************
	// Methods
    //*************************************************************************

    /**
	 * Creates a new thread pool with a specified number of threads.
	 * 
	 * @param p_iNumThreads The number of threads
	 */
	public HWThreadPool(int p_iNumThreads)
    {
    	// Basic ThreadGroup stuff
    	super("ThreadPool-" + (m_iThreadPoolID));
    	m_iThreadPoolID += 1;
    	setDaemon(true);

    	m_bIsAlive = true;

    	// Create the set of threads that will be available
    	m_lTaskQueue = new LinkedList<>();
    	for (int i = 0; i < p_iNumThreads; i+=1)
    	{
    		new PooledThread().start();
    	}
    }

    /**
	 * Requests a new task to run. The task executes on the next available idle
	 * thread in the pool. Tasks start execution in the order they are received.
	 * 
	 * @param p_task The new task to run as soon as possible
	 */
	public synchronized void RunTask(Runnable p_task)
    {
    	if (!m_bIsAlive)
    	{
    		throw new IllegalStateException();
    	}

    	if (p_task != null)
    	{
    		m_lTaskQueue.add(p_task);
    		notify();
    	}
    }

    /**
	 * Gets the next task awaiting execution.
	 * 
	 * @return The next task to run
	 * @throws InterruptedException
	 */
	protected synchronized Runnable Task() throws InterruptedException
    {
    	while (m_lTaskQueue.size() == 0)
    	{
    		if (!m_bIsAlive)
    		{
    			return null;
    		}
    		wait();
    	}
    	return m_lTaskQueue.removeFirst();
    }

    /**
	 * Closes the thread pool, stopping all threads. Any remaining tasks are
	 * ignored.
	 */
	public synchronized void Close()
    {
    	if (!m_bIsAlive)
    	{
    		m_bIsAlive = false;
    		m_lTaskQueue.clear();
    		interrupt();
    	}
    }

    /**
	 * Closes the thread pool and waits for all running threads to finish. Any
	 * remaining tasks are executed.
	 */
	public void Join()
    {
    	// Notify all waiting threads that the thread pool is no longer alive
    	synchronized(this)
    	{
    		m_bIsAlive = false;
    		notifyAll();
    	}

    	// Wait for all the threads to finish
    	Thread[] threads = new Thread[activeCount()];
    	int count = enumerate(threads);
    	for (int i = 0; i < count; i+=1)
    	{
    		try
    		{
    			threads[i].join();
    		}
    		catch (InterruptedException ex) {}
    	}
    }

    /**
	 * Signals that a PooledThread has started. This method does nothing by
	 * default; subclasses should override to do any thread-specific startup
	 * tasks.
	 */
	protected void ThreadStarted()
    {
    	// Do nothing
    }

    /**
	 * Signals that a PooledThread has stopped. This method does nothing by
	 * default; subclasses should override to do any thread-specific cleanup
	 * tasks.
	 */
	protected void ThreadStopped()
    {
    	// Do nothing
    }

    // A PooledThread is just a thread in a HWThreadPool group, designed to run tasks.
    private class PooledThread extends Thread
    {
    	public PooledThread()
    	{
    		super(HWThreadPool.this, "PooledThread-" + (m_iThreadID));
    		m_iThreadID += 1;
    	}

		@Override
    	public void run()
    	{
    		// Signal that this thread has started
    		ThreadStarted();

    		while (!isInterrupted())
    		{
    			// Get a task to run
    			Runnable task = null;
    			try
    			{
    				task = Task();
    			}
    			catch (InterruptedException ex) {}

    			// If the task returned null or had an exception, close the thread by returning.
    			if (task == null)
    			{
    				return;
    			}

    			// Run the task and eat any exceptions it throws
    			try
    			{
    				task.run();
    			}
    			catch (Throwable t)
    			{
    				uncaughtException(this, t);
    			}
    		}

    		// Signal that this thread has stopped
    		ThreadStopped();
    	}
    }
}