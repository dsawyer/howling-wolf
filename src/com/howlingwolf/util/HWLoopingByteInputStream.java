package com.howlingwolf.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Allows for looping through a byte input stream. The looping stops when
 * the close() method is called.
 * <p>
 * Add functionality for looping a specific number of times?
 * 
 * @author Dylan Sawyer
 */
public class HWLoopingByteInputStream extends ByteArrayInputStream
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private boolean m_bClosed;

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates a new LoopingByteInputStream with the specified byte array.
	 * The array is not copied.
	 * 
	 * @param p_buffer Specified byte array
	 */
	public HWLoopingByteInputStream(byte[] p_buffer)
    {
		super(p_buffer);
		m_bClosed = false;
    }

    // Reads length bytes from the array. If the end of the array is reached,
    // the reading starts over from the beginning of the array. Returns -1 if
    // the array has been closed.
	@Override
    public int read(byte[] p_buffer, int p_iOffset, int p_iLength)
    {
    	if (m_bClosed)
    	{
    		return -1;
    	}

    	int totalBytesRead = 0;

    	while (totalBytesRead < p_iLength)
    	{
    		int numBytesRead = super.read(p_buffer, p_iOffset + totalBytesRead, p_iLength - totalBytesRead);

    		if (numBytesRead > 0)
    		{
    			totalBytesRead += numBytesRead;
    		}
    		else
    		{
    			reset();
    		}
    	}

    	return totalBytesRead;
    }

    // Closes the stream. Future calls to the read() methods will return -1.
	@Override
    public void close() throws IOException
    {
    	super.close();
    	m_bClosed = true;
    }
}