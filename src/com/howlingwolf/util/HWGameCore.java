package com.howlingwolf.util;

// Java imports
import java.awt.*;
import java.awt.image.BufferedImage;

// Core imports
import com.howlingwolf.managers.HWScreenManager;

/**
 * Abstract class used as the basis for the GameManager. Reusable.
 * 
 * @author Dylan Sawyer
 */
public abstract class HWGameCore
{
	private long m_lStartTime;
	
	private long lastFrameReset;
	private long numFrames;
	private int fps;
	
	private static final long STATS_UPDATE_PER_SECOND = 4;
	private static final long STATS_UPDATE = 1000 / STATS_UPDATE_PER_SECOND;
	
    /**
	 * Default font size.
	 */
	protected static final int FONT_SIZE = 24;
	
	// Number of times the game updates
	private static final long LOGIC_UPDATES_PER_SECOND = 60;
	// Maximum amount of frames to be skipped
	private static final int MAX_FRAMESKIP = 5;
	// Maximum number of frames per second (used for throttling fps)
	private static final long MAX_FRAMES_PER_SECOND = 60;
	
	// Amount of time between game updates
	private static final long MILLIS_PER_LOGIC_UPDATE = 1000 / LOGIC_UPDATES_PER_SECOND;
	// Amount of time between Draw updates (used when throttling)
	private static final long MILLIS_PER_DRAW_UPDATE = 1000 / MAX_FRAMES_PER_SECOND;
	
	private static final boolean SHOW_STATS = true;

    /*private static final DisplayMode POSSIBLE_MODES[] = {
        new DisplayMode(800, 600, 16, 0),
        new DisplayMode(800, 600, 32, 0),
        new DisplayMode(800, 600, 24, 0),
        new DisplayMode(640, 480, 16, 0),
        new DisplayMode(640, 480, 32, 0),
        new DisplayMode(640, 480, 24, 0),
        new DisplayMode(1024, 768, 16, 0),
        new DisplayMode(1024, 768, 32, 0),
        new DisplayMode(1024, 768, 24, 0),
        new DisplayMode(1920, 1080, 32, 0),
    };*/

    private boolean isRunning;
    /**
	 * The screen manager used in the game.
	 */
	protected HWScreenManager screen;
	
	/**
	 * The current full screen display mode used in the game.
	 */
	protected DisplayMode currentFullScreenDisplay;


    /**
     * Signals the game loop that it's time to quit
     */
    public void Stop() {
        isRunning = false;
    }


    /**
     * Calls Init() and GameLoop()
     */
    public void Run() {
        try {
            Init();
            GameLoop();
        }
        finally {
            screen.RestoreScreen();
            LazilyExit();
        }
    }


    /**
     * Exits the VM from a daemon thread. The daemon thread waits
     * 2 seconds then calls System.exit(0). Since the VM should
     * exit when only daemon threads are running, this makes sure
     * System.exit(0) is only called if neccesary. It's neccesary
     * if the Java Sound system is running.
     */
    public void LazilyExit()
	{
        Thread thread;
		thread = new Thread()
		{
			@Override
			public void run()
			{
				// first, wait for the VM exit on its own.
				try
				{
					Thread.sleep(2000);
				}
				catch (InterruptedException ex) { }
				// system is still running, so force an exit
				System.exit(0);
			}
		};
        thread.setDaemon(true);
        thread.start();
    }


    /**
     * Sets windowed mode and initiates and objects.
     */
    public void Init() {
        screen = new HWScreenManager();
        currentFullScreenDisplay = new DisplayMode(1920, 1080, 32, 0);//screen.FindFirstCompatibleMode(POSSIBLE_MODES);
        //screen.SetFullScreen(currentFullScreenDisplay);
		screen.SetWindowed();

        isRunning = true;
    }

    /**
     * Runs through the game loop until Stop() is called.
     */
    public void GameLoop()
	{
        m_lStartTime = System.currentTimeMillis();
		numFrames = 0;
		lastFrameReset = m_lStartTime;
		fps = 0;
		long elapsedTime;
		long nextGameUpdate = TimeSinceStart();
		long lastUpdateTime = nextGameUpdate;
		
		int logicUpdates;
		//float interpolation;

        while (isRunning)
		{
			logicUpdates = 0;
			
			// Game Update, constant speed
			while((nextGameUpdate < TimeSinceStart()) && (logicUpdates < MAX_FRAMESKIP))
			{
				elapsedTime = nextGameUpdate - lastUpdateTime;
				Update(elapsedTime);
				
				lastUpdateTime = nextGameUpdate;
				nextGameUpdate += MILLIS_PER_LOGIC_UPDATE;
				logicUpdates += 1;
			}
			
			// Draw updates
			//interpolation = (float)(TimeSinceStart() + MILLIS_PER_LOGIC_UPDATE - nextGameUpdate) / (float)(MILLIS_PER_LOGIC_UPDATE);
			//DrawGame(interpolation);
			DrawGame(0.0f);
        }
    }
	
	// Gets the amount of time (in milliseconds) since the game started
	private long TimeSinceStart()
	{
		return System.currentTimeMillis() - m_lStartTime;
	}
	
	// Does all the necessary drawing stuff.
	// The interpolation value is not currently used, and probably won't
	// be in this iteration of the engine. It is used to see how much time
	// (percentage) we are between logic updates, so that things can be drawn
	// in a more fluid place.
	private void DrawGame(float p_fInterpolation)
	{
		// Get the image graphics used as the base for drawing
		Graphics2D bg = screen.GetBackgroundGraphicsContext();
		Draw(bg, p_fInterpolation);

		if (SHOW_STATS)
		{
			//float fps = 1000 / p_fInterpolation;
			numFrames += 1;
			long timeSinceLastReset = System.currentTimeMillis() - lastFrameReset;
			if (timeSinceLastReset >= STATS_UPDATE)
			{
				float secs = ((float)timeSinceLastReset / 1000);
				fps = (int)((float)numFrames / secs);
				numFrames = 0;
				lastFrameReset = System.currentTimeMillis();
			}
			
			String fpsString = "FPS: " + fps;
			bg.setColor(Color.white);
			bg.drawString(fpsString, 10, 20);
		}

		bg.dispose();
		screen.UpdateDisplay();
	}


    /**
     * Updates the state of the game/animation based on the amount of elapsed
	 * time that has passed. Subclasses must override this method.
	 * 
	 * @param elapsedTime The amount of time that has passed since the last
	 *					  Update
	 */
    public abstract void Update(long elapsedTime);


    /**
     * Draws to the screen. Subclasses must override this method.
	 * 
	 * @param g The graphics context upon which the game is drawn
	 * @param p_fInterpolation Interpolated value between logical updates
	 *						   (NOT USED CURRENTLY)
	 */
    public abstract void Draw(Graphics2D g, float p_fInterpolation);
	
	public BufferedImage GetDrawSpace()
	{
		return screen.GetDrawingFrame();
	}
}
