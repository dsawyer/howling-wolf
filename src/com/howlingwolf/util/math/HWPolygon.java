package com.howlingwolf.util.math;

// Java imports
import java.awt.Graphics2D;
import java.util.ArrayList;

// Camera import
import com.howlingwolf.util.HWCamera;

/**
 * Models a polygon with 3+ vertices.
 * 
 * @author Dylan Sawyer
 */
public class HWPolygon implements HWShape
{
	private final HWVec2[] m_Vertices;
	private final int m_iNumVertices;
	
	private HWBoundingBox m_BoundingBox;
	
	// This is just a flag that decides whether a bounding box needs to be
	// calculated when the point in polygon algorithm is used
	private boolean m_bHasMovedSinceLastAABBCalc = false;
	
	private static final int MIN_NUM_VERTICES = 3;
	
	/**
	 * Creates a basic empty polygon.
	 */
	public HWPolygon()
	{
		this(MIN_NUM_VERTICES);
	}
	
	/**
	 * Creates a polygon with the given number of vertices.
	 * 
	 * @param numVert	The number of vertices the new polygon has
	 */
	public HWPolygon(int numVert)
	{
		// Make sure that the given number of vertices is at least 3
		if (numVert < MIN_NUM_VERTICES)
		{
			m_iNumVertices = MIN_NUM_VERTICES;
		}
		else
		{
			m_iNumVertices = numVert;
		}
		
		m_Vertices = new HWVec2[m_iNumVertices];
		for (int i = 0; i < m_iNumVertices; i+=1)
		{
			m_Vertices[i] = new HWVec2();
		}
		
		m_BoundingBox = new HWBoundingBox();
	}
	
	/**
	 * Creates a new polygon with the given vertices.
	 * 
	 * @param vertices	The vertices for the new polygon
	 * @param numVert	The number of vertices the new polygon has
	 */
	public HWPolygon(HWVec2[] vertices, int numVert)
	{
		// Make sure that the given number of vertices is at least 3
		if (numVert < MIN_NUM_VERTICES)
		{
			m_iNumVertices = MIN_NUM_VERTICES;
		}
		else
		{
			m_iNumVertices = numVert;
		}
		
		m_Vertices = new HWVec2[m_iNumVertices];
		
		for (int i = 0; i < m_iNumVertices; i+=1)
		{
			m_Vertices[i] = vertices[i].Clone();
		}
		
		m_BoundingBox = new HWBoundingBox();
	}
	
	/**
	 * Creates a new polygon based on the provided list of vertices. Mostly used
	 * for cloning and when loading information from XML files.
	 * 
	 * @param p_verts	The list of vertices that are a part of this polygon
	 */
	public HWPolygon(ArrayList<HWVec2> p_verts)
	{
		if (p_verts.size() < MIN_NUM_VERTICES)
		{
			m_iNumVertices = MIN_NUM_VERTICES;
		}
		else
		{
			m_iNumVertices = p_verts.size();
		}
		
		m_Vertices = new HWVec2[m_iNumVertices];
		int count = 0;
		
		for (HWVec2 vert : p_verts)
		{
			m_Vertices[count] = vert.Clone();
			count+=1;
		}
		
		m_BoundingBox = new HWBoundingBox();
	}
	
	/**
	 * Creates a duplicate of this polygon.
	 * 
	 * @return	An exact copy of this polygon
	 */
	public HWPolygon Clone()
	{
		return new HWPolygon(m_Vertices, m_iNumVertices);
	}
	
	/**
	 * Gets the number of vertices contained in the polygon.
	 * 
	 * @return	The number of vertices the polygon has
	 */
	public int GetNumVertices()
	{
		return m_iNumVertices;
	}
	
	/**
	 * Returns all the vertices of the polygon.
	 * 
	 * @return	An array containing all the vertices of the polygon
	 */
	public HWVec2[] GetVertices()
	{
		return m_Vertices;
	}
	
	/**
	 * Gets the vertex at the given index. Checks to make sure that the given
	 * index is not less than 0, and mods it if it is greater than or equal to
	 * the number of vertices in the polygon.
	 * 
	 * @param index	The index of the desired vertex
	 * @return		The vertex desired
	 */
	public HWVec2 GetVertex(int index)
	{
		if (index < 0)
		{
			return null;
		}
		
		if (index >= m_iNumVertices)
		{
			return m_Vertices[index % m_iNumVertices];
		}
		
		return m_Vertices[index];
	}
	
	/**
	 * Sets the vertex at the given index. Checks to make sure that the index
	 * is within the bounds of the polygon's array of vertices.
	 * 
	 * @param vert	The new vertex's value
	 * @param index	The index of the vertex to be set
	 */
	public void SetVertex(HWVec2 vert, int index)
	{
		if ((index >= 0) && (index < m_iNumVertices))
		{
			m_Vertices[index].Set(vert);
			// Flag the polygon as moved
			DidMove();
		}
	}
	
	/**
	 * Translates the polygon by the given amount defined by the translation
	 * vector provided.
	 * 
	 * @param p_TransVec	The change in position for the polygon
	 */
	public void Translate(HWVec2 p_TransVec)
	{
		for (int i = 0; i < GetNumVertices(); i+=1)
		{
			GetVertex(i).Add(p_TransVec);
		}
		
		CalculateAABB();
	}
	
	// Sets the movement flag to true
	private void DidMove()
	{
		m_bHasMovedSinceLastAABBCalc = true;
	}
	
	/**
	 * Checks to see if the polygon has moved in any way since the last time it
	 * calculated the Axis Aligned Bounding Box.
	 * 
	 * @return True if it has moved, false otherwise
	 */
	public boolean HasMoved()
	{
		return m_bHasMovedSinceLastAABBCalc;
	}
	
	/**
	 * Gets the position of the upper left corner of the bounding box that
	 * contains this polygon. This is useful for easier movement calculations.
	 * 
	 * @return	The origin of the bounding box
	 */
	public HWVec2 GetPosition()
	{
		return m_BoundingBox.GetPosition();
	}
	
	/**
	 * Gets the bounding box for this polygon.
	 * 
	 * @return	The Axis Aligned Bounding Box for this polygon
	 */
	public HWBoundingBox GetAABB()
	{
		return m_BoundingBox;
	}
	
	/**
	 * Calculates the Axis Aligned Bounding Box around this polygon.
	 */
	public void CalculateAABB()
	{
		float minX, maxX, minY, maxY;
		HWVec2 testVert = GetVertex(0);
		
		// Set the values to initial ones that are guaranteed to be a part of
		// the polygon
		minX = maxX = testVert.GetX();
		minY = maxY = testVert.GetY();
		
		for (int i = 1; i < m_iNumVertices; i+=1)
		{
			testVert = GetVertex(i);
			// Check to see if any of the values of the bounding box need
			// to be modified
			minX = (testVert.GetX() < minX) ? testVert.GetX() : minX;
			maxX = (testVert.GetX() > maxX) ? testVert.GetX() : maxX;
			minY = (testVert.GetY() < minY) ? testVert.GetY() : minY;
			maxY = (testVert.GetY() > maxY) ? testVert.GetY() : maxY;
		}
		
		m_BoundingBox.Set(minX, minY, maxX - minX, maxY - minY);
		// Reset the flag to false, as the AABB was just calculated
		m_bHasMovedSinceLastAABBCalc = false;
	}
	
	/**
	 * This method uses the ray casting algorithm to determine whether a given
	 * point is within the bounds of this polygon. It takes a point outside and
	 * to the left of the polygon's bounding box and draws a line from that to
	 * the point being tested. It then checks to see how many of the polygon's
	 * lines it intersects. If the number of lines intersected is odd, then the
	 * point is contained within the polygon. If the number of lines intersected
	 * is even, then the point is not contained within the polygon.
	 * 
	 * @param point	The point being checked within the polygon
	 * @return		True if the point is in the polygon, false otherwise
	 */
	public boolean RaycastInclusion(HWVec2 point)
	{
		int numIntersections = 0;
		// Keep a list of intersecting points in the case that the ray passes
		// through a vertex, as it will count that as a second intersection on
		// the other line in which the vertex is used.
		ArrayList<HWVec2> intersections = new ArrayList<>();
		
		// Make a point outside the bounding box of the polygon that creates a
		// line to the point being checked
		HWVec2 raycastPoint = new HWVec2(m_BoundingBox.GetLeft() - 1.0f, point.GetY());
		
		// Calculate the A, B, and C values for the line that goes from the
		// raycastPoint to the point that we're checking, in the formula that
		// defines the line as: A*x + B*y = C.
		// We calculate this here because the raycast line does not change from
		// one intersection check to the next, so it provides a small
		// optimisation in calculating it only once.
		// A = y2 - y1; As our y values are identical, just set it to 0
		float raycastA = 0;
		// B = x1 - x2;
		float raycastB = point.GetX() - raycastPoint.GetX();
		// C = A * x1 + B * y1
		float raycastC = raycastB * raycastPoint.GetY();
		
		HWVec2 intersection;
		
		// Check to see if the line from the raycastPoint to the point we are
		// checking interseects each line of the polygon. Every time it does,
		// we increment the counter by 1.
		for (int i = 0; i < m_iNumVertices; i+=1)
		{
			intersection = HWMathFunctions.LinesIntersection(raycastA, raycastB, raycastC, GetVertex(i), GetVertex(i+1));
			
			if (intersection != null)
			{
				if (ContainsIntersection(intersection, intersections))
				{
					continue;
				}
				
				if ((HWMathFunctions.PointIsOnLineSegment(intersection, raycastPoint, point)) &&
					(HWMathFunctions.PointIsOnLineSegment(intersection, GetVertex(i), GetVertex(i+1))))
				{
					numIntersections+=1;
					intersections.add(intersection);
				}
			}
		}
		
		// If the number of intersections is odd,
		// then the point is within the polygon. Otherwise, it is not.
		return (numIntersections & 1) == 1;
	}
	
	private boolean ContainsIntersection(HWVec2 intersection, ArrayList<HWVec2> intersections)
	{
		for (HWVec2 intersect : intersections)
		{
			if ((intersection != null) && intersection.Equals(intersect))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Does a Axis Aligned Bounding Box check first (quick and easy) that cuts
	 * out most cases. If the point is within the AABB, then it uses the raycast
	 * algorithm to determine how many times a ray starting from the outside of
	 * the polygon to the point we are checking intersects the lines of the
	 * polygon.
	 * 
	 * @param point	The point to be tested
	 * @return		True if the point is within the polygon, false otherwise
	 */
	@Override
	public boolean ContainsPoint(HWVec2 point)
	{
		// Do a AABB check here first
		if (!WithinAABB(point))
		{
			return false;
		}
		
		// If the point is within the Axis Aligned Bounding Box,
		// then do the Raycast algorithm.
		return RaycastInclusion(point);
	}
	
	/**
	 * Checks to see if the point is within the Axis Aligned Bounding Box for
	 * this polygon. If the polygon has moved since the last time the AABB was
	 * calculated, it will recalculate that first.
	 * 
	 * @param point	The point to be tested
	 * @return		True if the point is within the AABB, false otherwise
	 */
	public boolean WithinAABB(HWVec2 point)
	{
		// If the polygon has moved in any way since the last time the AABB
		// was calculated, recalculate it.
		if (HasMoved())
		{
			CalculateAABB();
		}
		
		// Checks to see if the point's x and y values are within the minimum
		// and maximum values of the Axis Aligned Bounding Box
		return (point.GetX() >= m_BoundingBox.GetLeft()) &&
			   (point.GetX() <= m_BoundingBox.GetRight()) &&
			   (point.GetY() >= m_BoundingBox.GetTop()) &&
			   (point.GetY() <= m_BoundingBox.GetBottom());
	}
	
	@Override
	public void Draw(Graphics2D graphics)
	{
		HWVec2 cameraOffset = HWCamera.Handle().GetPosition();
		
		HWVec2 vert1, vert2;
		
		for (int i = 0; i < m_iNumVertices; i+=1)
		{
			vert1 = GetVertex(i);
			vert2 = GetVertex(i + 1);
			
			graphics.drawLine(vert1.GetXInt() - cameraOffset.GetXInt(), vert1.GetYInt() - cameraOffset.GetYInt(),
							  vert2.GetXInt() - cameraOffset.GetXInt(), vert2.GetYInt() - cameraOffset.GetYInt());
		}
	}
	
	@Override
	public String toString()
	{
		String result = "Vertices: ";
		
		for (HWVec2 vert : m_Vertices)
		{
			result += vert + ", ";
		}
		
		return result;
	}
}
