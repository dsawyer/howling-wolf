package com.howlingwolf.util.math;

// Java imports
import java.awt.Point;

/**
 * Models a 2D vector.
 * 
 * @author Dylan Sawyer
 */
public class HWVec2
{
	private float x;
	private float y;
	
	/**
	 * Creates a new empty 2D vector.
	 */
	public HWVec2()
	{
		this(0.0f, 0.0f);
	}
	
	/**
	 * Creates a new 2D vector with the given values.
	 * 
	 * @param p_x x value for the 2D vector
	 * @param p_y y value for the 2D vector
	 */
	public HWVec2(float p_x, float p_y)
	{
		x = p_x;
		y = p_y;
	}
	
	/**
	 * Creates a new 2D vector based off the coordinates of a Point object.
	 * 
	 * @param p_point The point object with the desired coordinates
	 */
	public HWVec2(Point p_point)
	{
		x = p_point.x;
		y = p_point.y;
	}
	
	/**
	 * Creates and returns a copy of this 2D vector.
	 * 
	 * @return A duplicate of this 2D vector
	 */
	public HWVec2 Clone()
	{
		return new HWVec2(x, y);
	}

	/**
	 * Checks to see if two vectors have the same values.
	 * 
	 * @param vec	The vector being tested against this one
	 * @return		True if the two vectors are equal, false otherwise
	 */
	public boolean Equals(HWVec2 vec)
	{
		return ((GetX() == vec.GetX()) && (GetY() == vec.GetY()));
	}
	
	/**
	 * Returns a 2D vector with 0 magnitude.
	 * 
	 * @return A 2D vector with 0 for both values, thus having 0 magnitude
	 */
	public static HWVec2 ZeroVector()
	{
		return new HWVec2(0, 0);
	}
	
	/**
	 * Changes both values of this 2D vector.
	 * 
	 * @param p_x The new x value of this 2D vector
	 * @param p_y The new y value of this 2D vector
	 */
	public void Set(float p_x, float p_y)
	{
		SetX(p_x);
		SetY(p_y);
	}
	
	/**
	 * Changes both values of this 2D vector.
	 * 
	 * @param vec The new values for this 2D vector
	 */
	public void Set(HWVec2 vec)
	{
		SetX(vec.GetX());
		SetY(vec.GetY());
	}
	
	/**
	 * Sets the values of this vector based on the coordinates of a Point object.
	 * 
	 * @param p_point The new values for this 2D vector
	 */
	public void Set(Point p_point)
	{
		Set(p_point.x, p_point.y);
	}
	
	/**
	 * Converts this 2D vector into a Point object. Useful for dealing with
	 * native Java code.
	 * 
	 * @return A Point object version of this 2D vector
	 */
	public Point GetPoint()
	{
		return new Point(GetXInt(), GetYInt());
	}
	
	/**
	 * Modifies both values of this 2D vector.
	 * 
	 * @param p_x The amount to modify the x value of this 2D vector
	 * @param p_y The amount to modify the y value of this 2D vector
	 */
	public void Add(float p_x, float p_y)
	{
		SetX(p_x + GetX());
		SetY(p_y + GetY());
	}
	
	/**
	 * Modifies both values of this 2D vector.
	 * 
	 * @param add The amount to add to both values of this 2D vector
	 */
	public void Add(HWVec2 add)
	{
		SetX(add.GetX() + GetX());
		SetY(add.GetY() + GetY());
	}
	
	/**
	 * Adds the values of a Point object to this 2D vector.
	 * 
	 * @param p_point The amount to add to both values of this 2D vector
	 */
	public void Add(Point p_point)
	{
		Add(p_point.x, p_point.y);
	}
	
	/**
	 * Scales both values of this vector by a given amount.
	 * 
	 * @param scale The amount with which both values are multiplied
	 */
	public void Scale(float scale)
	{
		SetX(scale * GetX());
		SetY(scale * GetY());
	}
	
	/**
	 * Gets the x value of this 2D vector.
	 * 
	 * @return x value
	 */
	public float GetX()
	{
		return x;
	}
	
	/**
	 * Gets the integer representation of the x value of this vector.
	 * 
	 * @return x value
	 */
	public int GetXInt()
	{
		return (int)x;
	}
	
	/**
	 * Sets the x value of this 2D vector.
	 * 
	 * @param p_x The new x value
	 */
	public void SetX(float p_x)
	{
		x = p_x;
	}
	
	/**
	 * Gets the y value of this 2D vector.
	 * 
	 * @return y value
	 */
	public float GetY()
	{
		return y;
	}
	
	/**
	 * Gets the integer representation of the y value of this vector.
	 * 
	 * @return y value
	 */
	public int GetYInt()
	{
		return (int)y;
	}
	
	/**
	 * Sets the y value of this 2D vector.
	 * 
	 * @param p_y The new y value
	 */
	public void SetY(float p_y)
	{
		y = p_y;
	}
	
	/**
	 * Gets the length of this 2D vector.
	 * 
	 * @return The length of this 2D vector
	 */
	public float Magnitude()
	{
		return (float)Math.sqrt(MagnitudeSquared());
	}
	
	/**
	 * Gets the squared length of this 2D vector. Useful to avoid the need for
	 * the costly square root function.
	 * 
	 * @return The squared length of this vector's magnitude
	 */
	public float MagnitudeSquared()
	{
		return (GetX() * GetX()) + (GetY() * GetY());
	}
	
	/**
	 * Normalises the magnitude of the vector.
	 */
	public void Normalise()
	{
		Scale(1 / Magnitude());
	}
	
	/**
	 * Provides a normalised version of this vector.
	 * 
	 * @return A unit (normalised) version of this vector
	 */
	public HWVec2 UnitVector()
	{
		HWVec2 retVec = this.Clone();
		retVec.Normalise();
		return retVec;
	}
	
	@Override
	public String toString()
	{
		return ("(" + x + ", " + y + ")");
	}
}