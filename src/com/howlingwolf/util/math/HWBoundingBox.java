package com.howlingwolf.util.math;

/**
 * Models an Axis Aligned Bounding Box.
 * 
 * @author Dylan Sawyer
 */
public class HWBoundingBox 
{
	private HWVec2 m_Origin;
	private HWVec2 m_Size;
	
	/**
	 * Creates a new empty bounding box.
	 */
	public HWBoundingBox()
	{
		this(new HWVec2(), new HWVec2());
	}
	
	/**
	 * Creates a bounding box with the given upper left and lower right
	 * coordinates.
	 * 
	 * @param origin	Upper left coordinate
	 * @param size		The size of the box
	 */
	public HWBoundingBox(HWVec2 origin, HWVec2 size)
	{
		this(origin.GetX(), origin.GetY(), size.GetX(), size.GetY());
	}
	
	/**
	 * Creates a new bounding box with the given size.
	 * 
	 * @param x			The x coordinate of the upper left corner
	 * @param y			The y coordinate of the upper left corner
	 * @param width		The width of the bounding box
	 * @param height	The height of the bounding box
	 */
	public HWBoundingBox(float x, float y, float width, float height)
	{
		m_Origin = new HWVec2(x, y);
		m_Size = new HWVec2(width, height);
	}
	
	/**
	 * Creates a copy of the bounding box.
	 * 
	 * @return	An exact duplicate of this HWBoundingBox
	 */
	public HWBoundingBox Clone()
	{
		return new HWBoundingBox(m_Origin, m_Size);
	}
	
	/**
	 * Sets the values of the bounding box with the minimum and maximum x and y
	 * values for the bounds.
	 * 
	 * @param x			The x value of the upper left corner
	 * @param y			The y value of the upper left corner
	 * @param width		The width of the bounding box
	 * @param height	The height of the bounding box
	 */
	public void Set(float x, float y, float width, float height)
	{
		m_Origin.Set(x, y);
		m_Size.Set(width, height);
	}
	
	/**
	 * Gets the position of the upper left corner of the bounding box.
	 * 
	 * @return	The bounding box's origin
	 */
	public HWVec2 GetPosition()
	{
		return m_Origin;
	}
	
	/**
	 * Sets the origin to a new position.
	 * 
	 * @param x	The x value of the origin
	 * @param y	The y value of the origin
	 */
	public void SetPosition(float x, float y)
	{
		m_Origin.Set(x, y);
	}
	
	/**
	 * Sets the origin to a new position.
	 * 
	 * @param pos	The new position of the origin
	 */
	public void SetPosition(HWVec2 pos)
	{
		m_Origin.Set(pos);
	}
	
	/**
	 * Gets the x value for the upper left coordinate.
	 * 
	 * @return	The smallest x value
	 */
	public float GetLeft()
	{
		return m_Origin.GetX();
	}
	
	/**
	 * Gets the x value for the lower right coordinate.
	 * 
	 * @return	The largest x value
	 */
	public float GetRight()
	{
		return GetLeft() + m_Size.GetX();
	}
	
	/**
	 * Gets the y value for the upper left coordinate.
	 * 
	 * @return	The smallest y value
	 */
	public float GetTop()
	{
		return m_Origin.GetY();
	}
	
	/**
	 * Gets the y value for the lower right coordinate.
	 * 
	 * @return	The largest y value
	 */
	public float GetBottom()
	{
		return m_Origin.GetY() + m_Size.GetY();
	}
	
	/**
	 * Gets the size of the bounding box.
	 * 
	 * @return	A HWVec2 that defines the width and height of the box as it's
	 *			x and y values respectively
	 */
	public HWVec2 GetSize()
	{
		return m_Size;
	}
	
	/**
	 * Sets the size of the bounding box.
	 * 
	 * @param size	The new size of the bounding box
	 */
	public void SetSize(HWVec2 size)
	{
		m_Size.Set(size);
	}
        
    /**
	 * Gets the width of the bounding box.
	 * 
	 * @return	The width of this bounding box
	 */
	public float GetWidth()
	{
		return m_Size.GetX();
	}
	
	/**
	 * Gets the height of the bounding box.
	 * 
	 * @return	The height of this bounding box
	 */
	public float GetHeight()
	{
		return m_Size.GetY();
	}
	
	/**
	 * Finds the centre of the bounding box by adding the half width to the
	 * origin's x value and half height to the y value.
	 * 
	 * @return	The centre of the bounding box
	 */
	public HWVec2 FindCentre()
	{
		HWVec2 centre = m_Origin.Clone();
		
		centre.SetX(GetLeft() + (m_Size.GetX() / 2));
		centre.SetY(GetTop() + (m_Size.GetY() / 2));
		
		return centre;
	}
}