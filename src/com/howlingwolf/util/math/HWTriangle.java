package com.howlingwolf.util.math;

/**
 *
 * @author Dylan Sawyer
 */
public class HWTriangle extends HWPolygon
{
	private static final int NUM_VERTICES = 3;
	
	/**
	 * Creates a new, default (empty) triangle.
	 */
	public HWTriangle()
	{
		super(NUM_VERTICES);
	}
	
	/**
	 * Creates a new triangle with the given vertices.
	 * 
	 * @param vertices	The list of vertices that are a part of this triangle
	 */
	public HWTriangle(HWVec2[] vertices)
	{
		super(vertices, NUM_VERTICES);
	}
	
	@Override
	public HWTriangle Clone()
	{
		return new HWTriangle(GetVertices());
	}
}
