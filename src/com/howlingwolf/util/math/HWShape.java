package com.howlingwolf.util.math;

// Java imports
import java.awt.Graphics2D;

/**
 * This just forces a shape to have a draw method.
 * 
 * @author Dylan Sawyer
 */
public interface HWShape
{
	
	/**
	 * Checks to see if the given point is located within the bounds of the
	 * shape.
	 * 
	 * @param point	The point to be tested
	 * @return		True if the point is inside the shape, false otherwise
	 */
	public boolean ContainsPoint(HWVec2 point);

	/**
	 * Draws the shape to the provided graphics context.
	 * Mainly useful for debugging.
	 * 
	 * @param graphics	The graphics context to draw upon
	 */
	public void Draw(Graphics2D graphics);
}
