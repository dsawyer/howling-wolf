package com.howlingwolf.util.math;

/**
 * Models a rectangle shape.
 * 
 * @author Dylan Sawyer
 */
public class HWRectangle extends HWPolygon
{
	private static final int NUM_VERTICES = 4;
	
	private boolean m_bIsRotated;
	
	/**
	 * Creates an empty rectangle.
	 */
	public HWRectangle()
	{
		super(NUM_VERTICES);
		m_bIsRotated = false;
	}
	
	/**
	 * Creates a rectangle that is axis aligned.
	 * 
	 * @param x			The x value of the upper left coordinate
	 * @param y			The y value of the upper left coordinate
	 * @param width		The width of the rectangle
	 * @param height	The height of the rectangle
	 */
	public HWRectangle(float x, float y, float width, float height)
	{
		super(NUM_VERTICES);
		
		float maxX = x + width;
		float maxY = y + height;
		
		SetVertex(new HWVec2(x, y), 0);
		SetVertex(new HWVec2(x, maxY), 1);
		SetVertex(new HWVec2(maxX, maxY), 2);
		SetVertex(new HWVec2(maxX, y), 3);
		
		m_bIsRotated = false;
	}
	
	/**
	 * An alternate method for getting a rectangle using two vectors.
	 * 
	 * @param origin	The point at the upper left corner of the rectangle
	 * @param size		The size of the rectangle
	 */
	public HWRectangle(HWVec2 origin, HWVec2 size)
	{
		this(origin.GetX(), origin.GetY(), size.GetX(), size.GetY());
	}
	
	/**
	 * Creates a new rectangle with the given vertices.
	 * 
	 * @param vertices	The list of vertices in this rectangle
	 */
	public HWRectangle(HWVec2[] vertices)
	{
		super(vertices, NUM_VERTICES);
	}
	
	@Override
	public HWRectangle Clone()
	{
		return new HWRectangle(GetVertices());
	}
	
	/**
	 * Rotates the rectangle. Will be implemented in the HWPolygon class once
	 * matrices are added in and rotating is made simpler.
	 * 
	 * @param angle	The angle by which the rectangle should be rotated
	 */
	public void Rotate(float angle)
	{
		m_bIsRotated = true;
	}
	
	// If the rectangle is rotated, then it needs to be calculated through the
	// usual point in polygon method. If it is not rotated, just check the AABB.
	@Override
	public boolean ContainsPoint(HWVec2 point)
	{
		if (m_bIsRotated)
		{
			return super.ContainsPoint(point);
		}
		else
		{
			return WithinAABB(point);
		}
	}
}
