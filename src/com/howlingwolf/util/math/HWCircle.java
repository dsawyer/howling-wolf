package com.howlingwolf.util.math;

// Java imports
import java.awt.Graphics2D;

/**
 * Models a circle. Mostly used for physics based calculations.
 * 
 * @author Dylan Sawyer
 */
public class HWCircle implements HWShape
{
	private HWVec2 m_Centre;
	private float m_fRadius;
	
	/**
	 * Creates an empty circle.
	 */
	public HWCircle()
	{
		this(HWVec2.ZeroVector(), 0);
	}
	
	/**
	 * Creates a circle with the given radius centred at the origin.
	 * 
	 * @param p_fRadius	The radius of the circle
	 */
	public HWCircle(float p_fRadius)
	{
		this(HWVec2.ZeroVector(), p_fRadius);
	}
	
	/**
	 * Creates a circle with the given radius at the given location.
	 * 
	 * @param p_Centre	The circle's location (centre)
	 * @param p_fRadius	The circle's radius
	 */
	public HWCircle(HWVec2 p_Centre, float p_fRadius)
	{
		m_Centre = p_Centre;
		m_fRadius = p_fRadius;
	}
	
	/**
	 * Creates an exact duplicate of this object.
	 * 
	 * @return A copy of this object with the same parameters and state
	 */
	public HWCircle Clone()
	{
		return new HWCircle(m_Centre, m_fRadius);
	}
	
	/**
	 * Gets the centre of the circle.
	 * 
	 * @return	The centre of the circle
	 */
	public HWVec2 GetCentre()
	{
		return m_Centre;
	}
	
	/**
	 * Gets the upper left corner of the square formed that perfectly contains
	 * the circle.
	 * 
	 * @return The upper left corner of the enclosing square
	 */
	public HWVec2 GetUpperLeft()
	{
		return new HWVec2((m_Centre.GetX() - m_fRadius), (m_Centre.GetY() - m_fRadius));
	}
	
	/**
	 * Gets the radius of the circle.
	 * 
	 * @return The size of the circle
	 */
	public float GetRadius()
	{
		return m_fRadius;
	}
	
	@Override
	public boolean ContainsPoint(HWVec2 point)
	{
		// Just get the distance squared to eliminate the need to calculate
		// a square root (which is very expensive)
		float distanceSquared = HWMathFunctions.DistanceSquared(GetCentre(), point);
		
		return distanceSquared < (m_fRadius * m_fRadius);
	}
	
	@Override
	public void Draw(Graphics2D graphics)
	{
		HWVec2 ul = GetUpperLeft();
		
		graphics.drawOval(ul.GetXInt(), ul.GetYInt(), (int)m_fRadius * 2, (int)m_fRadius * 2);
	}
}
