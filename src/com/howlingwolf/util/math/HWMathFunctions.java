package com.howlingwolf.util.math;

/**
 * This just contains various functions to be used with the defined classes
 * within this package.
 * 
 * @author Dylan Sawyer
 */
public class HWMathFunctions
{
	private static final float FLOAT_TOLERANCE = 0.01f;
	
	/**
	 * Provides a 2D vector that results from adding 2 vectors together. This is
	 * simply done by adding together the x values of the 2 vectors to get the
	 * x value for the resulting vector, and then doing the same for the y
	 * values.
	 * 
	 * @param vec1	The first vector
	 * @param vec2	The second vector
	 * @return		The vector resulting from adding two vectors together
	 */
	public static HWVec2 AddVectors(HWVec2 vec1, HWVec2 vec2)
	{
		HWVec2 retVec = new HWVec2();
		
		retVec.SetX(vec1.GetX() + vec2.GetX());
		retVec.SetY(vec1.GetY() + vec2.GetY());
		
		return retVec;
	}
	
	/**
	 * Provides a 2D vector that results from subtracting one vector from
	 * another. This is simply done by subtracting the x values of the 2
	 * vectors to get the x value for the resulting vector, and then doing the
	 * same for the y values.
	 * 
	 * @param vec1	The first vector
	 * @param vec2	The second vector
	 * @return		The vector resulting from subtracting the second vector
	 *				from the first one
	 */
	public static HWVec2 SubtractVectors(HWVec2 vec1, HWVec2 vec2)
	{
		HWVec2 retVec = new HWVec2();
		
		retVec.SetX(vec1.GetX() - vec2.GetX());
		retVec.SetY(vec1.GetY() - vec2.GetY());
		
		return retVec;
	}
	
	/**
	 * This calculates the distance between two vectors that are being treated
	 * as points. In essence, this calculates the vector between two points and
	 * then calculates said vector's magnitude.
	 * 
	 * @param vec1	The first point
	 * @param vec2	The second point
	 * @return		The distance between the two points
	 */
	public static float Distance(HWVec2 vec1, HWVec2 vec2)
	{
		float dx = vec1.GetX() - vec2.GetX();
		float dy = vec1.GetY() - vec2.GetY();
		
		return (float)Math.sqrt((dx * dx) + (dy * dy));
	}
	
	/**
	 * This calculates the squared distance between two vectors that are being
	 * treated as points, eliminating the costly square root function. In
	 * essence, this calculates the vector between two points and then
	 * calculates said vector's squared magnitude.
	 * 
	 * @param vec1	The first point
	 * @param vec2	The second point
	 * @return		The squared distance between the two points
	 */
	public static float DistanceSquared(HWVec2 vec1, HWVec2 vec2)
	{
		float dx = vec1.GetX() - vec2.GetX();
		float dy = vec1.GetY() - vec2.GetY();
		
		return (dx * dx) + (dy * dy);
	}
	
	/**
	 * This calculates the dot product between two vectors. This results in a
	 * scalar value that is equal to the product of the magnitudes of each
	 * vector and the cosine of the angle between them.
	 * 
	 * @param vec1	The first vector
	 * @param vec2	The second vector
	 * @return		The scalar product of the two vectors
	 */
	public static float DotProduct(HWVec2 vec1, HWVec2 vec2)
	{
		float x = vec1.GetX() * vec2.GetX();
		float y = vec1.GetY() * vec2.GetY();
		
		return x + y;
	}
	
	/**
	 * This is a modified version of the dot product where both vectors are
	 * first normalised, reducing their magnitudes to 1. This then calculates
	 * the dot product with these unit vectors, and inverts the cosine value
	 * returned to calculate the angle between the vectors (in radians).
	 * 
	 * @param vec1	The first vector
	 * @param vec2	The second vector
	 * @return		The cosine of the angle between two vectors
	 */
	public static float AngleBetweenVectors(HWVec2 vec1, HWVec2 vec2)
	{
		HWVec2 unit1 = vec1.UnitVector();
		HWVec2 unit2 = vec2.UnitVector();
		
		float cosA = DotProduct(unit1, unit2);
		
		return (float)Math.acos(cosA);
	}
	
	/**
	 * This calculates the area contained within a parallelogram between the two
	 * vectors. This uses a simplified version of the cross product, which
	 * is used to find a vector that is perpendicular to the plane made by the
	 * two vectors. This resulting vector's magnitude happens to be the area of
	 * the parallelogram.
	 * <p>
	 * Normally, the cross product is used between 3D vectors, so simulating
	 * that with 2D vectors involves their Z values being 0, which renders the
	 * resulting 3D vector's X and Y values 0. The calculation that gets the
	 * resulting Z value is thus it's magnitude, and by extension, the size of
	 * the area between the two vectors.
	 * 
	 * @param vec1	The first vector
	 * @param vec2	The second vector
	 * @return		The are between the two vectors
	 */
	public static float AreaBetweenVectors(HWVec2 vec1, HWVec2 vec2)
	{
		return Math.abs(CrossProduct(vec1, vec2));
	}
	
	/**
	 * Gets the cross product between two 2D vectors.
	 * 
	 * @param vec1	The first vector
	 * @param vec2	The second vector
	 * @return		The cross product of the two vectors
	 */
	public static float CrossProduct(HWVec2 vec1, HWVec2 vec2)
	{
		return ((vec1.GetX() * vec2.GetY()) - (vec1.GetY() * vec2.GetX()));
	}

	/**
	 * Checks to see if a point is within the bounds of a line (used in tandem
	 * with the segment intersection stuff). Not to be used on it's own!
	 * 
	 * @param point			The point being checked
	 * @param linePoint1	The first endpoint of the line segment
	 * @param linePoint2	The second endpoint of the line segment
	 * @return				True if the point is on the line, false otherwise
	 */
	public static boolean PointIsOnLineSegment(HWVec2 point, HWVec2 linePoint1, HWVec2 linePoint2)
	{
		return (Math.min(linePoint1.GetX() - FLOAT_TOLERANCE, linePoint2.GetX() - FLOAT_TOLERANCE) < point.GetX()) &&
			   (Math.max(linePoint1.GetX() + FLOAT_TOLERANCE, linePoint2.GetX() + FLOAT_TOLERANCE) > point.GetX()) &&
			   (Math.min(linePoint1.GetY() - FLOAT_TOLERANCE, linePoint2.GetY() - FLOAT_TOLERANCE) < point.GetY()) &&
			   (Math.max(linePoint1.GetY() + FLOAT_TOLERANCE, linePoint2.GetY() + FLOAT_TOLERANCE) > point.GetY());
	}
	
	/**
	 * Gets the point at which two lines intersect. Uses two pairs of points
	 * to define their respective lines.
	 * 
	 * @param line1Point1	The first point on the first line
	 * @param line1Point2	The second point on the first line
	 * @param line2Point1	The first point on the second line
	 * @param line2Point2	The second point on the second line
	 * @return				The point at which the two lines intersect, or null
	 *						if they are parallel
	 */
	public static HWVec2 LinesIntersection(HWVec2 line1Point1, HWVec2 line1Point2,
										  HWVec2 line2Point1, HWVec2 line2Point2)
	{
		// First, use the points of the lines to get the A*x + B*y = C formula
		// for that line.
		// A = y2 - y1
		// B = x1 - x2
		// C = A*x1 + B*y1
		
		float line1A = line1Point2.GetY() - line1Point1.GetY();
		float line1B = line1Point1.GetX() - line1Point2.GetX();
		
		float line2A = line2Point2.GetY() - line2Point1.GetY();
		float line2B = line2Point1.GetX() - line2Point2.GetX();
		
		float denom = (line1A * line2B) - (line2A * line1B);
		
		// The lines do not intersect if they are parallel (technically, they
		// intersect either 0 or infinite times), but for the purpose of this
		// algorithm, we'll count it as not intersecting.
		if (denom == 0.0f) // Lines are parallel
		{
			return null;
		}
		
		// We only calculate the C values of the equations here, as we need
		// to check the denominator for a divide by zero (hence parallel
		// lines -> 0 or infinite intersections). Put off the calculation
		// to save time on useless calculations.
		float line1C = (line1A * line1Point1.GetX()) + (line1B * line1Point1.GetY());
		float line2C = (line2A * line2Point1.GetX()) + (line2B * line2Point1.GetY());
		
		float intersectX = ((line2B * line1C) - (line1B * line2C)) / denom;
		float intersectY = ((line1A * line2C) - (line2A * line1C)) / denom;
		
		return new HWVec2(intersectX, intersectY);
	}
	
	/**
	 * A modified version of the previous LinesIntersection method. This one is
	 * used when one line is being tested against multiple ones, to allow the
	 * A, B, and C values of the line formula to be calculated just the once.
	 * A small optimisation.
	 * 
	 * @param line1A		The first line's A value
	 * @param line1B		The first line's B value
	 * @param line1C		The first line's C value
	 * @param line2Point1	The first point on the second line
	 * @param line2Point2	The second point on the second line
	 * @return				The point at which the two lines intersect, or null
	 *						if they are parallel
	 */
	public static HWVec2 LinesIntersection(float line1A, float line1B, float line1C,
										   HWVec2 line2Point1, HWVec2 line2Point2)
	{
		// First, use the points of the lines to get the A*x + B*y = C formula
		// for that line.
		// A = y2 - y1
		// B = x1 - x2
		// C = A*x1 + B*y1
		
		float line2A = line2Point2.GetY() - line2Point1.GetY();
		float line2B = line2Point1.GetX() - line2Point2.GetX();
		
		float denom = (line1A * line2B) - (line2A * line1B);
		
		// The lines do not intersect if they are parallel (technically, they
		// intersect either 0 or infinite times), but for the purpose of this
		// algorithm, we'll count it as not intersecting.
		if (denom == 0.0f) // Lines are parallel
		{
			return null;
		}
		
		// We only calculate the C values of the equations here, as we need
		// to check the denominator for a divide by zero (hence parallel
		// lines -> 0 or infinite intersections). Put off the calculation
		// to save time on useless calculations.
		float line2C = (line2A * line2Point1.GetX()) + (line2B * line2Point1.GetY());
		
		float intersectX = ((line2B * line1C) - (line1B * line2C)) / denom;
		float intersectY = ((line1A * line2C) - (line2A * line1C)) / denom;

		return new HWVec2(intersectX, intersectY);
	}
	
	/**
	 * Checks to see if a line (defined by two points) intersects a circle.
	 * 
	 * @param circle	The circle being checked
	 * @param point1	The first point on the line
	 * @param point2	The second point on the line
	 * @return			True if the line does intersect the circle, false
	 *					otherwise
	 */
	public static boolean DoesLineIntersectCircle(HWCircle circle, HWVec2 point1, HWVec2 point2)
	{
		HWVec2 point1Translated = SubtractVectors(point1, circle.GetCentre());
		HWVec2 point2Translated = SubtractVectors(point2, circle.GetCentre());
		
		float dx = point2Translated.GetX() - point1Translated.GetX();
		float dy = point2Translated.GetY() - point1Translated.GetY();
		float drSquared = (dx * dx) + (dy * dy);
		
		float determinant = (point1Translated.GetX() * point2Translated.GetY()) - (point2Translated.GetX() * point1Translated.GetY());
		
		float discriminant = ((circle.GetRadius() * circle.GetRadius()) * drSquared) - (determinant * determinant);
		
		return (discriminant >= 0);
	}
	
	/**
	 * Checks to see if the line segment (defined by two points) intersects
	 * with the given circle. This involves checking first to see if the
	 * (infinite) line defined by the two points intersects the circle. If so,
	 * then a check is performed to see if either the points are within the
	 * circle. If not, one final check is done to see if the closest point on
	 * the line that intersects the circle is between the endpoints.
	 * 
	 * @param circle	The circle being checked
	 * @param point1	The first point on the line
	 * @param point2	The second point on the line
	 * @return			True if the segment intersects the circle, false
	 *					otherwise
	 */
	public static boolean DoesLineSegmentIntersectCircle(HWCircle circle, HWVec2 point1, HWVec2 point2)
	{
		if (DoesLineIntersectCircle(circle, point1, point2))
		{
			// Do extra checks to make sure the line segment itself intersects the circle
			
			// Check to see if the end points of the segment are in the circle
			if (circle.ContainsPoint(point1) || circle.ContainsPoint(point2))
			{
				return true;
			}
			
			HWVec2 deltaVec = SubtractVectors(point2, point1);
			
			float dot1 = DotProduct(deltaVec, point1);
			float dot2 = DotProduct(deltaVec, point2);
			float dotC = DotProduct(deltaVec, circle.GetCentre());
			
			return ((dot1 < dotC) && (dotC < dot2)) ||
				   ((dot2 < dotC) && (dotC < dot1));
		}
		
		return false;
	}
	
	/**
	 * Checks to see if there is an intersection between a circle and a polygon.
	 * This is essentially a first check for collision detection.
	 * 
	 * @param circle	The circle being checked
	 * @param polygon	The polygon being checked
	 * @return			True if the two shapes intersect, false otherwise
	 */
	public static boolean DoesCircleIntersectPolygon(HWCircle circle, HWPolygon polygon)
	{
		// Check to see if the circle's centre is inside the polygon
		if (polygon.ContainsPoint(circle.GetCentre()))
		{
			return true;
		}
		
		// Check if any of the polygon's lines intersect the circle
		for (int i = 0; i < polygon.GetNumVertices(); i+=1)
		{
			if (DoesLineSegmentIntersectCircle(circle, polygon.GetVertex(i), polygon.GetVertex(i + 1)))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Uses a projection method to test each polygon's edge normals to see if
	 * projecting the polygons along that axis causes them to overlap. If they
	 * overlap on all of the projected axis, then they have intersected.
	 * <p>
	 * Refer to http://elancev.name/oliver/2D%20polygon.htm for full details
	 * on the algorithm, as well as more implementations that will be done
	 * in the collision manager (resolving collisions based on depth, using
	 * projected velocities, etc.)
	 * 
	 * @param polyA	The first polygon to be tested
	 * @param polyB	The second polygon to be tested
	 * @return		True if the polygons intersect, false otherwise
	 */
	public static boolean DoPolygonsIntersect(HWPolygon polyA, HWPolygon polyB)
	{
		HWVec2 edge, normal;
		
		// Check to see if projecting the polygons on the normals of each edge
		// from polygon A seperates the polygons along that axis
		for (int i = 0; i < polyA.GetNumVertices(); i+=1)
		{
			edge = SubtractVectors(polyA.GetVertex(i), polyA.GetVertex(i+1));
			normal = new HWVec2(-edge.GetY(), edge.GetX());
			
			if (DoesAxisSeperatePolygons(normal, polyA, polyB))
			{
				return false;
			}
		}
		
		// Check to see if projecting the polygons on the normals of each edge
		// from polygon B seperates the polygons along that axis
		for (int i = 0; i < polyB.GetNumVertices(); i+=1)
		{
			edge = SubtractVectors(polyB.GetVertex(i), polyB.GetVertex(i+1));
			normal = new HWVec2(-edge.GetY(), edge.GetX());
			
			if (DoesAxisSeperatePolygons(normal, polyA, polyB))
			{
				return false;
			}
		}
		
		// The polygons are not seperated, and thus intersect
		return true;
	}
	
	// Helper method for use with the DoPolygonsIntersect method
	private static boolean DoesAxisSeperatePolygons(HWVec2 axis, HWPolygon polyA, HWPolygon polyB)
	{
		float minA, minB, maxA, maxB;
		float dot = DotProduct(axis, polyA.GetVertex(0));
		minA = maxA = dot;
		
		// Calculate the maximum and minimum values of the interval when
		// polygon A is projected onto the line defined by axis
		for (int i = 1; i < polyA.GetNumVertices(); i+=1)
		{
			dot = DotProduct(axis, polyA.GetVertex(i));
			
			if (dot < minA)
			{
				minA = dot;
			}
			else if (dot > maxA)
			{
				maxA = dot;
			}
		}
		
		dot = DotProduct(axis, polyB.GetVertex(0));
		minB = maxB = dot;
		
		// Calculate the maximum and minimum values of the interval when
		// polygon B is projected onto the line defined by axis
		for (int i = 1; i < polyB.GetNumVertices(); i+=1)
		{
			dot = DotProduct(axis, polyB.GetVertex(i));
			
			if (dot < minB)
			{
				minB = dot;
			}
			else if (dot > maxB)
			{
				maxB = dot;
			}
		}
		
		// If the intervals overlap, then the axis does not seperate the polygons
		return ((minA > maxB) || (minB > maxA));
	}
}