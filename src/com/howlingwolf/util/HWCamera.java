package com.howlingwolf.util;

// Math imports
import com.howlingwolf.util.math.HWVec2;
import com.howlingwolf.util.math.HWBoundingBox;
import com.howlingwolf.util.math.HWRectangle;

/**
 * Models a camera for a 2D perspective. Keeps track of where in an area
 * it is, as well as the size of the viewport.
 * <p>
 There is only ever one HWCamera object at a time.
 * 
 * @author Dylan Sawyer
 */
public final class HWCamera
{
	//*************************************************************************
	// Member Variables
	//*************************************************************************
	
	// The singleton instance of the HWCamera
	private static HWCamera instance;

	// HWSize of the HWCamera view (in pixels)
	private final HWBoundingBox m_CameraView;
	// HWSize of the area the HWCamera is looking at (in pixels)
	private final HWVec2 m_WorldSize;

	//*************************************************************************
	// Constants
	//*************************************************************************

	/**
	 * The internal width used by the camera.
	 */
	public static final int CAMERA_WIDTH = 512;
	/**
	 * The internal height used by the camera.
	 */
	public static final int CAMERA_HEIGHT = 288;

	private static final int DEFAULT_OFFSET = 0;

	//*************************************************************************
	// Methods
	//*************************************************************************

	// Constructor
	// Creates a basic HWCamera
	private HWCamera()
    {
    	m_WorldSize = new HWVec2(CAMERA_WIDTH, CAMERA_HEIGHT);
    	m_CameraView = new HWBoundingBox(DEFAULT_OFFSET, DEFAULT_OFFSET, CAMERA_WIDTH, CAMERA_HEIGHT);
    }
	
	/**
	 * Gets a handle to the instance of the HWCamera. If the HWCamera has not yet
	 * been initialised, then it creates a new camera.
	 * 
	 * @return A handle to the HWCamera
	 */
	public static synchronized HWCamera Handle()
    {
    	if (instance == null)
    	{
    		instance = new HWCamera();
    	}

    	return instance;
    }
	
    /**
	 * Moves the camera around the area. Positive GetX values move it right,
	 * negative values move it left. Positive GetY values move it down, negative
	 * values move it up. Based on the origin (0,0) being in the upper left
	 * corner of the screen.
	 * 
	 * @param offset A HWVec2 containing the distance to move the camera
	 *				 from it's current position
	 */
	public void Move(HWVec2 offset)
    {
    	SetPosition(m_CameraView.GetLeft() + offset.GetX(), m_CameraView.GetTop() + offset.GetY());
    }

    /**
	 * Gives the position on screen of a HWVec2 from world coordinates.
	 * 
	 * @param worldLocation The coordinates in World space
	 * @return A HWVec2 representing the coordinates in Screen space
	 */
	public HWVec2 WorldToScreen(HWVec2 worldLocation)
    {
    	return new HWVec2((worldLocation.GetX() - m_CameraView.GetLeft()),
							(worldLocation.GetY() - m_CameraView.GetTop()));
    }
    /**
	 * Gives the position in world coordinates of a HWVec2 on screen
	 * 
	 * @param screenLocation The coordinates in Screen space
	 * @return A HWVec2 representing the coordinates in World space
	 */
	public HWVec2 ScreenToWorld(HWVec2 screenLocation)
    {
    	return new HWVec2((screenLocation.GetX() + m_CameraView.GetLeft()),
							(screenLocation.GetY() + m_CameraView.GetTop()));
    }

    /**
	 * Gets the position of the HWCamera in the area (world).
	 * 
	 * @return HWCamera's position
	 */
	public HWVec2 GetPosition()
    {
    	return m_CameraView.GetPosition();
    }
	
	/**
	 * Modifies the position of the HWCamera within the bounds of the area (world).
	 * 
	 * @param point The coordinates for the HWCamera's new position
	 */
	public void SetPosition(HWVec2 point)
	{
		SetPosition(point.GetX(), point.GetY());
	}

    /**
	 * Modifies the position of the HWCamera within the bounds of the area (world).
	 * 
	 * @param x GetX coordinate of the HWCamera's new position
	 * @param y GetY coordinate of the HWCamera's new position
	 */
	public void SetPosition(float x, float y)
    {
		float newX = HWExtraFunctions.Clamp(x, 0, Math.abs(m_WorldSize.GetX() - m_CameraView.GetWidth()));
		float newY = HWExtraFunctions.Clamp(y, 0, Math.abs(m_WorldSize.GetY() - m_CameraView.GetHeight()));
		
		m_CameraView.SetPosition(new HWVec2(newX, newY));
    }
	
	/**
	 * Gets the size of the world.
	 * 
	 * @return World's size
	 */
	public HWVec2 GetWorldSize()
	{
		return m_WorldSize;
	}
	
	/**
	 * Modifies the size of the world.
	 * 
	 * @param size The size of the world
	 */
	public void SetWorldSize(HWVec2 size)
	{
		m_WorldSize.SetX(size.GetXInt());
		m_WorldSize.SetY(size.GetYInt());
	}
	
	/**
	 * Gets the size of the HWCamera's viewport.
	 * 
	 * @return HWSize of the HWCamera's viewport
	 */
	public HWVec2 GetCameraSize()
	{
		return m_CameraView.GetSize();
	}
	
	/**
	 * Gets the BoundingBox that defines the viewport for the camera.
	 * 
	 * @return	HWBoundingBox that defines the viewport
	 */
	public HWBoundingBox GetCameraView()
	{
		return m_CameraView;
	}
	
	public HWRectangle GetCameraRect()
	{
		return new HWRectangle(m_CameraView.GetPosition(), m_CameraView.GetSize());
	}
	
	/**
	 * Modifies the size of the HWCamera's viewport. Checks to make sure that
	 * the width and height remain within a particular set of bounds.
	 * <p>
	 * GetWidth can never exceed 1920 pixels.
	 * <p>
	 * GetHeight can never exceed 1080 pixels.
	 * 
	 * @param size The new size of the HWCamera's viewport
	 */
	public void SetCameraSize(HWVec2 size)
	{
		m_CameraView.SetSize(size);
	}
}