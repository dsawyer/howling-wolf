package com.howlingwolf.graphics;

// Math imports
import com.howlingwolf.util.math.HWVec2;

/**
 * An interface used by any object that is used as a light source in an area.
 * 
 * @author Dylan Sawyer
 */
public interface HWLightSource
{

	/**
	 * Gets the position for the origin of the light source.
	 * 
	 * @return The point from which the light emanates
	 */
	public HWVec2 GetPosition();
	
	/**
	 * Gets the farthest distance at which the light can shine. May become
	 * unnecessary further on as the algorithm for calculating light levels
	 * is implemented.
	 * 
	 * @return	The maximum distance that the light source can affect
	 */
	public float GetEffectiveDistance();
	
	/**
	 * Gets the power of the light source. This affects how bright it's
	 * surroundings are lit, as well as how far the light will reach (brighter
	 * lights shine farther).
	 * 
	 * @return	The strength of the light source at it's origin
	 */
	public float GetLightStrength();
	
	/**
	 * A way to check if an object is an active light source.
	 * 
	 * @return	True if the object is an active light source, false otherwise
	 */
	public boolean IsActiveLightSource();
}