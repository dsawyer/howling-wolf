package com.howlingwolf.graphics.window;

// Java imports
import java.awt.Color;
import java.awt.Graphics2D;

// Core imports
import com.howlingwolf.managers.HWOptionsManager;
import com.howlingwolf.managers.HWAudioManager;
import com.howlingwolf.util.HWExtraTypes;

/**
 *
 * @author Dylan Sawyer
 */
public class HWDialogBox implements HWWindow
{
	private HWBorderedWindow m_Window;
	
	private String message;
	private String displayMessage;
	
	private long timeSinceLastChar = 0;
	
	private long displaySpeed;
	
	/**
	 * Display messages at a slow speed.
	 */
	public static final long SLOW_SPEED  = 80;
	/**
	 * Display messages at a medium speed.
	 */
	public static final long MEDIUM_SPEED  = 50;
	/**
	 * Display messages at a fast speed.
	 */
	public static final long FAST_SPEED  = 20;
	/**
	 * Display messages almost instantly.
	 */
	public static final long INSTANT_SPEED = 1;
	
	/**
	 * Creates a new DialogueBox object, which is a HWBorderedWindow with a message. Sets
	 * the message display speed to a default value.
	 * 
	 * @param x GetX position of the window on the screen
	 * @param y GetY position of the window on the screen
	 * @param width GetWidth of the window
	 * @param height GetHeight of the window
	 * @param mes The message to be displayed in the dialogue box
	 */
	public HWDialogBox(int x, int y, int width, int height, String mes)
	{
		//super(x, y, width, height);
		message = mes;
		displayMessage = "";
		// Make this get the speed set by the HWOptionsManager
		displaySpeed = HWOptionsManager.SharedManager().GetMessageSpeed();
		
		m_Window = new HWBorderedWindow(x, y, width, height);
		m_Window.ScaleWindow(3, "Window.png");
	}
	
	/**
	 * Sets the speed at which the message is displayed.
	 * 
	 * @param speed New display speed
	 */
	public void SetDisplaySpeed(long speed)
	{
		if ((speed < INSTANT_SPEED) || (speed > SLOW_SPEED))
		{
			return;
		}
		
		displaySpeed = speed;
	}
	
	/**
	 * Checks to see if the entire message has been displayed.
	 * 
	 * @return True if the entire message is displayed, false otherwise
	 */
	public boolean DisplayingFullMessage()
	{
		if (displayMessage.length() == message.length())
		{
			return true;
		}
		
		return false;
	}
	
	private void AddCharacter()
	{
		//displayMessage = message.substring(0, displayMessage.length() + 1);
		// Possibly more efficient than above?
		String charac = message.substring(displayMessage.length(), displayMessage.length() + 1);
		displayMessage += charac;
		
		if (!charac.equals(" "))
		{
			HWAudioManager.SharedManager().PlaySFX(HWExtraTypes.SOUND_DIALOG);
		}
	}
	
	/**
	 * Resets the display message, so if the dialogue box is called again, it
	 * can start over.
	 * 
	 * @param nextMessage The next message that will appear in the dialog box
	 */
	public void Reset(String nextMessage)
	{
		message = nextMessage;
		displayMessage = "";
	}
	
	@Override
	public int GetState()
	{
		return m_Window.GetState();
	}
	
	@Override
	public void SetState(int p_iState)
	{
		m_Window.SetState(p_iState);
	}
	
	@Override
	public void Draw(Graphics2D graphics)
	{
		m_Window.Draw(graphics);
		
		if (m_Window.GetState() == HWWindow.STATE_OPEN)
		{
			graphics.setColor(Color.white);
			graphics.drawString(displayMessage, m_Window.GetBounds().Left() + 32, m_Window.GetBounds().Top() + 32);
		}
	}
	
	@Override
	public void Update(long elapsedTime)
	{
		m_Window.Update(elapsedTime);
		
		if (m_Window.GetState() == HWWindow.STATE_OPEN)
		{
			if (displayMessage.length() != message.length())
			{
				timeSinceLastChar += elapsedTime;

				while (timeSinceLastChar >= displaySpeed)
				{
					timeSinceLastChar -= displaySpeed;
					AddCharacter();
					
					if (DisplayingFullMessage())
					{
						timeSinceLastChar = 0;
						break;
					}
				}
			}
		}
	}
}
