package com.howlingwolf.graphics.window;

// Java imports
import java.util.ArrayList;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

// Physics imports
import com.howlingwolf.util.math.HWVec2;

/**
 * Contains a group of labels and images to display to the player.
 * 
 * @author Dylan Sawyer
 */
public class HWDisplayWindow extends HWBorderedWindow
{
	private ArrayList<WindowLabel> m_Labels;
	private ArrayList<WindowImage> m_Images;
	
	/**
	 * Creates a new DisplayHWBorderedWindow with the given size.
	 * 
	 * @param x GetX position of the window on the screen
	 * @param y GetY position of the window on the screen
	 * @param width GetWidth of the window
	 * @param height GetHeight of the window
	 */
	public HWDisplayWindow(int x, int y, int width, int height)
	{
		super(x, y, width, height);
		
		m_Labels = new ArrayList<>();
		m_Images = new ArrayList<>();
	}
	
	
	/**
	 * Adds a set of text to the window.
	 * 
	 * @param x GetX position of the text relative to the window
	 * @param y GetY position of the text relative to the window
	 * @param label The text to be displayed
	 */
	public void AddLabel(int x, int y, String label)
	{
		m_Labels.add(new WindowLabel(x, y, label));
	}
	
	/**
	 * Adds an image to the window.
	 * 
	 * @param x GetX position of the image relative to the window
	 * @param y GetY position of the image relative to the window
	 * @param image The image to be displayed
	 */
	public void AddImage(int x, int y, BufferedImage image)
	{
		m_Images.add(new WindowImage(x, y, image));
	}
	
	@Override
	public void Draw(Graphics2D graphics)
	{
		super.Draw(graphics);
		
		// If the window is open
		/*if (m_iOpenState == HWBorderedWindow.OPEN)
		{
			// Draw all the text
			for (WindowLabel label : m_Labels)
			{
				graphics.drawString(label.GetLabel(),
						(int)m_Bounds.Left() + label.GetOffset().GetX(),
						(int)m_Bounds.Top() + label.GetOffset().GetY());
			}
			
			// Draw all the images
			for (WindowImage image : m_Images)
			{
				graphics.drawImage(image.GetImage(), null,
						(int)m_Bounds.Left() + image.GetOffset().GetXInt(),
						(int)m_Bounds.Top() + image.GetOffset().GetYInt());
			}
		}*/
	}
	
	// Private class for text that appears in the window
	private class WindowLabel
	{
		private String label;
		private HWVec2 position;
		
		public WindowLabel(int x, int y, String message)
		{
			label = message;
			position = new HWVec2(x, y);
		}
		
		public String GetLabel()
		{
			return label;
		}
		
		public HWVec2 GetOffset()
		{
			return position;
		}
	}
	
	// Private class for images that appear in the window
	private class WindowImage
	{
		private BufferedImage image;
		private HWVec2 position;
		
		public WindowImage(int x, int y, BufferedImage pic)
		{
			image = pic;
			position = new HWVec2(x, y);
		}
		
		public BufferedImage GetImage()
		{
			return image;
		}
		
		public HWVec2 GetOffset()
		{
			return position;
		}
	}
}
