package com.howlingwolf.graphics.window;

// Java imports
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;

// Engine imports

// Utility imports
import com.howlingwolf.managers.HWResourceManager;
import com.howlingwolf.util.HWExtraFunctions;

// Physics imports
import com.howlingwolf.physics.modules.HWHitbox;

/**
 * Models a Window for menus, dialogues, etc. This contains the background for
 * a simple window with a border. It can be created using a single image or
 * multiple pieces of an image.
 * 
 * @author Dylan Sawyer
 */
public class HWBorderedWindow implements HWWindow
{
	/**
	 * The location and size of the window on screen.
	 */
	private final HWHitbox m_Bounds;
	/**
	 * The background image used when drawing the window, scaled to it's size.
	 */
	private BufferedImage m_Background;
	
	/**
	 * The state of the window.
	 */
	private int m_iOpenState;
	
	private int m_iOpenAmount = 0;
	private int m_iAmountPerFrame = 0;
	private long m_lAnimationTime = 0;
	
	private final long TIME_BETWEEN_FRAMES = 1000 / 30;
	
	private static final String FILEPATH = "images/system/";
	
	/**
	 * Creates a new window with a border.
	 * 
	 * @param x			The x coordinate of the window (upper left)
	 * @param y			The y coordinate of the window (upper left)
	 * @param p_iWidth	The width of the window
	 * @param p_iHeight	The height of the window
	 */
	public HWBorderedWindow(int x, int y, int p_iWidth, int p_iHeight)
	{
		m_Bounds = new HWHitbox(x, y, p_iWidth, p_iHeight);
		m_iOpenState = HWWindow.STATE_OPENING;
		
		m_iAmountPerFrame = p_iHeight / 5;
		m_iOpenAmount = m_iAmountPerFrame;
	}
	
	/**
	 * Scales the window with a uniformly sized border.
	 * 
	 * @param p_iBorderWidth	The thickness of the border on all sides
	 * @param p_sFilename		The filename of the image to be used
	 */
	public void ScaleWindow(int p_iBorderWidth, String p_sFilename)
	{
		ScaleWindow(p_iBorderWidth, p_iBorderWidth, p_iBorderWidth, p_iBorderWidth, p_sFilename);
	}
	
	/**
	 * Scales the window with the given thicknesses for the borders.
	 * 
	 * @param p_iLeftWidth		The thickness of the left border
	 * @param p_iRightWidth		The thickness of the right border
	 * @param p_iTopHeight		The thickness of the top border
	 * @param p_iBottomHeight	The thickness of the bottom border
	 * @param p_sFilename		The filename of the image to be used
	 */
	public void ScaleWindow(int p_iLeftWidth, int p_iRightWidth, int p_iTopHeight, int p_iBottomHeight, String p_sFilename)
	{
		BufferedImage baseImage = HWResourceManager.SharedManager().LoadBufferedImage(FILEPATH + p_sFilename);
		
		BufferedImage upperLeft, upperRight, bottomLeft, bottomRight, topBorder,
				bottomBorder, rightBorder, leftBorder, centre;
		
		int imageWidth, imageHeight, centreWidth, centreHeight, newCentreWidth, newCentreHeight;
		
		int paddedWidth = p_iLeftWidth + p_iRightWidth;
		int paddedHeight = p_iTopHeight + p_iBottomHeight;
		
		imageWidth = baseImage.getWidth();
		imageHeight = baseImage.getHeight();
		
		centreWidth = imageWidth - paddedWidth;
		centreHeight = imageHeight - paddedHeight;
		
		newCentreWidth = m_Bounds.GetWidth() - paddedWidth;
		newCentreHeight = m_Bounds.GetHeight() - paddedHeight;
		
		// Draw out the corners of the window...
		upperLeft = baseImage.getSubimage(0, 0, p_iLeftWidth, p_iTopHeight);
		upperRight = baseImage.getSubimage(imageWidth - p_iRightWidth, 0, p_iRightWidth, p_iTopHeight);
		bottomLeft = baseImage.getSubimage(0, imageHeight - p_iBottomHeight, p_iLeftWidth, p_iBottomHeight);
		bottomRight = baseImage.getSubimage(imageWidth - p_iRightWidth, imageHeight - p_iBottomHeight, p_iRightWidth, p_iBottomHeight);
		
		// Get the base border images
		topBorder = baseImage.getSubimage(p_iLeftWidth, 0, centreWidth, p_iTopHeight);
		bottomBorder = baseImage.getSubimage(p_iLeftWidth, imageHeight - p_iBottomHeight, centreWidth, p_iBottomHeight);
		leftBorder = baseImage.getSubimage(0, p_iTopHeight, p_iLeftWidth, centreHeight);
		rightBorder = baseImage.getSubimage(imageWidth - p_iRightWidth, p_iTopHeight, p_iRightWidth, centreHeight);
		
		// Get the base centre image
		centre = baseImage.getSubimage(p_iLeftWidth, p_iTopHeight, centreWidth, centreHeight);
		
		// Scale the borders and centre
		topBorder = HWExtraFunctions.CreateScaledImage(topBorder, newCentreWidth, topBorder.getHeight());
		bottomBorder = HWExtraFunctions.CreateScaledImage(bottomBorder, newCentreWidth, bottomBorder.getHeight());
		
		leftBorder = HWExtraFunctions.CreateScaledImage(leftBorder, leftBorder.getWidth(), newCentreHeight);
		rightBorder = HWExtraFunctions.CreateScaledImage(rightBorder, rightBorder.getWidth(), newCentreHeight);
		
		centre = HWExtraFunctions.CreateScaledImage(centre, newCentreWidth, newCentreHeight);
		
		m_Background = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(m_Bounds.GetWidth(), m_Bounds.GetHeight(), Transparency.TRANSLUCENT);
		
		Graphics2D graphics = (Graphics2D)m_Background.getGraphics();
		
		// Put the corners on
		graphics.drawImage(upperLeft, null, 0, 0);
		graphics.drawImage(upperRight, null, m_Bounds.GetWidth() - p_iRightWidth, 0);
		graphics.drawImage(bottomLeft, null, 0, m_Bounds.GetHeight() - p_iBottomHeight);
		graphics.drawImage(bottomRight, null, m_Bounds.GetWidth() - p_iRightWidth, m_Bounds.GetHeight() - p_iBottomHeight);
		
		// Put the borders on
		graphics.drawImage(topBorder, null, p_iLeftWidth, 0);
		graphics.drawImage(bottomBorder, null, p_iLeftWidth, m_Bounds.GetHeight() - p_iBottomHeight);
		graphics.drawImage(leftBorder, null, 0, p_iTopHeight);
		graphics.drawImage(rightBorder, null, m_Bounds.GetWidth() - p_iRightWidth, p_iTopHeight);
		
		// Put in the centre
		graphics.drawImage(centre, null, p_iLeftWidth, p_iTopHeight);
		
		// And then the m_Background image for the window is complete.
	}

	@Override
	public int GetState()
	{
		return m_iOpenState;
	}
	
	@Override
	public void SetState(int state)
	{
		if ((state > HWWindow.STATE_CLOSED) || (state < HWWindow.STATE_OPENING))
		{
			return;
		}
		
		m_iOpenState = state;
		
		if (m_iOpenState == HWWindow.STATE_OPEN)
		{
			m_iOpenAmount = m_Bounds.GetSize().GetHeight();
		}
	}
	
	/**
	 * Gets the size of the window.
	 * 
	 * @return	The size and position of the window
	 */
	public HWHitbox GetBounds()
	{
		return m_Bounds;
	}
	
	@Override
	public void Draw(Graphics2D graphics)
	{	
		if (m_iOpenAmount < m_Bounds.GetSize().GetHeight())
		{
			BufferedImage destImage = HWExtraFunctions.CreateScaledImage(m_Background, m_Background.getWidth(), m_iOpenAmount);
			int offsetY;
			offsetY = (m_Bounds.GetSize().GetHeight() - destImage.getHeight()) / 2;

			graphics.drawImage(destImage, null, (int)m_Bounds.Left(), (int)m_Bounds.Top() + offsetY);
		}
		else
		{
			graphics.drawImage(m_Background, null, (int)m_Bounds.Left(), (int)m_Bounds.Top());
		}
	}
	
	@Override
	public void Update(long elapsedTime)
	{
		if (m_iOpenState == HWWindow.STATE_OPENING)
		{
			m_lAnimationTime += elapsedTime;
			
			if (m_lAnimationTime >= TIME_BETWEEN_FRAMES)
			{
				m_iOpenAmount += m_iAmountPerFrame;
				m_lAnimationTime -= TIME_BETWEEN_FRAMES;
			}
			
			if (m_iOpenAmount >= m_Bounds.GetSize().GetHeight())
			{
				m_iOpenState = HWWindow.STATE_OPEN;
				m_iOpenAmount = m_Bounds.GetSize().GetHeight();
				m_lAnimationTime = 0;
			}
		}
		
		if (m_iOpenState == HWWindow.STATE_CLOSING)
		{
			m_lAnimationTime += elapsedTime;
			
			if (m_lAnimationTime >= TIME_BETWEEN_FRAMES)
			{
				m_iOpenAmount -= m_iAmountPerFrame;
				m_lAnimationTime -= TIME_BETWEEN_FRAMES;
			}
			
			if (m_iOpenAmount <= m_iAmountPerFrame)
			{
				m_iOpenState = HWWindow.STATE_CLOSED;
				m_iOpenAmount = m_iAmountPerFrame;
			}
		}
	}
}
