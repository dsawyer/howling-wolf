package com.howlingwolf.graphics.window;

// Java imports
import java.awt.Graphics2D;

/**
 *
 * @author Dylan Sawyer
 */
public interface HWWindow
{
	/**
	 * Opening state.
	 */
	public static final int STATE_OPENING = 0;
	/**
	 * Open state.
	 */
	public static final int STATE_OPEN = 1;
	/**
	 * Closing state.
	 */
	public static final int STATE_CLOSING = 2;
	/**
	 * Closed state.
	 */
	public static final int STATE_CLOSED = 3;
	
	/**
	 * Gets the state of the window.
	 * 
	 * @return HWWindow state
	 */
	public int GetState();
	
	/**
	 * Sets the state of the window.
	 * 
	 * @param p_iState	The new state of the window
	 */
	public void SetState(int p_iState);
	
	/**
	 * Tells the window to update itself based on the amount of time that has
	 * passed since the last update was called.
	 * 
	 * @param p_lElapsedTime	The amount of time since the last update
	 */
	public void Update(long p_lElapsedTime);
	
	/**
	 * Tells the window to draw itself on the provided graphics context.
	 * 
	 * @param graphics The graphics context
	 */
	public void Draw(Graphics2D graphics);
}
