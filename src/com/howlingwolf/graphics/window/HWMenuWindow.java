package com.howlingwolf.graphics.window;

// Java imports
import java.util.ArrayList;
import java.awt.Graphics2D;
import java.awt.Color;

// Core imports
import com.howlingwolf.managers.HWResourceManager;

// Graphics imports
import com.howlingwolf.graphics.HWAnimation;
import com.howlingwolf.graphics.HWSpritesheet;

/**
 * Models a small window that provides multiple options from which a player can
 * choose.
 * 
 * @author Dylan Sawyer
 */
public class HWMenuWindow extends HWBorderedWindow
{
	// The options the player has to choose from
	private ArrayList<MenuOption> m_Options;
	
	private int m_iCurrentOption;
	
	private HWAnimation m_Cursor;
	
	/**
	 * The cursor should move up.
	 */
	public static final int CURSOR_UP = 0;
	/**
	 * The cursor should move down.
	 */
	public static final int CURSOR_DOWN = 1;
	
	// For future use
	
	/**
	 * The cursor should move left.
	 */
	public static final int CURSOR_LEFT = 1;
	/**
	 * The cursor should move right.
	 */
	public static final int CURSOR_RIGHT = 1;
	
	private static final int ANIMATION_FRAME_TIME = 100;
	
	private static final int CURSOR_WIDTH = 16;
	private static final int CURSOR_HEIGHT = 16;
	
	private static final int BASE_X_OFFSET = 8;
	private static final int BASE_Y_OFFSET = 16;
	private static final int OPTION_SPACING = 16;
	
	private static final int CURSOR_X_OFFSET = -10;
	private static final int CURSOR_Y_OFFSET = -12;
	
	private static final int DEFAULT_WIDTH = 100;
	private static final int DEFAULT_HEIGHT = 32;
	
	/**
	 * Creates a new MenuHWBorderedWindow at the given location.
	 * 
	 * @param x GetX position of the window on the screen
	 * @param y GetY position of the window on the screen
	 * @param options The list of options in the menu
	 */
	public HWMenuWindow(int x, int y, ArrayList<String> options)
	{
		super(x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT + (OPTION_SPACING * options.size()));
		
		m_Options = new ArrayList<>();
		m_iCurrentOption = -1;
		
		for (String name : options)
		{
			AddOption(name);
		}
		
		m_Cursor = new HWAnimation("menuCursor", true);
		
		HWSpritesheet cursor = HWResourceManager.SharedManager().GetSpritesheet("SystemIcons");
		
		m_Cursor.AddFrame(cursor.FindFrame("cursorRight1").Clone());
		m_Cursor.AddFrame(cursor.FindFrame("cursorRight2").Clone());
		m_Cursor.AddFrame(cursor.FindFrame("cursorRight3").Clone());
		m_Cursor.AddFrame(cursor.FindFrame("cursorRight2").Clone());
	}
	
	private void AddOption(String name)
	{
		m_Options.add(new MenuOption(name));
		
		if (m_Options.size() == 1)
		{
			// Just added the first option, so set it to selected
			m_Options.get(0).SetSelected(true);
			m_iCurrentOption = 0;
		}
	}
	
	/**
	 * Changes which option is currently selected, thus moving the cursor.
	 * 
	 * @param direction The direction in which the cursor moved
	 */
	public void ChangeSelection(int direction)
	{
		if (direction == CURSOR_UP)
		{
			m_Options.get(m_iCurrentOption).SetSelected(false);
			if (m_iCurrentOption == 0)
			{
				m_iCurrentOption = m_Options.size() - 1;
			}
			else
			{
				m_iCurrentOption -= 1;
			}
			
			m_Options.get(m_iCurrentOption).SetSelected(true);
		}
		else if (direction == CURSOR_DOWN)
		{
			m_Options.get(m_iCurrentOption).SetSelected(false);
			
			m_iCurrentOption += 1;
			m_iCurrentOption %= m_Options.size();
			
			m_Options.get(m_iCurrentOption).SetSelected(true);
		}
	}
	
	/**
	 * Gets the identifier of the currently selected option.
	 * 
	 * @return A string representing the option currently selected
	 */
	public String FindCurrentSelectedOption()
	{
		return m_Options.get(m_iCurrentOption).GetIdentifier();
	}
	
	@Override
	public void Draw(Graphics2D graphics)
	{
		super.Draw(graphics);
		
		/*if (m_iOpenState == HWBorderedWindow.OPEN)
		{
			graphics.setColor(Color.white);

			for (int i = 0; i < m_Options.size(); i+=1)
			{
				graphics.drawString(m_Options.get(i).GetIdentifier(), m_Bounds.Left() + BASE_X_OFFSET, m_Bounds.Top() + BASE_Y_OFFSET + (i * OPTION_SPACING));
			}

			graphics.drawImage(m_Cursor.GetCurrentImage(), null, (int)m_Bounds.Left() + CURSOR_X_OFFSET, (int)m_Bounds.Top() + BASE_Y_OFFSET + CURSOR_Y_OFFSET + (m_iCurrentOption * OPTION_SPACING));
		}*/
	}
	
	@Override
	public void Update(long elapsedTime)
	{
		super.Update(elapsedTime);
		
		// If the window is open, update the cursor's animation
		/*if (m_iOpenState == HWBorderedWindow.OPEN)
		{
			m_Cursor.Update(elapsedTime);
		}*/
	}
	
	// Private container class for options, containing an identifier and a flag
	// to check if it is selected or not.
	private class MenuOption
	{
		private String m_sOptionName;
		private boolean m_bIsSelected;
		
		public MenuOption(String name)
		{
			m_sOptionName = name;
			m_bIsSelected = false;
		}
		
		public String GetIdentifier()
		{
			return m_sOptionName;
		}
		
		public boolean IsSelected()
		{
			return m_bIsSelected;
		}
		
		public void SetSelected(boolean select)
		{
			m_bIsSelected = select;
		}
	}
}