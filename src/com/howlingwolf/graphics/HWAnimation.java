package com.howlingwolf.graphics;

// Java imports
import java.awt.image.BufferedImage;
import java.util.ArrayList;

// Event imports
import com.howlingwolf.event.HWEventManager;
import com.howlingwolf.event.HWAnimationCompletedEvent;

/**
 * The HWAnimation class manages a series of frames and their display times.
 * 
 * @author Dylan Sawyer
 */
public final class HWAnimation
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private ArrayList<HWAnimationFrame> m_aFrames;
	private String m_sAnimName;
	private int m_iCurrentFrameIndex;
	private long m_lAnimTime;
	private long m_lTotalDuration;
	
	// Defines whether the animation loops or not
	private boolean m_bLoop;
	
	private boolean m_bDone;


    //**************************************************************************
	// Methods
	//**************************************************************************

	/**
	 * Creates a new, empty animation.
	 * 
	 * @param p_sName	The identifier for the animation
	 * @param p_bLoop	True if the animation should loop, false otherwise
	 */
	public HWAnimation(String p_sName, boolean p_bLoop)
	{
		this(null, p_sName, p_bLoop);
    }

    // Private Constructor for cloning
    private HWAnimation(ArrayList<HWAnimationFrame> p_frames, String p_sName, boolean p_bLoop)
    {
		m_lTotalDuration = 0;
    	m_aFrames = new ArrayList<>();
		
		if (p_frames != null)
		{
			for (HWAnimationFrame frame : p_frames)
			{
				m_aFrames.add(frame.Clone());
			}
		}
		
		m_sAnimName = p_sName;
		m_bLoop = p_bLoop;
		m_bDone = false;
    	Reset();
    }

    /**
	 * Creates a duplicate of this animation.
	 * 
	 * @return A copy of this animation
	 */
	public HWAnimation Clone()
    {
    	return new HWAnimation(m_aFrames, m_sAnimName, m_bLoop);
    }

    /**
	 * Adds an image to the animation with it's duration.
	 * 
	 * @param p_image		The image for the animation
	 * @param p_lDuration	The duration the image is shown
	 */
	public synchronized void AddFrame(BufferedImage p_image, long p_lDuration)
    {
    	m_lTotalDuration += p_lDuration;
		HWAnimationFrame anim = new HWAnimationFrame("", p_image, p_lDuration);
		anim.SetEndTime(m_lTotalDuration);
    	m_aFrames.add(anim);
    }
	
	/**
	 * Adds a frame to the list for the animation.
	 * 
	 * @param p_frame	The HWAnimationFrame to be added
	 */
	public synchronized void AddFrame(HWAnimationFrame p_frame)
	{
		if (p_frame != null)
		{
			m_lTotalDuration += p_frame.GetDuration();
			p_frame.SetEndTime(m_lTotalDuration);
			m_aFrames.add(p_frame);
		}
	}
	
	/**
	 * Gets the frame at the given index.
	 * 
	 * @param index	The index of the desired frame
	 * @return		The animation frame at the given index, null if the index
	 *				is invalid
	 */
	public synchronized HWAnimationFrame GetFrame(int index)
	{
		if ((index > -1) && (index < m_aFrames.size()))
		{
			return m_aFrames.get(index);
		}
		
		return null;
	}

    /**
	 * Starts this animation over from the beginning.
	 */
	public synchronized void Reset()
    {
    	m_lAnimTime = 0;
    	m_iCurrentFrameIndex = 0;
    }
	
	/**
	 * Starts the animation from a specific point.
	 * 
	 * @param startTime The time for the animation
	 * @param frameIndex The frame for the animation
	 */
	public synchronized void Start(long startTime, int frameIndex)
	{
		m_lAnimTime = startTime;
		m_iCurrentFrameIndex = frameIndex;
	}
	
	/**
	 * Gets the current point in time for the animation.
	 * 
	 * @return The time of the animation
	 */
	public synchronized long Time()
	{
		return m_lAnimTime;
	}
	
	/**
	 * Gets the current frame index for the animation.
	 * 
	 * @return The current frame index
	 */
	public synchronized int GetFrameIndex()
	{
		return m_iCurrentFrameIndex;
	}
	
	/**
	 * Sets the current frame of the animation to the last frame in the list.
	 */
	public synchronized void SetToLastFrame()
	{
		m_iCurrentFrameIndex = m_aFrames.size() - 1;
	}
	
	/**
	 * Gets the identifier for the animation.
	 * 
	 * @return The animation's identifier
	 */
	public synchronized String GetIdentifier()
	{
		return m_sAnimName;
	}
	
	/**
	 * Gets the images for the animation.
	 * 
	 * @return The animation's images
	 */
	public synchronized ArrayList<BufferedImage> GetFrames()
	{
		ArrayList<BufferedImage> list = new ArrayList<>();
		
		for (HWAnimationFrame frame : m_aFrames)
		{
			list.add(frame.GetFrame());
		}
		
		return list;
	}
	
	/**
	 * Gets the list of all the animation frames for the animation.
	 * 
	 * @return	All the frames for this animation
	 */
	public synchronized ArrayList<HWAnimationFrame> GetAnimationFrames()
	{
		return m_aFrames;
	}

    /**
	 * Updates the animation's current frame as long as the animation is more
	 * than one frame.
	 * 
	 * @param p_lElapsedTime Time since the last update
	 */
	public synchronized void Update(long p_lElapsedTime)
    {
    	if ((m_aFrames.size() > 1) && !m_bDone)
    	{
    		m_lAnimTime += p_lElapsedTime;

    		// Set the frames back to the beginning if necessary
    		if ((m_lAnimTime >= m_lTotalDuration) && m_bLoop)
    		{
    			m_lAnimTime %= m_lTotalDuration;
    			m_iCurrentFrameIndex = 0;
    		}
			// Stop the animation, hold the last frame
			else if (m_lAnimTime >= m_lTotalDuration)
			{
				// Do some kind of call back saying the animation is done
				HWEventManager.SharedManager().AddEvent(new HWAnimationCompletedEvent(m_sAnimName));
				
				m_bDone = true;
				return;
			}

    		while (m_lAnimTime > GetFrame(m_iCurrentFrameIndex).GetEndTime())
    		{
    			m_iCurrentFrameIndex += 1;
    		}
    	}
    }

    /**
	 * Gets the animation's current image.
	 * 
	 * @return The current image for the animation
	 */
	public synchronized BufferedImage GetCurrentImage()
    {
    	if (m_aFrames.isEmpty())
    	{
    		return null;
    	}
    	else
    	{
    		return GetFrame(m_iCurrentFrameIndex).GetFrame();
    	}
    }
}