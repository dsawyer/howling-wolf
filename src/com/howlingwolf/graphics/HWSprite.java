package com.howlingwolf.graphics;

// Java imports
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;

// Core imports
import com.howlingwolf.util.HWCamera;

// Math imports
import com.howlingwolf.util.math.HWVec2;
import com.howlingwolf.util.math.HWMathFunctions;

/**
 * Allows a visual representation of an object to appear on screen and be
 * animated.
 * 
 * @author Dylan Sawyer
 */
public class HWSprite
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private ArrayList<HWAnimation> m_Animations;
	private HWAnimation m_CurrentAnimation;
	private HWVec2 m_Position;


	//**************************************************************************
	// Methods
	//**************************************************************************
	
	/**
	 * Creates an empty sprite.
	 */
	public HWSprite()
	{
		this(new ArrayList<HWAnimation>(), null, new HWVec2());
	}
	
	/**
	 * Creates a sprite with the given animations and location.
	 * 
	 * @param animations The animations for the sprite
	 * @param current The current animation for the sprite
	 * @param pos The location of the sprite in the game world
	 */
	public HWSprite(ArrayList<HWAnimation> animations, HWAnimation current, HWVec2 pos)
	{
		m_Animations = new ArrayList<>(animations);
		m_Position = pos.Clone();
		
		if (current != null)
		{
			m_CurrentAnimation = current.Clone();
		}
		else
		{
			m_CurrentAnimation = null;
		}
	}
	
	/**
	 * Makes a copy of this sprite.
	 * 
	 * @return A copy of this sprite
	 */
	public HWSprite Clone()
	{
		return new HWSprite(m_Animations, m_CurrentAnimation, m_Position);
	}
	
	/**
	 * Adds an animation to the sprite's list of animations.
	 * 
	 * @param anim The new animation
	 */
	public void AddAnimation(HWAnimation anim)
	{
		m_Animations.add(anim);
		
		if (m_CurrentAnimation == null)
		{
			m_CurrentAnimation = anim;
		}
	}
	
	/**
	 * Gets the animation that the sprite is currently using.
	 * 
	 * @return	The current HWAnimation
	 */
	public HWAnimation GetCurrentAnimation()
	{
		return m_CurrentAnimation;
	}
	
	/**
	 * Places the sprite in a specific spot.
	 * 
	 * @param x The new x position of the sprite
	 * @param y The new y position of the sprite
	 */
	public void SetPosition(int x, int y)
	{
		m_Position.Set(x, y);
	}
	
	/**
	 * Places the sprite in a specific spot.
	 * 
	 * @param vec The new location of the sprite
	 */
	public void SetPosition(HWVec2 vec)
	{
		m_Position = vec.Clone();
	}
	
	/**
	 * Gets the location of the sprite.
	 * 
	 * @return The location of the sprite in the game world
	 */
	public HWVec2 GetPosition()
	{
		return m_Position;
	}
	
	/**
	 * Calculates the centre of the HWSprite object using it's default Position
	 * (which is in the upper left corner) and adding half the width and height.
	 * 
	 * @return	A vector containing the coordinates of the centre of the object
	 */
	public HWVec2 GetCentre()
	{
		return HWMathFunctions.AddVectors(m_Position, new HWVec2(GetWidth() / 2, GetHeight() / 2));
	}

    /**
	 * Updates the sprite's current animation.
	 * 
	 * @param p_lElapsedTime The amount of time since the last update
	 */
	public void Update(long p_lElapsedTime)
    {
    	m_CurrentAnimation.Update(p_lElapsedTime);
    }
	
	/**
	 * Draws the sprite to the given graphics context.
	 * 
	 * @param graphics The drawing board
	 */
	public void Draw(Graphics2D graphics)
	{
		HWVec2 drawPlace = HWCamera.Handle().WorldToScreen(m_Position);
		
		graphics.drawImage(GetCurrentImage(), null, drawPlace.GetXInt(), drawPlace.GetYInt());
	}
	
	/**
	 * Changes the current animation.
	 * <p>
	 * Retaining the animations information is for changing animations that are
	 * similar (movements, for example), but have a different direction/facing.
	 * 
	 * @param animID The identifier for the new animation to use
	 * @param retainAnimInfo Retains the previous animation's info if true
	 */
	public void ChangeAnimation(String animID, boolean retainAnimInfo)
	{
		// Trying to change to the same animation as the current one.
		// Do nothing.
		if (animID.equals(m_CurrentAnimation.GetIdentifier()))
		{
			return;
		}
		
		if (retainAnimInfo)
		{
			long time;
			int frame;
			time = m_CurrentAnimation.Time();
			frame = m_CurrentAnimation.GetFrameIndex();
			
			m_CurrentAnimation = FindAnimation(animID);
			m_CurrentAnimation.Start(time, frame);
		}
		else
		{
			m_CurrentAnimation = FindAnimation(animID);
		}
	}
	
	/**
	 * Gets a particular animation from the sprite's list of animations.
	 * 
	 * @param animID The identifier for the desired animation
	 * @return The desired animation, null if it doesn't exist
	 */
	public HWAnimation FindAnimation(String animID)
	{
		Iterator<HWAnimation> animations= m_Animations.iterator();
		while (animations.hasNext())
		{
			HWAnimation anim = animations.next();
			if (anim.GetIdentifier().equalsIgnoreCase(animID))
			{
				return anim;
			}
		}
		// There are no animations with the given identifier
		return null;
	}

    /**
	 * Gets the sprite's current width.
	 * 
	 * @return The width of the sprite's current aniamtion
	 */
	public int GetWidth()
    {
    	return m_CurrentAnimation.GetCurrentImage().getWidth(null);
    }

    /**
	 * Gets the sprite's current height.
	 * 
	 * @return The height of the sprite's current animation
	 */
	public int GetHeight()
    {
    	return m_CurrentAnimation.GetCurrentImage().getHeight(null);
    }

    /**
	 * Gets the sprite's current image.
	 * 
	 * @return The current image for the sprite's current animation
	 */
	public BufferedImage GetCurrentImage()
    {
    	return m_CurrentAnimation.GetCurrentImage();
    }
}