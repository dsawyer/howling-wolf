package com.howlingwolf.graphics;

/**
 * Models a vector with 4 elements to contain the information for a RGBA colour.
 * 
 * @author Dylan
 */
public class HWColour
{
	private byte[] m_Colours;
	
	/**
	 * The number of components in this colour object.
	 */
	public final static int NUM_COMPONENTS = 4;
	
	/**
	 * The maximum value a colour can have.
	 */
	public final static int MAX_VALUE = 255;

	/**
	 * The minimum value a colour can have.
	 */
	public final static int MIN_VALUE = 0;
	
	/**
	 * Index into the colour array for red.
	 */
	public final static int INDEX_RED = 0;

	/**
	 * Index into the colour array for green.
	 */
	public final static int INDEX_GREEN = 1;

	/**
	 * Index into the colour array for blue.
	 */
	public final static int INDEX_BLUE = 2;

	/**
	 * Index into the colour array for alpha.
	 */
	public final static int INDEX_ALPHA = 3;
	
	/**
	 * The value that defines whether a colour is set to Black or White when
	 * checking the contrast value.
	 */
	public final static int CONTRAST_VALUE = 125;
	
	/**
	 * Pre-made colour - Black.
	 */
	public final static HWColour BLACK = new HWColour(0, 0, 0, 255);

	/**
	 * Pre-made colour - Red.
	 */
	public final static HWColour RED = new HWColour(255, 0, 0, 255);

	/**
	 * Pre-made colour - Green.
	 */
	public final static HWColour GREEN = new HWColour(0, 255, 0, 255);

	/**
	 * Pre-made colour - Blue.
	 */
	public final static HWColour BLUE = new HWColour(0, 0, 255, 255);

	/**
	 * Pre-made colour - White.
	 */
	public final static HWColour WHITE = new HWColour(255, 255, 255, 255);
	
	/**
	 * The weight of the red value when converting to gray scale.
	 */
	public final static float WEIGHT_RED = 0.299f;

	/**
	 * The weight of the green value when converting to gray scale.
	 */
	public final static float WEIGHT_GREEN = 0.587f;

	/**
	 * The weight of the blue value when converting to gray scale.
	 */
	public final static float WEIGHT_BLUE = 0.114f;
	
	
	/**
	 * Creates a new empty colour.
	 */
	public HWColour()
	{
		m_Colours = new byte[NUM_COMPONENTS];
	}
	
	/**
	 * Creates a colour with the given RGBA components.
	 * 
	 * @param red	The red component of the colour
	 * @param green	The green component of the colour
	 * @param blue	The blue component of the colour
	 * @param alpha	The alpha component of the colour
	 */
	public HWColour(int red, int green, int blue, int alpha)
	{
		m_Colours = new byte[NUM_COMPONENTS];
		SetRed(red);
		SetGreen(green);
		SetBlue(blue);
		SetAlpha(alpha);
	}
	
	/**
	 * Creates a colour with the given RGBA components.
	 * 
	 * @param p_colours	The four components for the colour
	 */
	public HWColour(byte[] p_colours)
	{
		m_Colours = new byte[] { p_colours[INDEX_RED], p_colours[INDEX_GREEN], p_colours[INDEX_BLUE], p_colours[INDEX_ALPHA] };
	}
	
	/**
	 * Creates an exact copy of this colour.
	 * 
	 * @return	A duplicate of this colour
	 */
	public HWColour Clone()
	{
		return new HWColour(GetColours());
	}
	
	/**
	 * Creates a new colour object from a provided 32-bit int value that
	 * contains the four components in 8 bit chunks.
	 * 
	 * @param p_colour	The integer value containing the colour information
	 * @return			A new HWColour object with the given parameters
	 */
	public static HWColour ColourFromARGBInt(int p_colour)
	{
		HWColour colour = new HWColour();
		// Set all the values for each component
		colour.SetAlpha((p_colour >>> 24) & 0xFF);
		colour.SetRed((p_colour >>> 16) & 0xFF);
		colour.SetGreen((p_colour >>> 8) & 0xFF);
		colour.SetBlue((p_colour) & 0xFF);
		
		return colour;
	}
	
	/**
	 * Gets the array containing all the colour components.
	 * 
	 * @return	All the components of the RGBA colour
	 */
	public byte[] GetColours()
	{
		return m_Colours;
	}
	
	/**
	 * Gets the ARGB colour value in the form of an int.
	 * 
	 * @return	A 32-bit value containing each 8-bit component
	 */
	public int GetARGBInt()
	{
		return ((GetAlpha() << 24) | (GetRed() << 16) | (GetGreen() << 8) | (GetBlue()));
	}
	
	/**
	 * Gets the RGBA colour value in the form of an int.
	 * 
	 * @return	A 32-bit value containing each 8-bit component
	 */
	public int GetRGBAInt()
	{
		return ((GetRed() << 24) | (GetGreen() << 16) | (GetBlue() << 8) | (GetAlpha()));
	}
	
	/**
	 * Sets all the values for the colour.
	 * 
	 * @param p_colours All the values of the RGBA components
	 */
	public void SetColours(byte[] p_colours)
	{
		m_Colours = new byte[] { p_colours[INDEX_RED], p_colours[INDEX_GREEN], p_colours[INDEX_BLUE], p_colours[INDEX_ALPHA] };
	}
	
	/**
	 * Gets the red component of the colour.
	 * 
	 * @return	The red part of the RGBA colour
	 */
	public final int GetRed()
	{
		return ((int)m_Colours[INDEX_RED] & 0xFF);
	}
	
	/**
	 * Sets the red component of the colour
	 * 
	 * @param p_red	The new red component for the RGBA colour
	 */
	public final void SetRed(int p_red)
	{
		if (p_red > MAX_VALUE)
		{
			m_Colours[INDEX_RED] = (byte)MAX_VALUE;
		}
		else if (p_red < MIN_VALUE)
		{
			m_Colours[INDEX_RED] = (byte)MIN_VALUE;
		}
		else
		{
			m_Colours[INDEX_RED] = (byte)p_red;
		}
	}
	
	/**
	 * Used when a byte is passed instead of an int.
	 * 
	 * @param p_red	The red component
	 */
	public final void SetRed(byte p_red)
	{
		SetRed((int)p_red);
	}
	
	/**
	 * Gets the green component of the colour.
	 * 
	 * @return	The green component of the RGBA colour
	 */
	public int GetGreen()
	{
		return ((int)m_Colours[INDEX_GREEN] & 0xFF);
	}
	
	/**
	 * Sets the green component of the colour.
	 * 
	 * @param p_green	The new green component of the RGBA colour
	 */
	public final void SetGreen(int p_green)
	{
		if (p_green > MAX_VALUE)
		{
			m_Colours[INDEX_GREEN] = (byte)MAX_VALUE;
		}
		else if (p_green < MIN_VALUE)
		{
			m_Colours[INDEX_GREEN] = (byte)MIN_VALUE;
		}
		else
		{
			m_Colours[INDEX_GREEN] = (byte)p_green;
		}
	}
	
	/**
	 * Used when a byte is passed instead of an int.
	 * 
	 * @param p_green	The green component
	 */
	public final void SetGreen(byte p_green)
	{
		SetGreen((int)p_green);
	}
	
	/**
	 * Gets the blue component of the colour.
	 * 
	 * @return	The blue component of the RGBA colour
	 */
	public int GetBlue()
	{
		return ((int)m_Colours[INDEX_BLUE] & 0xFF);
	}
	
	/**
	 * Sets the blue component of the colour.
	 * 
	 * @param p_blue	The new blue component of the RGBA colour
	 */
	public final void SetBlue(int p_blue)
	{
		if (p_blue > MAX_VALUE)
		{
			m_Colours[INDEX_BLUE] = (byte)MAX_VALUE;
		}
		else if (p_blue < MIN_VALUE)
		{
			m_Colours[INDEX_BLUE] = (byte)MIN_VALUE;
		}
		else
		{
			m_Colours[INDEX_BLUE] = (byte)p_blue;
		}
	}
	
	/**
	 * Used when a byte is passed instead of an int.
	 * 
	 * @param p_blue	The blue component
	 */
	public final void SetBlue(byte p_blue)
	{
		SetBlue((int)p_blue);
	}
	
	/**
	 * Gets the alpha component of the colour.
	 * 
	 * @return	The alpha component of the RGBA colour
	 */
	public int GetAlpha()
	{
		return ((int)m_Colours[INDEX_ALPHA] & 0xFF);
	}
	
	/**
	 * Sets the alpha component of the colour.
	 * 
	 * @param p_alpha	The new alpha component of the RGBA colour
	 */
	public final void SetAlpha(int p_alpha)
	{
		if (p_alpha > MAX_VALUE)
		{
			m_Colours[INDEX_ALPHA] = (byte)MAX_VALUE;
		}
		else if (p_alpha < MIN_VALUE)
		{
			m_Colours[INDEX_ALPHA] = (byte)MIN_VALUE;
		}
		else
		{
			m_Colours[INDEX_ALPHA] = (byte)p_alpha;
		}
	}
	
	/**
	 * Used when a byte is passed instead of an int.
	 * 
	 * @param p_alpha	The alpha component
	 */
	public final void SetAlpha(byte p_alpha)
	{
		SetAlpha((int)p_alpha);
	}
	
	/**
	 * Sets the colour to White.
	 */
	public final void SetWhite()
	{
		SetRed(MAX_VALUE);
		SetGreen(MAX_VALUE);
		SetBlue(MAX_VALUE);
	}
	
	/**
	 * Sets the colour to Black.
	 */
	public final void SetBlack()
	{
		SetRed(MIN_VALUE);
		SetGreen(MIN_VALUE);
		SetBlue(MIN_VALUE);
	}
	
	/**
	 * Sets the colour's components based on an integer value containing all
	 * four of the values in 8-bit sections.
	 * 
	 * @param argb	The 32-bit representation of the four 8-bit values
	 */
	public final void SetFromARGB(int argb)
	{
		SetAlpha((argb >>> 24) & 0xFF);
		SetRed((argb >>> 16) & 0xFF);
		SetGreen((argb >>> 8) & 0xFF);
		SetBlue((argb) & 0xFF);
	}
	
	/**
	 * Sets the colour to a gray scale based off the red value.
	 */
	public final void GrayscaleRed()
	{
		SetGreen(GetRed());
		SetBlue(GetRed());
	}
	
	/**
	 * Sets the colour to a gray scale based off the green value.
	 */
	public final void GrayscaleGreen()
	{
		SetRed(GetGreen());
		SetBlue(GetGreen());
	}
	
	/**
	 * Sets the colour to a gray scale based off the blue value.
	 */
	public final void GrayscaleBlue()
	{
		SetRed(GetBlue());
		SetGreen(GetBlue());
	}
	
	/**
	 * Sets the colour to a gray scale based off the average value of the three
	 * colour components.
	 */
	public final void GrayscaleAverage()
	{
		int average = (GetRed() + GetGreen() + GetBlue()) / 3;
		
		SetRed(average);
		SetGreen(average);
		SetBlue(average);
	}
	
	/**
	 * Sets the colour to a gray scale based off a weighted average value of the
	 * three colour components.
	 */
	public final void GrayscaleWeighted()
	{
		// Do some kind of weighted average to calculate the value
		int weightedAverage = (int)((GetRed() * WEIGHT_RED) + (GetGreen() * WEIGHT_GREEN) + (GetBlue() * WEIGHT_BLUE));
		
		SetRed(weightedAverage);
		SetGreen(weightedAverage);
		SetBlue(weightedAverage);
	}
	
	/**
	 * Sets the colour to white or black based on if the red colour component is
	 * greater or less than the contrast value.
	 */
	public final void ContrastRed()
	{
		if (GetRed() < CONTRAST_VALUE)
		{
			SetColours(BLACK.GetColours());
		}
		else
		{
			SetColours(WHITE.GetColours());
		}
	}
	
	/**
	 * Sets the colour to white or black based on if the green colour component
	 * is greater or less than the contrast value.
	 */
	public final void ContrastGreen()
	{
		if (GetGreen() < CONTRAST_VALUE)
		{
			SetColours(BLACK.GetColours());
		}
		else
		{
			SetColours(WHITE.GetColours());
		}
	}
	
	/**
	 * Sets the colour to white or black based on if the blue colour component
	 * is greater or less than the contrast value.
	 */
	public final void ContrastBlue()
	{
		if (GetBlue() < CONTRAST_VALUE)
		{
			SetColours(BLACK.GetColours());
		}
		else
		{
			SetColours(WHITE.GetColours());
		}
	}
	
	/**
	 * Sets the colour to white or black based on if the average of the three
	 * colour components is greater or less than the contrast value.
	 */
	public final void ContrastAverage()
	{
		int average = (GetRed() + GetGreen() + GetBlue()) / 3;
		
		if (average < CONTRAST_VALUE)
		{
			SetColours(BLACK.GetColours());
		}
		else
		{
			SetColours(WHITE.GetColours());
		}
	}
	
	/**
	 * Sets the colour to white or black based on if the weighted average of the
	 * three colour components is greater or less than the contrast value.
	 */
	public final void ContrastWeighted()
	{
		// Do some kind of weighted average to calculate the value
		int weightedAverage = (int)((GetRed() * WEIGHT_RED) + (GetGreen() * WEIGHT_GREEN) + (GetBlue() * WEIGHT_BLUE));
		
		if (weightedAverage < CONTRAST_VALUE)
		{
			SetColours(BLACK.GetColours());
		}
		else
		{
			SetColours(WHITE.GetColours());
		}
	}
	
	@Override
	public String toString()
	{
		String result = "";
		result += "Red:   " + GetRed() + "\n";
		result += "Green: " + GetGreen() + "\n";
		result += "Blue:  " + GetBlue() + "\n";
		result += "Alpha: " + GetAlpha() + "\n";
		
		return result;
	}
}