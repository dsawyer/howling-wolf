package com.howlingwolf.graphics;

// Java imports
import java.util.ArrayList;

/**
 * Container class for sprite sheets. Has an identifier, as well as methods
 * to draw out images for animations.
 * 
 * @author Dylan Sawyer
 */
public class HWSpritesheet
{
	// Container for all the frames in the sprite sheet
	private final ArrayList<HWAnimationFrame> m_frames;
	// Identifier for the sprite sheet
	private final String m_sIdentifier;
	
	/**
	 * Creates a  new Spritesheet using the given filename.
	 * 
	 * @param filename The name of the file to be used as a sprite sheet
	 */
	public HWSpritesheet(String filename)
	{
		m_sIdentifier = filename;
		m_frames = new ArrayList<>();
	}
	
	/**
	 * Adds an HWAnimationFrame to the list of frames that are contained in the
	 * sprite sheet.
	 * 
	 * @param p_frame	The animation frame in the sprite sheet
	 */
	public void AddFrame(HWAnimationFrame p_frame)
	{
		m_frames.add(p_frame);
	}
	
	/**
	 * Locates the HWAnimateFrame with the given identifier within the sprite
	 * sheet.
	 * 
	 * @param p_sID	The ID of the desired animation frame
	 * @return		The HWAnimationFrame requested, null if none match the ID
	 */
	public HWAnimationFrame FindFrame(String p_sID)
	{
		for (HWAnimationFrame frame : m_frames)
		{
			if (frame.GetIdentifier().equalsIgnoreCase(p_sID))
			{
				return frame;
			}
		}
		
		return null;
	}
	
	/**
	 * Gets the identifier for this HWSpritesheet.
	 * 
	 * @return The sprite sheet's identifier
	 */
	public String GetIdentifier()
	{
		return m_sIdentifier;
	}
}