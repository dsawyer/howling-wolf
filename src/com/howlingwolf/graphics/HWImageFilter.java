package com.howlingwolf.graphics;

// Java imports
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;

// World imports
import com.howlingwolf.graphics.HWLightSource;
import com.howlingwolf.world.HWAreaObject;

// Utility imports
import com.howlingwolf.util.HWCamera;
import com.howlingwolf.util.HWExtraFunctions;

// Math imports
import com.howlingwolf.util.math.HWVec2;
import com.howlingwolf.util.math.HWRectangle;
import com.howlingwolf.util.math.HWCircle;
import com.howlingwolf.util.math.HWMathFunctions;

/**
 * Main class that defines a way to modify an image in some way. Has various
 * methods for filtering an image in different ways.
 * 
 * @author Dylan
 */
public class HWImageFilter
{
	private static final int CONTRAST_VALUE = 140;
	
	public static void FilterContrast(BufferedImage p_image)
	{
		HWColour pixel = new HWColour();
		
		for (int y = 0; y < p_image.getHeight(); y+=1)
		{
			for (int x = 0; x < p_image.getWidth(); x+=1)
			{
				pixel.SetFromARGB(p_image.getRGB(x, y));
				pixel.ContrastWeighted();
				p_image.setRGB(x, y, pixel.GetARGBInt());
			}
		}
	}
	
	public static void FilterGrayscale(BufferedImage p_image)
	{
		HWColour pixel = new HWColour();
		
		for (int y = 0; y < p_image.getHeight(); y+=1)
		{
			for (int x = 0; x < p_image.getWidth(); x+=1)
			{
				pixel.SetFromARGB(p_image.getRGB(x, y));
				pixel.GrayscaleWeighted();
				p_image.setRGB(x, y, pixel.GetARGBInt());
			}
		}
	}
	
	public static BufferedImage CreateLightmap(ArrayList<HWLightSource> lights)
	{
		// Assume that all objects passed to this method are both light sources
		// and are affecting the current visuals.
		
		HWVec2 size = HWCamera.Handle().GetCameraSize();
		HWVec2 offset = HWCamera.Handle().GetPosition();
		HWCircle light;
		HWVec2 centre;
		
		HWColour lightPixel = HWColour.BLACK;
		
		BufferedImage lightmap = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(size.GetXInt(), size.GetYInt(), Transparency.TRANSLUCENT);
		
		for (int y = 0; y < size.GetYInt(); y+=1)
		{
			for (int x = 0; x < size.GetXInt(); x+=1)
			{
				
			}
		}
		
		return lightmap;
	}
}