package com.howlingwolf.graphics.tileEngine;

// Java imports
import java.util.ArrayList;
import java.awt.image.BufferedImage;

// Graphics imports
import com.howlingwolf.graphics.HWAnimation;
import com.howlingwolf.graphics.HWAnimationFrame;

// Math imports
import com.howlingwolf.util.math.HWPolygon;
import com.howlingwolf.util.math.HWVec2;

// Physics imports
import com.howlingwolf.physics.modules.HWPhysicsShape;
import com.howlingwolf.physics.modules.HWPhysicsBody;

/**
 * Models a HWTile.
 * 
 * @author Dylan Sawyer
 */
public class HWTile implements HWPhysicsBody
{
	/**
	 * The default amount of time used between frames for animated tiles.
	 */
	public static final long FRAME_DURATION = 300;
	/**
	 * Sentinel value that denotes a tile with nothing in it. Used when taking
	 * in purely numerical data for building layers.
	 */
	public static final int EMPTY_TILE = -1;
	
	// The animation for the tile, be it only one image or multiple
	private HWAnimation m_Animation;
	// The shape used for collision purposes on this tile
	private HWPhysicsShape m_PhysicsShape;
	
	/**
	 * Default constructor.
	 */
	public HWTile()
	{
		this(null, new ArrayList<BufferedImage>());
	}
	
	/**
	 * Creates a new Tile object with the given type and image/animation frames.
	 * 
	 * @param p_collider	The type of collision this tile uses
	 * @param images		The image(s) of the tile
	 */
	public HWTile(HWPolygon p_collider, ArrayList<BufferedImage> images)
	{
		if (p_collider == null)
		{
			m_PhysicsShape = new HWPhysicsShape(this, false);
		}
		else
		{
			m_PhysicsShape = new HWPhysicsShape(p_collider.Clone(), this, false);
		}
		
		m_Animation = new HWAnimation("", true);
		for (BufferedImage frame : images)
		{
			m_Animation.AddFrame(frame, FRAME_DURATION);
		}
	}
	
	/**
	 * Makes a copy of this tile.
	 * 
	 * @return A completely separate copy that is identical to the source HWTile
	 */
	public HWTile Clone()
	{
		HWTile tile = new HWTile();
		if (m_PhysicsShape.GetShape() != null)
		{
			tile.SetCollisionShape(m_PhysicsShape.GetShape().Clone());
		}
		
		for (HWAnimationFrame frame : m_Animation.GetAnimationFrames())
		{
			tile.AddFrame(frame.Clone());
		}
		
		return tile;
	}
	
	/**
	 * Adds an image to the tile's animation.
	 * 
	 * @param p_image The image to be added as an HWAnimationFrame
	 */
	public void AddFrame(BufferedImage p_image)
	{
		m_Animation.AddFrame(p_image, FRAME_DURATION);
	}
	
	/**
	 * Adds the given animation frame to the list for the tile's animation.
	 * 
	 * @param p_frame	The HWAnimationFrame that needs to be added
	 */
	public void AddFrame(HWAnimationFrame p_frame)
	{
		m_Animation.AddFrame(p_frame);
	}
	
	/**
	 * Adds an image to the tile's animation.
	 * 
	 * @param p_image		The image to be added as an HWAnimationFrame
	 * @param p_lDuration	The duration the image should be displayed during
	 *						the animation
	 */
	public void AddFrame(BufferedImage p_image, long p_lDuration)
	{
		m_Animation.AddFrame(p_image, p_lDuration);
	}
	
	/**
	 * Sets the CollisionShape for this HWTile.
	 * 
	 * @param p_shape	The new collision shape to be used
	 */
	public void SetCollisionShape(HWPolygon p_shape)
	{
		m_PhysicsShape.SetShape(p_shape);
	}
	
	/**
	 * Gets the current image for the tile, depending on its animation.
	 * 
	 * @return The current image of the tile's animation
	 */
	public BufferedImage GetCurrentImage()
	{
		return m_Animation.GetCurrentImage();
	}
	
	/**
	 * Gets the animation used by the tile.
	 * 
	 * @return	The tile's animation
	 */
	public HWAnimation GetAnimation()
	{
		return m_Animation;
	}
	
	/**
	 * Gets the collision type for this tile. Keep in mind that the position
	 * of the polygon defining the collision shape for the tile is given in
	 * its relative coordinates (it's origin (0,0) is the upper left corner
	 * of the tile).
	 * 
	 * @return The SetCollisionShape used by this object
	 */
	@Override
	public HWPhysicsShape GetPhysicsShape()
	{
		return m_PhysicsShape;
	}
	
	@Override
	public void CollidedWith(HWPhysicsBody body, HWVec2 depth)
	{
		// Do nothing
	}
	
	@Override
	public void StaticCollisionWith(HWPhysicsShape shape, HWVec2 depth)
	{
		// Do nothing
	}
	
	/**
	 * Updates the animation for this tile. The animation update returns
	 * immediately if there is only one frame in the animation.
	 * 
	 * @param p_lElapsedTime The amount of time that has passed since the last
	 *						 update
	 */
	public void Update(long p_lElapsedTime)
	{
		m_Animation.Update(p_lElapsedTime);
	}
}