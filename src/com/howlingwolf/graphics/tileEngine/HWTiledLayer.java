package com.howlingwolf.graphics.tileEngine;

// Java imports
import java.awt.*;
import java.awt.image.BufferedImage;

// Scene imports
import com.howlingwolf.scene.HWScene;

// Manager imports
import com.howlingwolf.managers.HWResourceManager;

// Utility imports
import com.howlingwolf.util.HWCamera;
import com.howlingwolf.util.HWExtraFunctions;

// Math imports
import com.howlingwolf.util.math.HWVec2;

/**
 * Represents a layer that is composed of tiles.
 * 
 * @author Dylan Sawyer
 */
public class HWTiledLayer extends HWScene
{
	//*************************************************************************
	// Member Variables
	//*************************************************************************

	// All the values that represent a tile in the layer
	private final String m_sTilesetID;
	private final HWTile[] m_Tiles;
	private final int[] m_iTileData;

	// The size of the layer (in tiles)
	private final int m_iWidth;
	private final int m_iHeight;
	
	// The size of the tiles in the layer
	private final int m_iTileWidth;
	private final int m_iTileHeight;

	//*************************************************************************
	// Methods
	//*************************************************************************

    /**
	 * Creates a layer and initialises it with the given data.
	 * 
	 * @param p_data Values for each individual tile in the layer
	 * @param p_width The width of the layer in tiles
	 * @param p_height The height of the layer in tiles
	 * @param p_iX The x offset of the layer in the world
	 * @param p_iY The y offset of the layer in the world
	 * @param p_tilesetID The identifier for which WEHWTileset object is used by
					  this layer
	 * @param p_id The identifier for this layer
	 */
	public HWTiledLayer(int[] p_data, int p_width, int p_height, int p_iX, int p_iY, String p_tilesetID, String p_id)
    {
		super(p_id);
		
		// Layer size in tiles
    	m_iWidth = p_width;
    	m_iHeight = p_height;
		
		m_sTilesetID = p_tilesetID;
		
		HWTileset tileset = HWResourceManager.SharedManager().GetTileset(p_tilesetID).Clone();
		
		// HWTile size
		m_iTileWidth = tileset.TileWidth();
		m_iTileHeight = tileset.TileHeight();

		// The size of the layer (in pixels)
		SetWidth(m_iWidth * m_iTileWidth);
		SetHeight(m_iHeight * m_iTileHeight);
		
		// Layer offset
		SetPosition(p_iX, p_iY);
		
		m_Tiles = new HWTile[m_iWidth * m_iHeight];
		
		// Get a copy of the data (will be useful when creating an Editor)
		m_iTileData = p_data;
		
		HWTile temp;
		
		for (int y = 0; y < m_iHeight; y+=1)
		{
			for (int x = 0; x < m_iWidth; x+=1)
			{
				int tileID = p_data[(m_iWidth * y) + x];
				
				// If the tile isn't empty, add a new HWTile object
				if (tileID != HWTile.EMPTY_TILE)
				{
					temp = tileset.GetTileAt(tileID).Clone();
					if (temp.GetPhysicsShape() != null)
					{
						temp.GetPhysicsShape().Move(GetPosition());
						temp.GetPhysicsShape().Move(new HWVec2(x * m_iTileWidth, y * m_iTileHeight));
					}
					m_Tiles[(m_iWidth * y) + x] = temp;
				}
				else // Make certain it is a null reference
				{
					m_Tiles[(m_iWidth * y) + x] = null;
				}
				
				temp = null;
			}
		}
    }
	
	@Override
	public HWTiledLayer Clone()
	{
		return new HWTiledLayer(m_iTileData, m_iWidth, m_iHeight, GetPosition().GetXInt(),
								GetPosition().GetYInt(), m_sTilesetID, GetIdentifier());
	}
	
	/**
	 * Gets the width of the tiles in the layer (in pixels).
	 * 
	 * @return	The width of the tiles used in this layer
	 */
	public int GetTileWidth()
	{
		return m_iTileWidth;
	}
	
	/**
	 * Gets the height of the tiles in the layer (in pixels).
	 * 
	 * @return	The height of the tiles used in this layer
	 */
	public int GetTileHeight()
	{
		return m_iTileHeight;
	}
	
	/**
	 * Updates all the tiles in the layer.
	 * 
	 * @param p_lElapsedTime The amount of time since the last update
	 */
	@Override
	public void Update(long p_lElapsedTime)
	{
		for (HWTile tile : m_Tiles)
		{
			if (tile != null)
			{
				tile.Update(p_lElapsedTime);
			}
		}
	}

    /**
	 * Draws the layer onto the given {@link Graphics2D} object.
	 * 
	 * @param graphics The canvas upon which the layer is drawn
	 */
	@Override
	public void Draw(Graphics2D graphics)
    {
		HWVec2 first, last;
		HWCamera camera = HWCamera.Handle();
		
		int offsetX, offsetY;
		
		offsetX = (int)camera.GetPosition().GetX();
		offsetY = (int)camera.GetPosition().GetY();
		
		// Get upper left and lower right tile coordinates for
		// all tiles that are visible on screen
		first = (HWExtraFunctions.PixelsToTiles(camera.GetPosition())).Clone();
		last = new HWVec2(camera.GetPosition().GetX(), camera.GetPosition().GetY());
		last.SetX(last.GetX() + camera.GetCameraView().GetWidth());
		last.SetY(last.GetY() + camera.GetCameraView().GetHeight());
		last = HWExtraFunctions.PixelsToTiles(last);
		
		for (int y = first.GetYInt(); y <= last.GetYInt(); y+=1)
		{
			for (int x = first.GetXInt(); x <= last.GetXInt(); x+=1)
			{
				HWTile tile = GetTileAtOffset(x, y);
				
				// Make sure there is a tile to be drawn, and it isn't an empty space
				if (tile != null)
				{
					// Get the tile's current image
					BufferedImage image = tile.GetCurrentImage();
					// Draw the image at the given location on screen
					graphics.drawImage(image, (x * m_iTileWidth) - offsetX, (y * m_iTileHeight) - offsetY, null);
				}
			}
		}
    }
	
	/**
	 * Gets all the tiles in the layer.
	 * 
	 * @return	The tiles that are in this layer
	 */
	public HWTile[] GetTiles()
	{
		return m_Tiles;
	}

    // Gets the tile at the given position in this layer.
	private HWTile GetTileAt(int num)
    {
    	if ((num >= 0) && (num < m_Tiles.length))
    	{
    		return m_Tiles[num];
    	}

    	return null;
    }

    /**
	 * Gets the tile at the given position in this layer.
	 * 
	 * @param x GetX position of the tile in this layer
	 * @param y GetY position of the tile in this layer
	 * @return  The value of the tile at the given position
	 */
	public HWTile GetTileAt(int x, int y)
    {
		if ((x >= m_iWidth) || (x < 0) ||
			(y >= m_iHeight) || (y < 0))
		{
			return null;
		}
		
		return GetTileAt(x + (y * m_iWidth));
    }
	
	/**
	 * Gets the HWTile at the given position in this layer. Calculates the layer's
	 * internal offset on it's own.
	 * 
	 * @param x The x coordinate in the area to look for the HWTile
	 * @param y The y coordinate in the area to look for the HWTile
	 * @return  The HWTile that is at the given world location
	 */
	public HWTile GetTileAtOffset(int x, int y)
	{
		return GetTileAt(x - (GetX() / m_iTileWidth), y - (GetY() / m_iTileHeight));
	}
}