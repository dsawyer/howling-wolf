package com.howlingwolf.graphics.tileEngine;

/**
 * This classes models a tile set. It takes in an image and divides it up into
 * tiles, making these tiles usable individually.
 * 
 * @author Dylan Sawyer
 */
public class HWTileset
{
	//*************************************************************************
	// Member Variables
	//*************************************************************************

	// The list of tiles in the tileset
	//private BufferedImage[] m_Tiles;
	private final HWTile[] m_Tiles;

	// The dimensions of the tileset, in number of tiles
	private final int m_iTilesWide;
	private final int m_iTilesHigh;
	
	// The dimensions of the tiles in the tile set
	private final int m_iTileWidth;
	private final int m_iTileHeight;

	// The filename used for getting the tileset, also used for identification
	private final String m_sFilename;

	//*************************************************************************
	// Constants
	//*************************************************************************

	// HWTile dimensions
	/**
	 * The default width of each individual tile.
	 */
	public static final int TILE_WIDTH = 32;
	/**
	 * The default height of each individual tile.
	 */
	public static final int TILE_HEIGHT = 32;
	
	//*************************************************************************
	// Methods
	//*************************************************************************
	
	/**
	 * Creates a HWTileset object with the given information.
	 * 
	 * @param p_filename The name of the image file that contains the tile set's
					 graphical data, also the HWTileset object's identifier
	 * @param width		 The width of the tile set, in number of HWTiles
	 * @param height	 The height of the tile set, in number of HWTiles
	 * @param tileWidth	 The width of the HWTiles in the tile set
	 * @param tileHeight The height of the HWTiles in the tile set
	 * @param tiles		 The array of HWTile objects that form a tile set
	 */
	public HWTileset(String p_filename, int width, int height, int tileWidth, int tileHeight, HWTile[] tiles)
	{
		m_sFilename = p_filename;
		
		m_iTileWidth = tileWidth;
		m_iTileHeight = tileHeight;
		m_iTilesWide = width;
		m_iTilesHigh = height;
		
		m_Tiles = tiles;
	}
	
	/**
	 * Creates a copy of this tile set.
	 * 
	 * @return	An exact duplicate of this tile set
	 */
	public HWTileset Clone()
	{
		return new HWTileset(m_sFilename, m_iTilesWide, m_iTilesHigh, m_iTileWidth, m_iTileHeight, m_Tiles);
	}

	/**
	 * Gets the unique identifier for a tile set.
	 * 
	 * @return The identifier for this tile set
	 */
	public String GetIdentifier()
	{
		return m_sFilename;
	}
	
	/**
	 * The width of each individual tile in the tile set.
	 * 
	 * @return The tile width
	 */
	public int TileWidth()
	{
		return m_iTileWidth;
	}
	
	/**
	 * The height of each individual tile in the tile set.
	 * 
	 * @return The tile height
	 */
	public int TileHeight()
	{
		return m_iTileHeight;
	}

	/**
	 * Gets the image (tile) from the tile set.
	 * 
	 * @param tilenum The tile's position in the set
	 * @return The HWTile
	 */
	public HWTile GetTileAt(int tilenum)
	{
		if ((tilenum >= 0) || (tilenum < m_Tiles.length))
		{
			return m_Tiles[tilenum];
		}

		return null;
	}

	/**
	 * Gets the image (tile) from the tile set.
	 * 
	 * @param x The x position of the tile in the set
	 * @param y The y position of the tile in the set
	 * @return  The HWTile
	 */
	public HWTile GetTileAt(int x, int y)
	{
		if ((x > 0) && (x < m_iTilesWide) &&
			(y > 0) && (y < m_iTilesHigh))
		{
			return GetTileAt(x + (y * m_iTilesWide));
		}
		
		return null;
	}
}