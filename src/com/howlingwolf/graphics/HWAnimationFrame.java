package com.howlingwolf.graphics;

// Java imports
import java.awt.image.BufferedImage;

/**
 * This class provides a data structure for each frame that will appear in an
 * HWAnimation.
 * 
 * @author Dylan
 */
public class HWAnimationFrame
{
	// The image for the animation
	private final BufferedImage m_Frame;
	// The identifier for the frame
	private final String m_sIdentifier;
	// The duration for which the image is shown
	private final long m_lDuration;
	// The time at which the frame should change
	private long m_lEndTime;

	/**
	 * Creates a new HWAnimationFrame.
	 * 
	 * @param p_sID			The identifier for this frame
	 * @param p_frame		The image for this frame
	 * @param p_lDuration	The amount of time this frame is shown in an
	 *						animation
	 */
	public HWAnimationFrame(String p_sID, BufferedImage p_frame, long p_lDuration)
	{
		m_sIdentifier = p_sID;
		m_Frame = p_frame;
		m_lDuration = p_lDuration;
		m_lEndTime = m_lDuration;
	}
	
	/**
	 * Creates an exact duplicate of the animation frame.
	 * 
	 * @return	A copy of this animation frame
	 */
	public HWAnimationFrame Clone()
	{
		return new HWAnimationFrame(m_sIdentifier, m_Frame, m_lDuration);
	}
	
	/**
	 * Gets the identifier for the animation frame.
	 * 
	 * @return	The ID of this animation frame
	 */
	public String GetIdentifier()
	{
		return m_sIdentifier;
	}
	
	/**
	 * Gets the image for the animation frame.
	 * 
	 * @return	The BufferedImage of this animation frame
	 */
	public BufferedImage GetFrame()
	{
		return m_Frame;
	}
	
	/**
	 * Gets the amount of time the animation frame is shown.
	 * 
	 * @return	The duration of this animation frame
	 */
	public long GetDuration()
	{
		return m_lDuration;
	}
	
	/**
	 * Gets the time at which the frame should be swapped out for the next one
	 * in the animation.
	 * 
	 * @return	The swap time in the animation when this frame should be changed
	 */
	public long GetEndTime()
	{
		return m_lEndTime;
	}
	
	/**
	 * Sets the time at which the frame should be swapped out for the next one
	 * in the animation.
	 * 
	 * @param p_lTime	The swap time in the animation when this frame should be
	 *					changed
	 */
	public void SetEndTime(long p_lTime)
	{
		m_lEndTime = p_lTime;
	}
}