package com.howlingwolf.managers;

// Java imports
import java.util.ArrayList;
import javax.sound.sampled.AudioFormat;

// Audio imports
import com.howlingwolf.audio.music.HWMidiFile;
import com.howlingwolf.audio.music.HWMidiPlayer;
import com.howlingwolf.audio.sound.HWSound;

// Core imports
import com.howlingwolf.util.xml.HWXMLReaderSAX;

/**
 * Manages all the sounds within the game.
 * 
 * @author Dylan Sawyer
 */
public class HWAudioManager
{
	private final HWSoundManager m_SoundManager;
	private final HWMidiPlayer m_MidiPlayer;
	
	private final ArrayList<HWSound> m_Sounds;
	private final ArrayList<HWMidiFile> m_Midis;
	
	private static HWAudioManager instance;
	
	// Always... ALWAYS make sure you have the right and consistent audio format
	// Otherwise Java will complain... Loudly
	private static final AudioFormat PLAYBACK_FORMAT = new AudioFormat(22050, 16, 1, true, false);
	
	// Private constructor. Sets up the lists and players.
	private HWAudioManager()
	{
		m_SoundManager = new HWSoundManager();
		m_MidiPlayer = new HWMidiPlayer();
		m_MidiPlayer.AddMetaEventListener();
		m_Sounds = new ArrayList<>();
		m_Midis = new ArrayList<>();
	}
	
	/**
	 * Gets a handle to the HWAudioManager singleton.
	 * 
	 * @return A reference to the HWAudioManager
	 */
	public synchronized static HWAudioManager SharedManager()
	{
		if (instance == null)
		{
			instance = new HWAudioManager();
		}
		
		return instance;
	}
	
	/**
	 * Plays a sound effect.
	 * 
	 * @param p_sID The identifier for the sound effect
	 */
	public void PlaySFX(String p_sID)
	{
		m_SoundManager.Play(GetSound(p_sID));
	}
	
	// Helper method that gets a HWSound object, either one that has already been
	// loaded, or it creates a new one.
	private HWSound GetSound(String p_sID)
	{
		HWSound sound;
		
		for (int i = 0; i < m_Sounds.size(); i+=1)
		{
			sound = m_Sounds.get(i);
			
			if (sound.GetIdentifier().equalsIgnoreCase(p_sID))
			{
				return sound;
			}
		}
		
		sound = new HWSound(p_sID);
		m_Sounds.add(sound);
		return sound;
	}
	
	/**
	 * Plays a background music file (currently using MIDI).
	 * 
	 * @param p_sID The name of the background music file
	 */
	public void PlayBGM(String p_sID)
	{
		m_MidiPlayer.Play(GetMidi(p_sID));
	}
	
	// Helper method that gets a midi file, either one that has already been
	// loaded, or a new one using an XML file
	private HWMidiFile GetMidi(String p_sID)
	{
		HWMidiFile midi;
		
		for (int i = 0; i < m_Midis.size(); i+=1)
		{
			midi = m_Midis.get(i);
			
			if (midi.GetIdentifier().equalsIgnoreCase(p_sID))
			{
				return midi;
			}
		}
		
		midi = HWXMLReaderSAX.LoadMidi(p_sID);
		m_Midis.add(midi);
		return midi;
	}
	
	/**
	 * Will eventually be used for playing more background sounds.
	 */
	public void PlayAmbient()
	{
		
	}
}
