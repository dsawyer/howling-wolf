package com.howlingwolf.managers;

// Graphics imports
import com.howlingwolf.graphics.window.HWDialogBox;

/**
 * Maintains all the options used in the game.
 * 
 * @author Dylan Sawyer
 */
public final class HWOptionsManager
{
	private static HWOptionsManager instance;
	
	private long m_MessageSpeed;
	
	private HWOptionsManager()
	{
		m_MessageSpeed = HWDialogBox.MEDIUM_SPEED;
	}
	
	/**
	 * Gets a handle to the HWOptionsManager.
	 * 
	 * @return The singleton instance of the HWOptionsManager
	 */
	public synchronized static HWOptionsManager SharedManager()
	{
		if (instance == null)
		{
			instance = new HWOptionsManager();
		}
		
		return instance;
	}
	
	/**
	 * Gets the Message Speed
	 * 
	 * @return The currently chosen message speed
	 */
	public long GetMessageSpeed()
	{
		return m_MessageSpeed;
	}
	
	/**
	 * Sets the Message Speed
	 * 
	 * @param speed
	 */
	public void SetMessageSpeed(long speed)
	{
		if ((m_MessageSpeed < HWDialogBox.INSTANT_SPEED) || (m_MessageSpeed > HWDialogBox.SLOW_SPEED))
		{
			return;
		}
		
		m_MessageSpeed = speed;
	}
}
