package com.howlingwolf.managers;

// Java imports
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

// Core imports
import com.howlingwolf.util.HWCamera;
/**
 * The HWScreenManager class manages initialising and displaying full screen
 * graphics modes.
 * 
 * @author Dylan Sawyer
 */
public class HWScreenManager
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private final GraphicsDevice m_GraphicsDevice;
	private JFrame m_Window;
	private JFrame m_FullScreenWindow;
	
	private BufferStrategy m_Strategy;
	private final Canvas m_DrawingBoard;
	private final BufferedImage m_DrawDestination;
	
	private boolean m_bIsFullScreen;
	
	private static final Dimension WINDOW_SIZE = new Dimension(1024, 576);
	//private static final Dimension WINDOW_SIZE = new Dimension(512, 288);

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates a new ScreenManager object.
	 */
	public HWScreenManager()
    {
    	GraphicsEnvironment environment =  GraphicsEnvironment.getLocalGraphicsEnvironment();
    	m_GraphicsDevice = environment.getDefaultScreenDevice();
		
		// Canvas
		m_DrawingBoard = new Canvas();
		m_DrawingBoard.setSize(WINDOW_SIZE);
		
		// "Background"
		// An image the size of the camera's view and being opaque
		m_DrawDestination = GetCompatibleImage(HWCamera.CAMERA_WIDTH, HWCamera.CAMERA_HEIGHT, false);
		
		// Buffer
		//SetBufferStrategy();
		
		m_DrawingBoard.setBackground(Color.BLUE);
		m_DrawingBoard.setForeground(Color.white);
    }
	
	// Sets the buffer strategy
	private void SetBufferStrategy()
	{
		m_DrawingBoard.createBufferStrategy(2);
		do
		{
			m_Strategy = m_DrawingBoard.getBufferStrategy();
		} while (m_Strategy == null);
	}
	
	// Creates a hardware accelerated image
	/**
	 * Creates a buffered image that is hardware accelerated.
	 * 
	 * @param width  The width of the image
	 * @param height The height of the image
	 * @param alpha  Whether the image has any alpha or not
	 * @return A hardware accelerated BufferedImage.
	 */
	public final BufferedImage GetCompatibleImage(final int width, final int height, final boolean alpha)
	{
		return m_GraphicsDevice.getDefaultConfiguration().createCompatibleImage(width, height,
				alpha ? Transparency.TRANSLUCENT : Transparency.OPAQUE);
	}
	
    /**
	 * Returns a list of compatible display modes for the default device on the
	 * system.
	 * 
	 * @return The {@link DisplayMode}s that are compatible with the default
	 *		   device
	 */
	public DisplayMode[] GetCompatibleDisplayModes()
    {
    	return m_GraphicsDevice.getDisplayModes();
    }

    /**
	 * Returns the first compatible mode in a list of modes.
	 * 
	 * @param p_modes Array of {@link DisplayMode}
	 * @return The compatible {@link DisplayMode}
	 */
	public DisplayMode FindFirstCompatibleMode(DisplayMode[] p_modes)
    {
    	DisplayMode[] goodModes = m_GraphicsDevice.getDisplayModes();

    	for (int i = 0; i < p_modes.length; i+=1)
    	{
    		for (int j = 0; j < goodModes.length; j+=1)
    		{
    			if (DisplayModesMatch(p_modes[i], goodModes[j]))
    			{
    				return p_modes[i];
    			}
    		}
    	}

    	return null;
    }

    /**
	 * Returns the current display mode.
	 * 
	 * @return The current {@link DisplayMode}
	 */
	public DisplayMode GetCurrentDisplayMode()
    {
    	return m_GraphicsDevice.getDisplayMode();
    }
	
    /**
	 * Determines if two display modes "match", i.e. same resolution,
     * same bit depth, and same refresh rate. Bit depth is ignored if
     * one of the modes has DisplayMode.BIT_DEPTH_MULTI. Also, the
     * refresh rate is ignored if one of the modes has a rate of
     * DisplayMode.REFRESH_RATE_UNKOWN.
	 * 
	 * @param p_mode1 The first mode to check
	 * @param p_mode2 The second mode to check
	 * @return True if the two {@link DisplayMode}s match
	 */
	public boolean DisplayModesMatch(DisplayMode p_mode1, DisplayMode p_mode2)
    {
    	// Check resolution
    	if ((p_mode1.getWidth() != p_mode2.getWidth()) ||
    		(p_mode1.getHeight() != p_mode2.getHeight()))
		{
			return false;
		}

		// Check bit depth
		if ((p_mode1.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI) &&
			(p_mode2.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI) &&
			(p_mode1.getBitDepth() != p_mode2.getBitDepth()))
		{
			return false;
		}

		// Check refresh rate
		if ((p_mode1.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN) &&
			(p_mode2.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN) &&
			(p_mode1.getRefreshRate() != p_mode2.getRefreshRate()))
		{
			return false;
		}

		// All matches are good as far as we're concerned
		return true;
    }
	
    /**
	 * Enters full screen mode and changes the display mode. If the specified
	 * display mode is null or not compatible with this device, or if the
	 * display mode cannot be changed on this system, the current display mode
	 * is used.
	 * 
	 * @param p_DisplayMode The {@link DisplayMode} to use in full screen
	 */
	public void SetFullScreen(DisplayMode p_DisplayMode)
    {
		if (m_Window != null)
		{
			m_Window.dispose();
		}
		
		m_FullScreenWindow = new JFrame();
		m_FullScreenWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m_FullScreenWindow.setUndecorated(true);
    	m_FullScreenWindow.setIgnoreRepaint(true);
		m_FullScreenWindow.setResizable(false);
		
		m_FullScreenWindow.add(m_DrawingBoard);
		m_FullScreenWindow.pack();
		m_DrawingBoard.requestFocusInWindow();
		
    	m_GraphicsDevice.setFullScreenWindow(m_FullScreenWindow);

    	if (p_DisplayMode != null && m_GraphicsDevice.isDisplayChangeSupported())
    	{
    		try
    		{
    			m_GraphicsDevice.setDisplayMode((p_DisplayMode));
    		}
    		catch (IllegalArgumentException ex) {}
    	}
		
		m_bIsFullScreen = true;
		
		SetBufferStrategy();
    }
	
	/**
	 * Enters windowed mode.
	 */
	public void SetWindowed()
	{
		if (m_FullScreenWindow != null)
		{
			m_FullScreenWindow.dispose();
		}
		
		m_GraphicsDevice.setFullScreenWindow(null);
		
		m_Window = new JFrame("2D Engine");
		m_Window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m_Window.setLocation(200, 50);
		
		m_DrawingBoard.setSize(WINDOW_SIZE);
		
		m_Window.add(m_DrawingBoard);
		
		m_Window.pack();
		m_Window.setVisible(true);
		
		m_DrawingBoard.requestFocusInWindow();
		SetBufferStrategy();
		
		m_bIsFullScreen = false;
	}

    /**
	 * Gets the graphics context for the display. The HWScreenManager uses
	 * double buffering, so applications must call Update() to show any
	 * graphics drawn. The application must dispose of the graphics object.
	 * 
	 * @return The {@link Graphics2D} context for the display
	 */
	public Graphics2D GetBackgroundGraphicsContext()
    {
		return (Graphics2D)m_DrawDestination.getGraphics();
    }
	
	/**
	 * Provides access to the actual image that will be drawn onto the canvas.
	 * 
	 * @return	The image drawn upon the canvas
	 */
	public BufferedImage GetDrawingFrame()
	{
		return m_DrawDestination;
	}
	
	private Graphics2D GetBuffer()
	{
		return (Graphics2D)m_Strategy.getDrawGraphics();
	}
	
	// Draws the image that represents the game action to the canvas'
	// graphics context provided
	private void DrawToCanvas(Graphics2D graphics)
	{
		// If the base image and the screen space being used are the same size,
		// just draw the image. Otherwise, scale it.
		if ((m_DrawingBoard.getWidth() == m_DrawDestination.getWidth()) &&
			(m_DrawingBoard.getHeight() == m_DrawDestination.getHeight()))
		{
			graphics.drawImage(m_DrawDestination, 0, 0, null);
		}
		else
		{
			// Uses the rectangle coordinates of the canvas to scale the 
			// base image so that it fits perfectly in the canvas.
			graphics.drawImage(m_DrawDestination, 0, 0, m_DrawingBoard.getWidth(), m_DrawingBoard.getHeight(), null);
		}
	}

    /**
	 * Updates the display.
	 */
	public void UpdateDisplay()
    {
    	do
    	{
			Graphics2D canvasGraphics = GetBuffer();
			
			DrawToCanvas(canvasGraphics);
			canvasGraphics.dispose();
    	} while(!UpdateScreen());

    	// Sync the display on some systems. (On Linux, this fixes event queue problems)
    	Toolkit.getDefaultToolkit().sync();
    }
	
	private boolean UpdateScreen()
	{
		try
		{
			m_Strategy.show();
			Toolkit.getDefaultToolkit().sync();
			return (!m_Strategy.contentsLost());
		}
		catch (NullPointerException | IllegalStateException e)
		{
			return true;
		}
	}

    /**
	 * Returns the window currently used in fullscreen mode. Returns null if not
	 * in fullscreen.
	 * 
	 * @return The window used in full screen, null if not in full screen
	 */
	public JFrame GetFullScreenWindow()
    {
    	return (JFrame)m_GraphicsDevice.getFullScreenWindow();
    }
	
	/**
	 * Gets the window in which the canvas is being used.
	 * 
	 * @return The window being used in windowed mode or full screen mode
	 */
	public JFrame GetWindow()
	{
		if (m_bIsFullScreen)
		{
			return m_FullScreenWindow;
		}
		else
		{
			return m_Window;
		}
	}
	
	/**
	 * Gets the {@link Canvas} upon which the application is being drawn
	 * 
	 * @return The drawing board
	 */
	public Canvas GetDrawingSpace()
	{
		return m_DrawingBoard;
	}

    /**
	 * Returns the width of the window currently used.
	 * 
	 * @return GetWidth of the drawing board
	 */
	public int GetWidth()
    {
		return m_DrawingBoard.getWidth();
    }

    /**
	 * Returns the height of the window currently used.
	 * 
	 * @return GetHeight of the drawing board
	 */
	public int GetHeight()
    {
		return m_DrawingBoard.getHeight();
    }

    /**
	 * Restores the screen's display mode.
	 */
	public void RestoreScreen()
    {
    	if (m_FullScreenWindow != null)
    	{
    		m_FullScreenWindow.dispose();
    	}

    	m_GraphicsDevice.setFullScreenWindow(null);
    }
}