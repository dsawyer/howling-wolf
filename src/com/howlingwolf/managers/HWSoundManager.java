package com.howlingwolf.managers;

// Java imports
import com.howlingwolf.audio.sound.HWSound;
import java.io.*;
import javax.sound.sampled.*;

// Core imports
import com.howlingwolf.util.HWThreadPool;

// Audio Imports

/**
 * The WEHWSoundManager class manages audio playback. It is a HWThreadPool, with each
 thread playing back one sound at a time. This allows the WEHWSoundManager to
 easily limit the number of simultaneous sounds being played.
 <p>
 * Possible additions:<p>
 * - Add a MasterVolume control of some kind, which uses Controls to set the
 * volume for each line.<p>
 * - Don't play a sound if more than, say, 500ms have passed since the request.
 * 
 * @author Dylan Sawyer
 */
public class HWSoundManager extends HWThreadPool
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************
	
	private Object m_PausedLock;
	private boolean m_bPaused;

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates a new HWSoundManager object using the maximum number of
 simultaneous sounds.
	 */
	public HWSoundManager()
    {
    	this(GetMaxSimultaneousSounds(null));
    }

    /**
	 * Creates a new HWSoundManager object with the specified number of
 simultaneous sounds.
	 * 
	 * @param p_iMaxSimultaneousSounds The maximum number of simultaneous sounds
	 */
	public HWSoundManager(int p_iMaxSimultaneousSounds)
    {
    	super(p_iMaxSimultaneousSounds);
    	m_PausedLock = new Object();
    	// Notify threads in pool it's okay to start
    	synchronized (this)
    	{
    		notifyAll();
    	}
    }

    /**
	 * Gets the maximum number of simultaneous sounds with the given GetAudioFormat
	 * that the default mixer can play.
	 * 
	 * @param p_format The {@link GetAudioFormat} to check
	 * @return The maximum number of simultaneous sounds for the given
	 *		   {@link GetAudioFormat}
	 */
	public static int GetMaxSimultaneousSounds(AudioFormat p_format)
    {
    	DataLine.Info lineInfo = new DataLine.Info(SourceDataLine.class, p_format);
    	Mixer mixer = AudioSystem.getMixer(null);
    	int maxLines = mixer.getMaxLines(lineInfo);
    	if (maxLines == AudioSystem.NOT_SPECIFIED)
    	{
    		maxLines = 32;
    	}

    	return maxLines;
    }

    /**
	 * Does any clean up before closing.
	 */
	protected void CleanUp()
    {
    	// Signal to unpause
    	SetPaused(false);

    	// Close the mixer (stops any running sounds)
    	Mixer mixer = AudioSystem.getMixer(null);
    	if (mixer.isOpen())
    	{
    		mixer.close();
    	}
    }

	/**
	 * Cleans up the WEHWSoundManager and closes the HWThreadPool.
	 */
	@Override
    public void Close()
    {
    	CleanUp();
    	super.Close();
    }

	/**
	 * Cleans up the WEHWSoundManager and joins the HWThreadPool.
	 */
	@Override
    public void Join()
    {
    	CleanUp();
    	super.Join();
    }

	/**
	 * Sets the paused state. HWSounds may not pause immediately.
	 * 
	 * @param p_bPause The new paused state
	 */
	public void SetPaused(boolean p_bPause)
	{
		if (m_bPaused != p_bPause)
		{
			synchronized (m_PausedLock)
			{
				m_bPaused = p_bPause;
				if (!m_bPaused)
				{
					// Restart sounds
					m_PausedLock.notifyAll();
				}
			}
		}
	}

	/**
	 * Returns the paused state.
	 * 
	 * @return The paused state.
	 */
	public boolean IsPaused()
	{
		return m_bPaused;
	}

	/**
	 * Plays a sound. This method returns immediately.
	 * 
	 * @param p_sound The {@link HWSound} to play
	 */
	public void Play(HWSound p_sound)
	{
		if (p_sound != null)
		{
			RunTask(new SoundPlayer(p_sound));
		}
	}

	/**
	 * Signals that a PooledThread has started.
	 */
	@Override
	protected void ThreadStarted()
	{
		// Wait for the HWSoundManager constructor to finish
		synchronized (this)
		{
			try
			{
				wait();
			}
			catch (InterruptedException ex) {}
		}
	}

	/**
	 * Signals that a PooledThread has stopped.
	 */
	@Override
	protected void ThreadStopped()
	{
		// Nothing in particular to do here
	}

	// The SoundPlayer class is a task for the PooledThreads to run. It receives
	// the thread's Line and byte buffer from the ThreadLocal variables and
	// plays a sound from an GetInputStream.
	//
	// This class only works when called from a PooledThread
	private class SoundPlayer implements Runnable
	{
		private InputStream m_Source;
		private HWSound m_Sound;
		
		public SoundPlayer(HWSound sound)
		{
			m_Sound = sound;
			m_Source = m_Sound.GetInputStream();
		}

		@Override
		public void run()
		{
			// Use a short, 100ms (1/10th of a second) buffer for filters that
			// change in real-time.
			int bufferSize = m_Sound.GetAudioFormat().getFrameSize() * Math.round(m_Sound.GetAudioFormat().getSampleRate() / 10);

			// Create, open, and start the line
			SourceDataLine line;
			DataLine.Info lineInfo = new DataLine.Info(SourceDataLine.class, m_Sound.GetAudioFormat());

			try
			{
				line = (SourceDataLine)AudioSystem.getLine(lineInfo);
				line.open(m_Sound.GetAudioFormat(), bufferSize);
			}
			catch (LineUnavailableException ex)
			{
				// The line is unavailable
				return;
			}

			line.start();

			// Create the buffer
			byte[] buffer = new byte[bufferSize];

			if (buffer == null)
			{
				// The line is unavailable
				return;
			}

			// Copy data to the line
			try
			{
				int numBytesRead = 0;

				while (numBytesRead != -1)
				{
					// If paused, wait until unpaused
					synchronized (m_PausedLock)
					{
						if (m_bPaused)
						{
							try
							{
								m_PausedLock.wait();
							}
							catch (InterruptedException ex)
							{
								return;
							}
						}
					}

					// Copy data
					numBytesRead = m_Source.read(buffer, 0, buffer.length);
					if (numBytesRead != -1)
					{
						line.write(buffer, 0, numBytesRead);
					}
				}
			}
			catch (IOException ex)
			{
			}
		}
	}
}