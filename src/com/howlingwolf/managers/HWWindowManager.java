package com.howlingwolf.managers;

// Java imports
import java.util.ArrayList;
import java.awt.Graphics2D;

// Engine imports

// Window imports
import com.howlingwolf.graphics.window.*;

// Input import
import com.howlingwolf.input.HWInputManager;

// Core imports
import com.howlingwolf.input.HWInputAction;
import com.howlingwolf.util.HWCamera;
import com.howlingwolf.util.HWExtraTypes;

// Graphics imports

/**
 * Manages all windows visible and any input they may receive.
 * 
 * @author Dylan Sawyer
 */
public final class HWWindowManager
{
	private HWWindow m_ActiveWindow;
	
	private ArrayList<HWBorderedWindow> m_BackgroundWindows;
	
	private ArrayList<String> m_Dialogs;
	
	private HWInputManager inputManager;
	
	private HWInputAction moveUp;
	private HWInputAction moveDown;
	private HWInputAction moveRight;
	private HWInputAction moveLeft;
	private HWInputAction accept;
	private HWInputAction cancel;
	private HWInputAction menu;
	
	private static final int DIALOG_WIDTH = HWCamera.CAMERA_WIDTH;
	private static final int DIALOG_HEIGHT = 96;
	private static final int DIALOG_X = 0;
	private static final int DIALOG_Y = HWCamera.CAMERA_HEIGHT - DIALOG_HEIGHT;
	
	/**
	 * Creates a new HWBorderedWindowManager object.
	 * 
	 * @param input The HWInputManager that allows this to respond to user input
				based on the current key bindings
	 */
	public HWWindowManager(HWInputManager input)
	{
		m_Dialogs = new ArrayList<>();
		m_BackgroundWindows = new ArrayList<>();
		inputManager = input;
		
		moveLeft = inputManager.GetKeyAction(HWGameManager.MOVE_LEFT);
		moveRight = inputManager.GetKeyAction(HWGameManager.MOVE_RIGHT);
		moveUp = inputManager.GetKeyAction(HWGameManager.MOVE_UP);
		moveDown = inputManager.GetKeyAction(HWGameManager.MOVE_DOWN);
		
		accept = inputManager.GetKeyAction(HWGameManager.ACTION);
		cancel = inputManager.GetKeyAction(HWGameManager.CANCEL);
		
		menu = inputManager.GetKeyAction(HWGameManager.MENU);
	}
	
	/**
	 * Checks to see if there is an active window or not.
	 * 
	 * @return True if there is an active window, false otherwise
	 */
	public boolean HasActiveWindow()
	{
		if (m_ActiveWindow != null)
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Adds a string of dialog to the game, either by creating a new dialog box
	 * or adding the dialog to the list that will proceed after the current
	 * dialog box's message has been seen.
	 * 
	 * @param dialog The new string of dialog to be displayed
	 */
	public void AddDialog(String dialog)
	{
		if (m_ActiveWindow == null)
		{
			m_ActiveWindow = new HWDialogBox(DIALOG_X, DIALOG_Y, DIALOG_WIDTH, DIALOG_HEIGHT, dialog);
		}
		else
		{
			m_Dialogs.add(dialog);
		}
	}
	
	/**
	 * Adds a set of dialog strings to the game, either by creating a new dialog
	 * box or adding the dialog to the list that will proceed after the current
	 * dialog box's message has been seen.
	 * 
	 * @param dialog The set of strings to be used in the dialog box
	 */
	public void AddDialog(ArrayList<String> dialog)
	{
		if (m_ActiveWindow == null)
		{
			m_ActiveWindow = new HWDialogBox(DIALOG_X, DIALOG_Y, DIALOG_WIDTH, DIALOG_HEIGHT, dialog.get(0));
		}
		
		if (dialog.size() > 1)
		{
			m_Dialogs.addAll(dialog.subList(1, dialog.size()));
		}
	}
	
	/**
	 * Adds a menu window at the given spot with the given options.
	 * 
	 * @param x X position of the window
	 * @param y Y position of the window
	 * @param options List of options available to the player
	 */
	/*public void AddMenu(int x, int y, ArrayList<String> options)
	{
		if (m_ActiveWindow != null)
		{
			m_BackgroundWindows.add(m_ActiveWindow);
		}
		
		m_ActiveWindow = new HWMenuWindow(x, y, options);
		ModifyDirectionalKeyBehaviour(HWInputAction.DETECT_INITIAL_PRESS_ONLY);
	}*/
	
	// Little extra something to keep the cursor from flying from one choice to
	// another in the menus...
	private void ModifyDirectionalKeyBehaviour(int behaviour)
	{
		moveUp.SetBehaviour(behaviour);
		moveDown.SetBehaviour(behaviour);
		moveLeft.SetBehaviour(behaviour);
		moveRight.SetBehaviour(behaviour);
	}
	
	/**
	 * Checks the input for the active window and deals with it according to
	 * what type of window it is.
	 * 
	 * @param elapsedTime The amount of time since the last check
	 */
	public void CheckInput(long elapsedTime)
	{
		if (m_ActiveWindow != null)
		{
			// Check for what kind of window is active
			// Dialog box
			if (m_ActiveWindow instanceof HWDialogBox)
			{
				CheckDialogInput();
			}
			// Menu window
			else if (m_ActiveWindow instanceof HWMenuWindow)
			{
				CheckMenuInput();
			}
			
			inputManager.ResetAllGameActions();
		}
	}
	
	// Checks the input for when a dialog box is active
	private void CheckDialogInput()
	{
		if (m_ActiveWindow.GetState() == HWWindow.STATE_OPEN)
		{
			HWDialogBox dialog = (HWDialogBox)m_ActiveWindow;

			if (dialog.DisplayingFullMessage())
			{
				if (accept.GetBehaviour() == HWInputAction.NORMAL)
				{
					accept.SetBehaviour(HWInputAction.DETECT_INITIAL_PRESS_ONLY);
					accept.PartialReset();
				}
				
				if (accept.IsPressed() || cancel.IsPressed())
				{
					// If there is more dialog, load that
					if (!m_Dialogs.isEmpty())
					{
						dialog.Reset(m_Dialogs.remove(0));
						dialog.SetDisplaySpeed(HWOptionsManager.SharedManager().GetMessageSpeed());
					}
					else // Otherwise, close the window
					{
						dialog.SetState(HWWindow.STATE_CLOSING);
					}
				}
			}
			else if (cancel.IsPressed())
			{
				// Double the display speed
				dialog.SetDisplaySpeed(HWDialogBox.INSTANT_SPEED);
			}
			else if (accept.IsPressed())
			{
				dialog.SetDisplaySpeed(HWOptionsManager.SharedManager().GetMessageSpeed() / 2);
			}
			else
			{
				if (accept.GetBehaviour() == HWInputAction.DETECT_INITIAL_PRESS_ONLY)
				{
					accept.SetBehaviour(HWInputAction.NORMAL);
				}
				dialog.SetDisplaySpeed(HWOptionsManager.SharedManager().GetMessageSpeed());
			}
		}
	}
	
	// Checks the input for an active menu
	private void CheckMenuInput()
	{
		if (m_ActiveWindow.GetState() == HWWindow.STATE_OPEN)
		{
			HWMenuWindow menuWindow = (HWMenuWindow)m_ActiveWindow;
			
			if (accept.IsPressed())
			{
				String option = menuWindow.FindCurrentSelectedOption();
				// Do menu logic here... probably make a menu manager
				// that has it's own window manager, so this would potentially
				// return whatever was selected
				
				HWAudioManager.SharedManager().PlaySFX(HWExtraTypes.SOUND_SELECT);
				
				if (option.equals("Continue"))
				{
					menuWindow.SetState(HWWindow.STATE_CLOSING);
				}
			}
			else if (moveDown.IsPressed())
			{
				menuWindow.ChangeSelection(HWMenuWindow.CURSOR_DOWN);
				HWAudioManager.SharedManager().PlaySFX(HWExtraTypes.SOUND_CURSOR);
			}
			else if (moveUp.IsPressed())
			{
				menuWindow.ChangeSelection(HWMenuWindow.CURSOR_UP);
				HWAudioManager.SharedManager().PlaySFX(HWExtraTypes.SOUND_CURSOR);
			}
		}
	}
	
	/**
	 * Draws the active window to the screen as well as any underlying windows.
	 * 
	 * @param graphics The graphics context upon which the window is drawn
	 */
	public void Draw(Graphics2D graphics)
	{
		// Draw any windows underneath the current one
		if (!m_BackgroundWindows.isEmpty())
		{
			for (HWBorderedWindow window : m_BackgroundWindows)
			{
				window.Draw(graphics);
			}
		}
		
		if (m_ActiveWindow != null)
		{
			m_ActiveWindow.Draw(graphics);
		}
	}
	
	/**
	 * Updates the active window, and discards it if it has closed.
	 * 
	 * @param elapsedTime The amount of time since the last update
	 */
	public void Update(long elapsedTime)
	{
		if (m_ActiveWindow != null)
		{
			m_ActiveWindow.Update(elapsedTime);
			
			if (m_ActiveWindow.GetState() == HWWindow.STATE_CLOSED)
			{
				m_ActiveWindow = null;
				
				if (!m_BackgroundWindows.isEmpty())
				{
					m_ActiveWindow = m_BackgroundWindows.remove(m_BackgroundWindows.size() - 1);
				}
				else
				{
					inputManager.ResetAllGameActions();
					ModifyDirectionalKeyBehaviour(HWInputAction.NORMAL);
				}
			}
		}
	}
}