package com.howlingwolf.managers;

// Java imports
import com.howlingwolf.input.HWInputManager;
import java.awt.*;
import java.awt.event.KeyEvent;

// Core imports
import com.howlingwolf.input.HWInputAction;
import com.howlingwolf.util.HWCamera;
import com.howlingwolf.util.HWGameCore;

// Event imports
import com.howlingwolf.event.HWEventManager;

// Scene imports
import com.howlingwolf.scene.HWSceneManager;

// Graphics imports
import com.howlingwolf.graphics.HWImageFilter;

/**
 * Manages everything to do with the game being played.
 * 
 * @author Dylan Sawyer
 */
public class HWGameManager extends HWGameCore
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************
	
	private HWInputManager m_InputManager;
	private HWSceneManager m_SceneManager;

	private HWInputAction moveLeft;
	private HWInputAction moveRight;
	private HWInputAction moveUp;
	private HWInputAction moveDown;
	private HWInputAction action;
	private HWInputAction cancel;
	private HWInputAction menu;
	private HWInputAction pause;
	private HWInputAction exit;
	private HWInputAction toggleFullscreen;
	private HWInputAction toggleContrast;
	private HWInputAction toggleGrayscale;
	
	private boolean m_bIsContrasting = false;
	private boolean m_bIsGrayscale = false;
	
	//**************************************************************************
	// Constants
	//**************************************************************************
	
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String MOVE_LEFT = "moveLeft";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String MOVE_RIGHT = "moveRight";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String MOVE_UP = "moveUp";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String MOVE_DOWN = "moveDown";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String ACTION = "action";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String CANCEL = "cancel";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String MENU = "menu";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String PAUSE = "pause";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String EXIT = "exit";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String TOGGLE_SCREEN = "toggleFullscreen";
	/**
	 * GetIdentifier value for the associated HWInputAction.
	 */
	public static final String TOGGLE_HITBOXES = "toggleHitboxes";
	
	public static final String TOGGLE_CONTRAST = "toggleContrast";
	
	public static final String TOGGLE_GRAYSCALE = "toggleGrayscale";

	//**************************************************************************
	// Methods
	//**************************************************************************

	/**
	 * Set the ball rolling. Get the game started.
	 * 
	 * @param args N/A
	 */
	public static void main(String[] args)
	{
		new HWGameManager().Run();
	}

	@Override
	public void Init()
	{
		super.Init();

		// Setup the input manager
		InitInput();

		// Initialise the scene manager
		m_SceneManager = HWSceneManager.SharedManager();
	}

	// Closes any resources used by the HWGameManager
	@Override
	public void Stop()
	{
		super.Stop();
	}

	// Initialises all the inputs for the game
	private void InitInput()
	{
		moveLeft = new HWInputAction(MOVE_LEFT);
		moveRight = new HWInputAction(MOVE_RIGHT);
		moveUp = new HWInputAction(MOVE_UP);
		moveDown = new HWInputAction(MOVE_DOWN);
		action = new HWInputAction(ACTION, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		cancel = new HWInputAction(CANCEL, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		menu = new HWInputAction(MENU, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		pause = new HWInputAction(PAUSE, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		exit = new HWInputAction(EXIT, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		toggleFullscreen = new HWInputAction(TOGGLE_SCREEN, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		HWInputAction toggleHitboxes = new HWInputAction(TOGGLE_HITBOXES, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		toggleContrast = new HWInputAction(TOGGLE_CONTRAST, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		toggleGrayscale = new HWInputAction(TOGGLE_GRAYSCALE, HWInputAction.DETECT_INITIAL_PRESS_ONLY);
		
		m_InputManager = new HWInputManager(screen.GetDrawingSpace());
		m_InputManager.SetListeners();
		m_InputManager.SetCursor(HWInputManager.INVISIBLE_CURSOR);

		m_InputManager.MapToKey(moveLeft, KeyEvent.VK_A);
		m_InputManager.MapToKey(moveRight, KeyEvent.VK_D);
		m_InputManager.MapToKey(moveUp, KeyEvent.VK_W);
		m_InputManager.MapToKey(moveDown, KeyEvent.VK_S);
		m_InputManager.MapToKey(action, KeyEvent.VK_SPACE);
		m_InputManager.MapToKey(cancel, KeyEvent.VK_BACK_SPACE);
		m_InputManager.MapToKey(menu, KeyEvent.VK_ENTER);
		m_InputManager.MapToKey(pause, KeyEvent.VK_P);
		m_InputManager.MapToKey(exit, KeyEvent.VK_ESCAPE);
		m_InputManager.MapToKey(toggleFullscreen, KeyEvent.VK_F);
		m_InputManager.MapToKey(toggleHitboxes, KeyEvent.VK_H);
		m_InputManager.MapToKey(toggleContrast, KeyEvent.VK_C);
		m_InputManager.MapToKey(toggleGrayscale, KeyEvent.VK_G);
	}

	// Checks for input
	private void CheckInput(long elapsedTime)
	{	
		if (exit.IsPressed())
		{
			Stop();
		}
		
		if (toggleFullscreen.IsPressed())
		{
			// For some reason, it is not releasing on it's own when
			// going from full screen to windowed mode, thus making the next
			// key input invalid...
			// Quick fix: force the release.
			ToggleFullscreen();
		}
		
		if (toggleContrast.IsPressed())
		{
			m_bIsContrasting = !m_bIsContrasting;
		}
		
		if (toggleGrayscale.IsPressed())
		{
			m_bIsGrayscale = !m_bIsGrayscale;
		}
	}
	
	private void ToggleFullscreen()
	{
		if (screen.GetFullScreenWindow() == null)
		{
			// Go to fullscreen mode
			screen.SetFullScreen(currentFullScreenDisplay);
		}
		else
		{
			// Go to windowed mode
			screen.SetWindowed();
			// All the GameActions are temporarily frozen when changing to
			// windowed mode for some reason...
			m_InputManager.ResetAllGameActions();
		}
	}
	
	/**
	 * Gets the HWInputManager being used with this instance of the game manager.
	 * 
	 * @return	A handle to the input manager
	 */
	public HWInputManager GetInputManager()
	{
		return m_InputManager;
	}

	// Draws everything
	@Override
	public void Draw(Graphics2D g, float p_fInterpolation)
	{
		// Currently not using the interpolation value
		
		// Draw a background....
		g.setColor(Color.black);
		g.fillRect(0, 0, HWCamera.CAMERA_WIDTH, HWCamera.CAMERA_HEIGHT);
		
		// Draw the area
		m_SceneManager.Draw(g);
		
		if (m_bIsContrasting)
		{
			HWImageFilter.FilterContrast(GetDrawSpace());
		}
		
		if (m_bIsGrayscale)
		{
			HWImageFilter.FilterGrayscale(GetDrawSpace());
		}
	}

	// Updates Animation, position, and velocity of all Sprites in the current map
	@Override
	public void Update(long elapsedTime)
	{
		// Get keyboard/mouse input
		CheckInput(elapsedTime);
		
		// Update any events
		HWEventManager.SharedManager().Update(elapsedTime, Integer.MAX_VALUE);
		
		// Update the scene
		m_SceneManager.Update(elapsedTime);
	}
}