package com.howlingwolf.managers;

// Java imports
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

// Graphics imports
import com.howlingwolf.graphics.HWSpritesheet;
import com.howlingwolf.graphics.tileEngine.HWTileset;

// Core imports
import com.howlingwolf.util.xml.HWXMLReaderSAX;

/**
 * Manages the loading, accessing, and deleting of various resources. Keeps
 * track of graphics and audio resources.
 * 
 * @author Dylan Sawyer
 */
public final class HWResourceManager
{
	//*************************************************************************
	// Member Variables
	//*************************************************************************

	private static HWResourceManager instance;
	
	private final ArrayList<HWTileset> m_Tilesets;
	private final ArrayList<HWSpritesheet> m_SpriteSheets;

	//**************************************************************************
	// Singleton Methods
	//**************************************************************************
	
	// Constructor - Private
	// Only called when initialising the HWResourceManager
    private HWResourceManager()
    {
    	// Do basic setup here for lists
		m_Tilesets = new ArrayList<>();
		m_SpriteSheets = new ArrayList<>();
    }

    /**
	 * Gets a handle to the instance of the HWResourceManager. If the
	 * HWResourceManager has not yet been initialised, then it creates a new
	 * HWResourceManager.
	 * 
	 * @return A handle to the HWResourceManager
	 */
	public static synchronized HWResourceManager SharedManager()
    {
    	if (instance == null)
    	{
    		instance = new HWResourceManager();
    	}

    	return instance;
    }

    //*************************************************************************
	// Methods
	//*************************************************************************

	//*************************************************************************
	// Image Loading
	//*************************************************************************
	
	/**
	 * Loads an Image from a given file.
	 * 
	 * @param p_sName The filename of the image
	 * @return An {@link Image}
	 */
	public Image LoadImage(String p_sName)
	{
		return new ImageIcon(getClass().getClassLoader().getResource(p_sName)).getImage();

	}
	
	/**
	 * Loads a BufferedImage from a given file.
	 * 
	 * @param p_sName The filename of the image
	 * @return A {@link BufferedImage}
	 */
	public BufferedImage LoadBufferedImage(String p_sName)
	{
		try
		{
			return ImageIO.read(getClass().getClassLoader().getResource(p_sName));
		}
		catch (IOException e)
		{
			return null;
		}
	}

	//*************************************************************************
	// Resource Accessing
	//*************************************************************************
	
	/**
	 * Gets a {@link HWTileset} from the currently loaded ones. If the desired
	 * {@link HWTileset} does not exist, the HWResourceManager loads it.
	 * 
	 * @param p_sName The identifier for the {@link HWTileset}, as well as it's
	 *				  filename
	 * @return The requested {@link HWTileset}
	 */
	public HWTileset GetTileset(String p_sName)
	{
		HWTileset tiles;
		
		for (int i = 0; i < m_Tilesets.size(); i+=1)
		{
			tiles = m_Tilesets.get(i);
			if (tiles.GetIdentifier().equals(p_sName))
			{
				return tiles;
			}
		}
		
		// Create a new HWTileset, save it and return it
		tiles = HWXMLReaderSAX.LoadTileset(p_sName);
		m_Tilesets.add(tiles);
		return tiles;
	}
	
	/**
	 * Gets a {@link HWSpritesheet} from the currently loaded ones. If the desired
	 * {@link HWSpritesheet} does not exist, the HWResourceManager loads it.
	 * 
	 * @param p_sName The identifier for the {@link HWSpritesheet}, as well as
	 *				  it's filename
	 * @return The requested {@link HWSpritesheet}
	 */
	public HWSpritesheet GetSpritesheet(String p_sName)
	{
		HWSpritesheet sprites;
		
		for (int i = 0; i < m_SpriteSheets.size(); i+=1)
		{
			sprites = m_SpriteSheets.get(i);
			if (sprites.GetIdentifier().equals(p_sName))
			{
				return sprites;
			}
		}

		// Create a new HWSpritesheet, save it and return it
		sprites = HWXMLReaderSAX.LoadSpritesheet(p_sName);
		m_SpriteSheets.add(sprites);
		return sprites;
	}
}