package com.howlingwolf.managers;

// Core imports
import com.howlingwolf.scene.*;

// Engine imports

// Utility imports
import com.howlingwolf.util.HWCamera;
import com.howlingwolf.util.HWExtraTypes.Direction;

// Event imports
import com.howlingwolf.event.HWEventManager;

// Physics imports
import com.howlingwolf.util.math.HWVec2;
import com.howlingwolf.physics.modules.HWSize;

// Java imports
import java.awt.Graphics2D;

/**
 * This class handles any transitions that take place.
 * 
 * @author Dylan Sawyer
 */
public class HWTransitionManager
{
	private static final int AREA_TRANSITION_PADDING = 12;
	
	private static final float MOVEMENT_RATIO = 0.02f;
	
	// Used for area transitions
	private Direction m_Direction;
	
	private HWScene m_SceneFrom;
	private HWScene m_SceneTo;
	
	/**
	 * Creates a new TransitionManager. Nothing special.
	 */
	public HWTransitionManager()
	{
		// Empty, nothing to initialise
	}
	
	/**
	 * Prepares to perform a transition between two Areas. This transition can
	 * go in one of four possible directions. It places the HWScene being traveled
	 * to in the direction the transition takes place, and gradually slides
	 * the HWScenes over.
	 * 
	 * @param p_direction	The direction in which the player will be traveling
	 * @param p_newScene	The new scene to be moved to
	 */
	public void CreateSlideTransition(Direction p_direction, HWScene p_newScene)
	{
		m_Direction = p_direction;
		m_SceneFrom = HWSceneManager.SharedManager().GetCurrentScene();
		m_SceneTo = p_newScene;
		
		// Because of the HWCamera object's centrality to all the drawing...
		// It needs to be modified during the transitions, while also modifying
		// the Player's position without the Player modifying the HWCamera's
		// position.
		
		// If going to the right
		/*if (m_Direction == Direction.RIGHT)
		{
			
			
		}
		// If going to the left
		else if (m_Direction == Direction.LEFT)
		{
			
		}
		// If going down
		else if (m_Direction == Direction.DOWN)
		{
			
			
		}
		// If going up
		else if (m_Direction == Direction.UP)
		{
			
		}*/
	}
	
	// Sets the Player's position and the TiledArea offsets to work within the
	// normal flow of things once more. Also resets the HWCamera's view size.
	private void CompletedTransition()
	{
		/*
		HWCamera.Handle().SetWorldSize(new HWSize(m_SceneTo.GetWidth(), m_SceneTo.GetHeight()));
		
		if (m_Direction == Direction.RIGHT)
		{
			m_SceneTo.ChangeOffset(-m_SceneFrom.GetWidthInTiles(), 0);
			m_SceneFrom = null;
			
			m_Player.Place(new HWVec2(AREA_TRANSITION_PADDING, m_Player.GetPosition().GetY()));
		}
		else if (m_Direction == Direction.LEFT)
		{
			m_SceneFrom = null;
			
			m_Player.Place(new HWVec2(m_SceneTo.GetWidth() - AREA_TRANSITION_PADDING - m_Player.GetHitbox().GetWidth(), m_Player.GetPosition().GetY()));
		}
		else if (m_Direction == Direction.UP)
		{
			m_SceneFrom = null;
			
			m_Player.Place(new HWVec2(m_Player.GetPosition().GetX(), m_SceneTo.GetHeight() - AREA_TRANSITION_PADDING - m_Player.GetHitbox().GetHeight()));
		}
		else if (m_Direction == Direction.DOWN)
		{
			m_SceneTo.ChangeOffset(0, -m_SceneFrom.GetHeightInTiles());
			m_SceneFrom = null;
			
			m_Player.Place(new HWVec2(m_Player.GetPosition().GetX(), AREA_TRANSITION_PADDING));
		}
		
		m_Direction = null;
		HWEventManager.SharedManager().AddEvent(new HWTransitionEvent(m_SceneTo));
		*/
	}
	
	// This does all the heavy lifting for updating positions during the
	// AreaTransitions.
	private void UpdateAreaTransition(long p_lElapsedTime)
	{
		HWCamera camera = HWCamera.Handle();
		float xoffset = 0.0f, yoffset = 0.0f;
		int pxoffset = 0, pyoffset = 0;
		
		if (m_Direction == Direction.RIGHT)
		{
			
		}
		else if (m_Direction == Direction.LEFT)
		{
			
		}
		else if (m_Direction == Direction.DOWN)
		{
			
		}
		else if (m_Direction == Direction.UP)
		{
			
		}
		
		camera.SetPosition(camera.GetPosition().GetX() + xoffset, camera.GetPosition().GetY() + yoffset);
	}
	
	/**
	 *
	 * @param p_lElapsedTime
	 */
	public void Update(long p_lElapsedTime)
	{
		if (m_Direction != null)
		{
			UpdateAreaTransition(p_lElapsedTime);
		}
	}
	
	/**
	 *
	 * @param graphics
	 */
	public void Draw(Graphics2D graphics)
	{
		//m_SceneFrom.Draw(graphics);
		m_SceneTo.Draw(graphics);
		
		//DrawForeground(graphics);
	}
	
	/**
	 *
	 * @param graphics
	 */
	private void DrawForeground(Graphics2D graphics)
	{
		//m_SceneFrom.DrawForeground(graphics);
		//m_SceneTo.DrawForeground(graphics);
	}
	
	/**
	 *
	 * @param graphics
	 */
	public void DrawHitboxes(Graphics2D graphics)
	{
		//m_SceneFrom.DrawHitboxes(graphics);
		//m_SceneTo.DrawHitboxes(graphics);
	}
}