package com.howlingwolf.physics.managers;

// Physics imports
import com.howlingwolf.util.math.*;
import com.howlingwolf.physics.modules.*;

/**
 * Collection of functions used for calculating collisions.
 * 
 * @author Dylan Sawyer
 */
public class HWCollisionManager
{
	private static final float INTERSECTION_TOLERANCE = 0.01f;
	
	/**
	 * This will throw an error if there is an attempt to call it. Subclasses
	 * should make their constructors private.
	 */
		public HWCollisionManager()
	{ throw new AssertionError(); }
	
	/**
	 * Resolves the collisions between a physics body and a physics shape.
	 * 
	 * @param bodyA	The body involved in the collision
	 * @param geom	The object (generally static) with which the body may collide
	 * @return		True if the two objects collided, False otherwise
	 */
	public static boolean CollisionResolution(HWPhysicsBody bodyA, HWPhysicsShape geom)
	{
		HWVec2 intersection = DidShapesCollide(bodyA.GetPhysicsShape(), geom);
		
		if (intersection == null)
		{
			return false;
		}
		else if (intersection.MagnitudeSquared() < INTERSECTION_TOLERANCE)
		{
			return false;
		}
		else
		{
			// Tell the body that it collided with an object, and the depth of
			// the collision
			bodyA.CollidedWith(geom.GetParent(), intersection);
			return true;
		}
	}
	
	/**
	 * Uses a projection method to test each polygon's edge normals to see if
	 * projecting the polygons along that axis causes them to overlap. If they
	 * overlap on all of the projected axis, then they have intersected.
	 * <p>
	 * Refer to http://elancev.name/oliver/2D%20polygon.htm for full details
	 * on the algorithm.
	 */
	private static HWVec2 DidShapesCollide(HWPhysicsShape objectA, HWPhysicsShape objectB)
	{
		HWVec2 MTD = new HWVec2(0,0);
		HWVec2 temp;
		float mtdMagSquared = Float.MAX_VALUE;
		float tempMagSquared;
		
		// Get a couple of variables ready to go
		HWVec2 edge, normal;
		
		// Check to see if projecting the polygons on the normals of each edge
		// from polygon A seperates the polygons along that axis
		for (int i = 0; i < objectA.GetShape().GetNumVertices(); i+=1)
		{
			edge = HWMathFunctions.SubtractVectors(objectA.GetShape().GetVertex(i), objectA.GetShape().GetVertex(i+1));
			normal = new HWVec2(-edge.GetY(), edge.GetX());
			
			temp = MTDSeperatingPolygons(normal, objectA.GetShape(), objectB.GetShape());
			
			if (temp == null)
			{
				return null;
			}
			
			// Check to see if the resulting push vector covers a shorter
			// distance than the previous ones
			tempMagSquared = temp.MagnitudeSquared();
			
			if (tempMagSquared < mtdMagSquared)
			{
				mtdMagSquared = tempMagSquared;
				MTD = temp;
			}
		}
		
		// Check to see if projecting the polygons on the normals of each edge
		// from polygon B seperates the polygons along that axis
		for (int i = 0; i < objectB.GetShape().GetNumVertices(); i+=1)
		{
			edge = HWMathFunctions.SubtractVectors(objectB.GetShape().GetVertex(i), objectB.GetShape().GetVertex(i+1));
			normal = new HWVec2(-edge.GetY(), edge.GetX());
			
			temp = MTDSeperatingPolygons(normal, objectA.GetShape(), objectB.GetShape());
			
			if (temp == null)
			{
				return null;
			}
			
			// Check to see if the resulting push vector covers a shorter
			// distance than the previous ones
			tempMagSquared = temp.MagnitudeSquared();
			
			if (tempMagSquared < mtdMagSquared)
			{
				mtdMagSquared = tempMagSquared;
				MTD = temp;
			}
		}
		
		// The polygons are not seperated, and thus intersect
		return MTD;
	}
	
	// Helper method for use with the DidShapesCollide method
	private static HWVec2 MTDSeperatingPolygons(HWVec2 axis, HWPolygon polyA, HWPolygon polyB)
	{
		float minA, minB, maxA, maxB;
		float dot = HWMathFunctions.DotProduct(axis, polyA.GetVertex(0));
		minA = maxA = dot;
		
		// Calculate the maximum and minimum values of the interval when
		// polygon A is projected onto the line defined by axis
		for (int i = 1; i < polyA.GetNumVertices(); i+=1)
		{
			dot = HWMathFunctions.DotProduct(axis, polyA.GetVertex(i));
			
			if (dot < minA)
			{
				minA = dot;
			}
			else if (dot > maxA)
			{
				maxA = dot;
			}
		}
		
		dot = HWMathFunctions.DotProduct(axis, polyB.GetVertex(0));
		minB = maxB = dot;
		
		// Calculate the maximum and minimum values of the interval when
		// polygon B is projected onto the line defined by axis
		for (int i = 1; i < polyB.GetNumVertices(); i+=1)
		{
			dot = HWMathFunctions.DotProduct(axis, polyB.GetVertex(i));
			
			if (dot < minB)
			{
				minB = dot;
			}
			else if (dot > maxB)
			{
				maxB = dot;
			}
		}
		
		// If the intervals overlap, then the axis does not seperate the polygons
		if ((minA > maxB) || (minB > maxA))
		{
			return null;
		}
		
		// Locate the amount of overlap on the intervals
		float d0 = maxA - minB;
		float d1 = maxB - minA;
		float depth = (d0 < d1) ? d0 : d1;
		
		// Convert the seperation axis into a push vector
		// (re-normalise the the axis and multiply by the interval overlap)
		float axisLengthSquared = axis.MagnitudeSquared();
		HWVec2 intersectionDepth = axis.Clone();
		intersectionDepth.Scale(depth / axisLengthSquared);
		
		return intersectionDepth;
	}
	
	/**
	 * Gets the intersecting depth (x and y) of two rectangles. If the two
	 * rectangles are not intersecting, returns null.
	 * 
	 * @param rectA	The first rectangle
	 * @param rectB	The second rectangle
	 * @return		The depth at which the two rectangles intersect, null if they
	 *				do not intersect
	 */
	public static HWVec2 GetAABBIntersectionDepth(HWBoundingBox rectA, HWBoundingBox rectB)
	{
		// Calculate half sizes.
		float halfWidthA = rectA.GetWidth() / 2.0f;
		float halfHeightA = rectA.GetHeight() / 2.0f;
		float halfWidthB = rectB.GetWidth() / 2.0f;
		float halfHeightB = rectB.GetHeight() / 2.0f;

		// Calculate centers.
		HWVec2 centerA = new HWVec2(rectA.GetLeft() + halfWidthA, rectA.GetTop() + halfHeightA);
		HWVec2 centerB = new HWVec2(rectB.GetLeft() + halfWidthB, rectB.GetTop() + halfHeightB);

		// Calculate current and minimum-non-intersecting distances between centers.
		float distanceX = centerA.GetX() - centerB.GetX();
		float distanceY = centerA.GetY() - centerB.GetY();
		float minDistanceX = halfWidthA + halfWidthB;
		float minDistanceY = halfHeightA + halfHeightB;

		// If we are not intersecting at all, return (0, 0).
		if (Math.abs(distanceX) >= minDistanceX || Math.abs(distanceY) >= minDistanceY)
		{
			return null;
		}

		// Calculate and return intersection depths.
		float depthX = distanceX > 0 ? minDistanceX - distanceX : -minDistanceX - distanceX;
		float depthY = distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY;
		return new HWVec2(depthX, depthY);
	}
}