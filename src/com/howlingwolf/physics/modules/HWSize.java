package com.howlingwolf.physics.modules;

/**
 * A representation of a physical object's size.
 * 
 * @author Dylan Sawyer
 */
public class HWSize
{
	private int width;
	private int height;
	
	/**
	 * Creates an empty Size.
	 */
	public HWSize()
	{
		this(0, 0);
	}
	
	/**
	 * Creates a size with the specified dimensions.
	 * 
	 * @param p_width The width of the new size
	 * @param p_height The height of the new size
	 */
	public HWSize(int p_width, int p_height)
	{
		width = p_width;
		height = p_height;
	}
	
	/**
	 * Makes a duplicate of this size.
	 * 
	 * @return A copy of this size
	 */
	public HWSize Clone()
	{
		return new HWSize(width, height);
	}
	
	/**
	 * Gets the width of the size.
	 * 
	 * @return The width
	 */
	public int GetWidth()
	{
		return width;
	}
	
	/**
	 * Sets the width of the size.
	 * 
	 * @param p_width The new width
	 */
	public void SetWidth(int p_width)
	{
		width = p_width;
	}
	
	/**
	 * Gets the height of the size.
	 * 
	 * @return The height
	 */
	public int GetHeight()
	{
		return height;
	}
	
	/**
	 * Set the height of the size.
	 * 
	 * @param p_height The new height
	 */
	public void SetHeight(int p_height)
	{
		height = p_height;
	}
	
	@Override
	public String toString()
	{
		return ("(" + width + ", " + height + ")");
	}
}
