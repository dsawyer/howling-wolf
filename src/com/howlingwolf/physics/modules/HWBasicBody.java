package com.howlingwolf.physics.modules;

// Math import
import com.howlingwolf.util.math.HWVec2;

/**
 * Just a simple implementation of the HWPhysicsBody interface for testing
 * purposes.
 * 
 * @author Dylan Sawyer
 */
public class HWBasicBody implements HWPhysicsBody
{
	private final HWPhysicsShape m_Shape;
	
	/**
	 * Creates a new HWBasicBody that has no special aspects to it. This is
	 * mainly for testing a few things.
	 * 
	 * @param shape	The shape that defines the physical border of the body
	 */
	public HWBasicBody(HWPhysicsShape shape)
	{
		m_Shape = shape;
	}
	
	@Override
	public HWPhysicsShape GetPhysicsShape()
	{
		return m_Shape;
	}
	
	@Override
	public void CollidedWith(HWPhysicsBody body, HWVec2 depth)
	{
		// Do nothing
	}
	
	@Override
	public void StaticCollisionWith(HWPhysicsShape shape, HWVec2 depth)
	{
		// Do nothing
	}
}
