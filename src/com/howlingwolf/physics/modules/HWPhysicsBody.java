package com.howlingwolf.physics.modules;

// Math imports
import com.howlingwolf.util.math.HWVec2;

/**
 * Interface that all objects that have a physical aspect to them must
 * implement. It merely provides access to the physical portion of the object.
 * 
 * @author Dylan Sawyer
 */
public interface HWPhysicsBody
{
	
	/**
	 * Gets the object's corresponding physical attributes.
	 * 
	 * @return	The physical shape of the containing object
	 */
	public HWPhysicsShape GetPhysicsShape();
	
	/**
	 * Processes a collision with another HWPhysicsBody.
	 * 
	 * @param body	The body with which this one collided
	 * @param depth	The depth at which the bodies collided
	 */
	public void CollidedWith(HWPhysicsBody body, HWVec2 depth);
	
	/**
	 * Processes a collision with a static shape.
	 * 
	 * @param shape	The HWPhysicsShape with which this body collided
	 * @param depth	The depth at which the two objects collided
	 */
	public void StaticCollisionWith(HWPhysicsShape shape, HWVec2 depth);
}
