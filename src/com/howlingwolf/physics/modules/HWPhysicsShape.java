package com.howlingwolf.physics.modules;

// Math imports
import com.howlingwolf.util.math.*;

/**
 * Models the physical aspects of an object in the world.
 * 
 * @author Dylan Sawyer
 */
public class HWPhysicsShape
{
	// The shape of the physical bounds of the object
	private HWPolygon m_Shape;
	// The velocity of the object
	private HWVec2 m_Velocity;
	// A link to the object that contains this physics information
	// Mostly used for message passing
	private HWPhysicsBody m_Parent;
	
	// Potentially add more things later on like mass and whatnot...
	
	// Whether the object is dynamic (true, moves around) or static (false,
	// part of the level geometry, etc.)
	private final boolean m_bIsDynamic;
	
	/**
	 * Creates a physical shape that is optionally dynamic with no actual
	 * physical shape. Also provides a link to the object that contains it.
	 * 
	 * @param p_parent		A link to the object that contains this object
	 * @param p_bIsDynamic	True if dynamic, false if static
	 */
	public HWPhysicsShape(HWPhysicsBody p_parent, boolean p_bIsDynamic)
	{
		this(null, p_parent, p_bIsDynamic);
	}
	
	/**
	 * Creates a physical shape that is optionally dynamic.
	 * 
	 * @param p_shape		The physical shape of the object
	 * @param p_parent		A link to the object that contains this object
	 * @param p_bIsDynamic	True if dynamic, false if static
	 */
	public HWPhysicsShape(HWPolygon p_shape, HWPhysicsBody p_parent, boolean p_bIsDynamic)
	{
		m_bIsDynamic = p_bIsDynamic;
		m_Velocity = HWVec2.ZeroVector();
		m_Parent = p_parent;
		if (p_shape != null)
		{
			m_Shape = p_shape.Clone();
		}
		else
		{
			m_Shape = null;
		}
	}
	
	/**
	 * Creates an exact duplicate of the physics shape.
	 * 
	 * @return A copy of this physics shape
	 */
	public HWPhysicsShape Clone()
	{
		HWPhysicsShape temp = new HWPhysicsShape(GetShape(), GetParent(), IsDynamic());
		temp.SetVelocity(GetVelocity());
		
		return temp;
	}
	
	/**
	 * Shows whether the object is a static physical object (doesn't move in
	 * any way), or a dynamic physical object.
	 * 
	 * @return	True if dynamic, false if static
	 */
	public boolean IsDynamic()
	{
		return m_bIsDynamic;
	}
	
	/**
	 * Gets the velocity of the object.
	 * 
	 * @return	A 2D vector that represents the object's velocity
	 */
	public HWVec2 GetVelocity()
	{
		return m_Velocity;
	}
	
	/**
	 * Sets the objects velocity.
	 * 
	 * @param p_NewVelocity	The new velocity of the object
	 */
	public void SetVelocity(HWVec2 p_NewVelocity)
	{
		m_Velocity = p_NewVelocity;
	}
	
	/**
	 * Gets the polygon that forms the physical bounds of the object.
	 * 
	 * @return	The physical bounds of the object
	 */
	public HWPolygon GetShape()
	{
		return m_Shape;
	}
	
	/**
	 * Sets the shape that defines the physical boundaries of the object.
	 * 
	 * @param newShape	The new physical shape of the object
	 */
	public void SetShape(HWPolygon newShape)
	{
		m_Shape = newShape;
	}
	
	/**
	 * Moves the shape based on the given translation vector (a 2D vector that
	 * defines how much the shape needs to be moved in both the x and y
	 * directions). This translation vector will usually be prompted from the
	 * object's velocity.
	 * 
	 * @param moveVec	The distance the object needs to be translated
	 */
	public void Move(HWVec2 moveVec)
	{
		if (m_Shape != null)
		{
			m_Shape.Translate(moveVec);
		}
	}
	
	/**
	 * Gets a handle to the object that contains the physics shape.
	 * 
	 * @return	A link to the object that this physics shape applies to
	 */
	public HWPhysicsBody GetParent()
	{
		return m_Parent;
	}
	
	/**
	 * Sets the parent of the physics shape. This is mainly used to avoid
	 * leaking in various objects constructors.
	 * 
	 * @param p_parent	The new parent of this physics shape
	 */
	public void SetParent(HWPhysicsBody p_parent)
	{
		m_Parent = p_parent;
	}
	
	/**
	 * Updates the object's physical attributes, although this is only relevant
	 * if the object is dynamic.
	 * 
	 * @param p_lElapsedTime	The amount of time that has passed since the
	 *							last update
	 */
	public void Update(long p_lElapsedTime)
	{
		if (IsDynamic())
		{
			// Do movement stuffs and/or other physics related bits
			// (energy calculation, mass changes, etc.)
		}
	}
}
