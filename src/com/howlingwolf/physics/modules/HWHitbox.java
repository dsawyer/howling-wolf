package com.howlingwolf.physics.modules;

// Java imports
import com.howlingwolf.util.math.HWVec2;
import java.awt.Graphics2D;

// Core imports
import com.howlingwolf.util.HWCamera;

/**
 * Models a hit box used for basic collision detection. Being phased out due to
 * the new collision detection algorithm being used.
 * 
 * @author Dylan Sawyer
 */
public class HWHitbox
{
	private HWVec2 m_Position;
	private HWSize m_Size;
	
	/**
	 * Creates a new empty Hitbox.
	 */
	public HWHitbox()
	{
		this(new HWVec2(), new HWSize());
	}
	
	/**
	 * Creates a Hitbox with the given location and size.
	 * 
	 * @param loc The hit box's location
	 * @param size The hit box's size
	 */
	public HWHitbox(HWVec2 loc, HWSize size)
	{
		m_Position = loc.Clone();
		m_Size = size.Clone();
	}
	
	/**
	 * Creates a Hitbox with the given location and size.
	 * 
	 * @param x GetX position of the hit box's location
	 * @param y GetY position of the hit box's location
	 * @param width GetWidth of the hit box
	 * @param height GetHeight of the hit box
	 */
	public HWHitbox(int x, int y, int width, int height)
	{
		this(new HWVec2(x, y), new HWSize(width, height));
	}
	
	/**
	 * Creates a Hitbox with the given location and size.
	 * 
	 * @param x GetX position of the hit box's location
	 * @param y GetY position of the hit box's location
	 * @param width GetWidth of the hit box
	 * @param height GetHeight of the hit box
	 */
	public HWHitbox(float x, float y, int width, int height)
	{
		this(new HWVec2(x, y), new HWSize(width, height));
	}
	
	/**
	 * Makes a copy of this HWHitbox.
	 * 
	 * @return A duplicate of this hit box
	 */
	public HWHitbox Clone()
	{
		return new HWHitbox(m_Position, m_Size);
	}
	
	/**
	 * Gets the hit box's position in the game world.
	 * 
	 * @return The location of the hit box
	 */
	public HWVec2 GetPosition()
	{
		return m_Position;
	}
	
	/**
	 * Gets the x position of this hitbox.
	 * 
	 * @return The x position
	 */
	public float GetX()
	{
		return m_Position.GetX();
	}
	
	/**
	 * Sets the x position of this hitbox.
	 * 
	 * @param p_X The new x position
	 */
	public void SetX(float p_X)
	{
		m_Position.SetX(p_X);
	}
	
	/**
	 * Gets the y position of this hitbox.
	 * 
	 * @return The y position
	 */
	public float GetY()
	{
		return m_Position.GetY();
	}
	
	/**
	 * Sets the y position of this hitbox.
	 * 
	 * @param p_Y The new y position
	 */
	public void SetY(float p_Y)
	{
		m_Position.SetY(p_Y);
	}
	
	/**
	 * Gets the middle point of the hit box.
	 * 
	 * @return The central point of the hit box
	 */
	public HWVec2 GetCentre()
	{
		return new HWVec2((m_Position.GetX() + (m_Size.GetWidth()/2)),
							(m_Position.GetY() + (m_Size.GetHeight()/2)));
	}
	
	/**
	 * Sets the hit box's position in the game world.
	 * 
	 * @param loc The new location of the hit box
	 */
	public void SetPosition(HWVec2 loc)
	{
		m_Position = loc.Clone();
	}
	
	/**
	 * Gets the size of the hit box.
	 * 
	 * @return The hit box's bounding width and height
	 */
	public HWSize GetSize()
	{
		return m_Size;
	}
	
	/**
	 * Gets the width of this hitbox.
	 * 
	 * @return The width, in pixels
	 */
	public int GetWidth()
	{
		return m_Size.GetWidth();
	}
	
	/**
	 * Sets the width of this hitbox.
	 * 
	 * @param p_iWidth The new width, in pixels
	 */
	public void SetWidth(int p_iWidth)
	{
		m_Size.SetWidth(p_iWidth);
	}
	
	/**
	 * Gets the height of this hitbox.
	 * 
	 * @return GetHeight, in pixels
	 */
	public int GetHeight()
	{
		return m_Size.GetHeight();
	}
	
	/**
	 * Sets the height of this hitbox.
	 * 
	 * @param p_iHeight The new height, in pixels
	 */
	public void SetHeight(int p_iHeight)
	{
		m_Size.SetHeight(p_iHeight);
	}
	
	/**
	 * Sets the size of the hit box.
	 * 
	 * @param size The new bounding width and height of the hit box
	 */
	public void SetSize(HWSize size)
	{
		m_Size = size.Clone();
	}
	
	/**
	 * Sets the size of the hit box.
	 * 
	 * @param size The new bounding width and height of the hit box
	 */
	public void SetSize(HWVec2 size)
	{
		m_Size.SetWidth(size.GetXInt());
		m_Size.SetHeight(size.GetYInt());
	}
	
	/**
	 * Gets the top side of the hit box.
	 * 
	 * @return The top most value of the hit box
	 */
	public float Top()
	{
		return m_Position.GetY();
	}
	
	/**
	 * Gets the right side of the hit box.
	 * 
	 * @return The right most value of the hit box
	 */
	public float Right()
	{
		return m_Position.GetX() + m_Size.GetWidth();
	}
	
	/**
	 * Gets the left side of the hit box.
	 * 
	 * @return The left most value of the hit box
	 */
	public float Left()
	{
		return m_Position.GetX();
	}
	
	/**
	 * Gets the bottom side of the hit box.
	 * 
	 * @return The bottom most value of the hit box
	 */
	public float Bottom()
	{
		return m_Position.GetY() + m_Size.GetHeight();
	}
	
	/**
	 * Checks to see if a point is contained within the rectangle defined by
	 * the hitbox.
	 * 
	 * @param point A point in the world
	 * @return True if the point is within the hitbox, false otherwise
	 */
	public boolean ContainsPoint(HWVec2 point)
	{
		return ContainsPoint(point.GetX(), point.GetY());
	}
	
	/**
	 * Checks to see if a point is contained within the rectangle defined by
	 * the hitbox.
	 * 
	 * @param x The x coordinate of the point in the world
	 * @param y The y coordinate of the point in the world
	 * @return True if the point is within the hitbox, false otherwise
	 */
	public boolean ContainsPoint(float x, float y)
	{
		if ((x >= Left()) && (x <= Right()) &&
			(y >= Top()) && (y <= Bottom()))
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Draws the hit box in the game world.
	 * 
	 * @param graphics The graphics context upon which it is drawn
	 */
	public void Draw(Graphics2D graphics)
	{
		HWVec2 drawPlace = HWCamera.Handle().WorldToScreen(m_Position);
		
		graphics.drawRect(drawPlace.GetXInt(), drawPlace.GetYInt(), m_Size.GetWidth(), m_Size.GetHeight());
	}
	
	@Override
	public String toString()
	{
		return ("Origin: " + m_Position + "\tSize: " + m_Size);
	}
}
