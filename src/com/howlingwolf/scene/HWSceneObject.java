package com.howlingwolf.scene;

// Java imports
import java.awt.Graphics2D;

/**
 * A basic interface that merely provides an Update method and forces a
 * Draw method on any object that would be included in a Scene.
 * 
 * @author Dylan Sawyer
 */
public interface HWSceneObject
{
	/**
	 * Gets the identifier for this scene object.
	 * 
	 * @return The unique identifier for this object
	 */
	public String GetIdentifier();
	/**
	 * Tells the object to update itself based on the amount of time that has
	 * passed since the last update was called.
	 * 
	 * @param p_lElapsedTime	The amount of time since the last update
	 */
	public void Update(long p_lElapsedTime);
	
	/**
	 * Tells the object to draw itself on the provided graphics context.
	 * 
	 * @param graphics The graphics context
	 */
	public void Draw(Graphics2D graphics);
}
