package com.howlingwolf.scene;

// Java imports
import java.awt.Graphics2D;
import java.util.ArrayList;

// Physics imports
import com.howlingwolf.util.math.HWVec2;

/**
 * A class representing a scene in a game, which can contain any number of
 * scene objects and draws them based on the order in which they were added.
 * This class is also a scene object itself.
 * 
 * @author Dylan Sawyer
 */
public class HWScene implements HWSceneObject
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************
	
	// The list of scene objects in this particular scene
	private final ArrayList<HWSceneObject> m_SceneObjects;
	private final String m_sID;
	
	// The scene's offset based on the world
	private HWVec2 m_Position;
	private HWVec2 m_Size;
	
	//**************************************************************************
	// Methods
	//**************************************************************************
	
	/**
	 * Creates a new HWScene, initialising the list of scene objects.
	 * 
	 * @param p_sID	The unique identifier for this scene
	 */
	public HWScene(String p_sID)
	{
		m_SceneObjects = new ArrayList<>();
		m_sID = p_sID;
		m_Position = new HWVec2();
		m_Size = new HWVec2();
	}
	
	/**
	 * Creates a new scene with the given list of scene objects.
	 * 
	 * @param p_sID		The unique identifier for this scene
	 * @param p_list	ArrayList of scene objects
	 */
	public HWScene(String p_sID, ArrayList<HWSceneObject> p_list)
	{
		m_SceneObjects = p_list;
		m_sID = p_sID;
		m_Position = new HWVec2();
		m_Size = new HWVec2();
	}
	
	/**
	 * Creates a duplicate of this scene and returns it.
	 * 
	 * @return An exact copy of this scene
	 */
	public HWScene Clone()
	{
		HWScene scene = new HWScene(m_sID);
		
		for (HWSceneObject node : m_SceneObjects)
		{
			scene.AddSceneObject(node);
		}
		
		return scene;
	}
	
	/**
	 * Adds a HWSceneObject to this scene. Checks to make sure that no other
	 * object with the same identifier has been added.
	 * 
	 * @param p_object The object to be added to the scene
	 */
	public void AddSceneObject(HWSceneObject p_object)
	{
		for (HWSceneObject node : m_SceneObjects)
		{
			// Check to see if there is a matching identifier already present
			// in the list
			if ((node != null) && node.GetIdentifier().equals(p_object.GetIdentifier()))
			{
				// If there is a match, jump out of the method
				return;
			}
		}
		
		// If no matches were found, add the object to the scene
		m_SceneObjects.add(p_object);
	}
	
	/**
	 * Removes a HWSceneObject from this scene.
	 * 
	 * @param p_object The object to be removed from the scene
	 */
	public void RemoveSceneObject(HWSceneObject p_object)
	{
		m_SceneObjects.remove(p_object);
	}
	
	/**
	 * Gets the scene object with the given identifier from the list contained
	 * within this scene.
	 * 
	 * @param p_sID	The identifier for the desired object
	 * @return		The object with the given identifier, null if none are found
	 */
	public HWSceneObject FindSceneObject(String p_sID)
	{
		for (HWSceneObject node : m_SceneObjects)
		{
			// Check to see if there is a matching identifier already present
			// in the list
			if ((node != null) && node.GetIdentifier().equals(p_sID))
			{
				// If there is a match, jump out of the method
				return node;
			}
		}
		
		return null;
	}
	
	/**
	 * Sets the position of the scene.
	 * 
	 * @param vec	The new position of the scene
	 */
	public void SetPosition(HWVec2 vec)
	{
		m_Position = vec;
	}
	
	/**
	 * Sets the position of the scene.
	 * 
	 * @param x	The new x position of the scene
	 * @param y	The new y position of the scene
	 */
	public void SetPosition(float x, float y)
	{
		m_Position = new HWVec2(x, y);
	}
	
	/**
	 * Moves the scene by the given amount.
	 * 
	 * @param x	The x distance to move the scene
	 * @param y	The y distance to move the scene
	 */
	public void MovePosition(float x, float y)
	{
		m_Position.Add(x, y);
	}
	
	/**
	 * Sets the x position of the scene.
	 * 
	 * @param x The new x position of the scene
	 */
	public void SetX(float x)
	{
		m_Position.SetX(x);
	}
	
	/**
	 * Sets the y position of the scene.
	 * 
	 * @param y	The new y position of the scene
	 */
	public void SetY(float y)
	{
		m_Position.SetY(y);
	}
	
	/**
	 * Gets the position of the scene.
	 * 
	 * @return	The position at which the scene is located
	 */
	public HWVec2 GetPosition()
	{
		return m_Position;
	}
	
	/**
	 * Gets the x position of the scene.
	 * 
	 * @return	The x position at which the scene is located
	 */
	public int GetX()
	{
		return m_Position.GetXInt();
	}
	
	/**
	 * Gets the y position of the scene.
	 * 
	 * @return	The y position at which the scene is located
	 */
	public int GetY()
	{
		return m_Position.GetYInt();
	}
	
	/**
	 * Sets the size of the scene.
	 * 
	 * @param size	The new size of the scene
	 */
	public void SetSize(HWVec2 size)
	{
		m_Size = size;
	}
	
	/**
	 * Sets the width of the scene.
	 * 
	 * @param p_iWidth	The new width of the scene
	 */
	public void SetWidth(int p_iWidth)
	{
		m_Size.SetX(p_iWidth);
	}
	
	/**
	 * Gets the width of the scene.
	 * 
	 * @return	The width of the scene
	 */
	public int GetWidth()
	{
		return m_Size.GetXInt();
	}
	
	/**
	 * Sets the height of the scene.
	 * 
	 * @param p_iHeight	The new height of the scene
	 */
	public void SetHeight(int p_iHeight)
	{
		m_Size.SetY(p_iHeight);
	}
	
	/**
	 * Gets the height of the scene.
	 * 
	 * @return	The height of the scene
	 */
	public int GetHeight()
	{
		return m_Size.GetYInt();
	}
	
	//**************************************************************************
	// HWSceneObject Methods
	//**************************************************************************
	
	@Override
	public String GetIdentifier()
	{
		return m_sID;
	}
	
	@Override
	public void Update(long p_lElapsedTime)
	{
		// Update all the scene objects
		for (HWSceneObject node : m_SceneObjects)
		{
			if (node != null)
			{
				node.Update(p_lElapsedTime);
			}
		}
	}
	
	@Override
	public void Draw(Graphics2D graphics)
	{
		// Draw all the scene objects in  the order they were added
		for (HWSceneObject node : m_SceneObjects)
		{
			if (node != null)
			{
				node.Draw(graphics);
			}
		}
	}
}