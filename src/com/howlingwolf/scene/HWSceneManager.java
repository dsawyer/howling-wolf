package com.howlingwolf.scene;

// Java imports
import java.awt.Graphics2D;

/**
 * Manages which scene is presented to the player.
 * 
 * *To-DO* Add functionality for transitions between scenes.
 * 
 * @author Dylan Sawyer
 */
public class HWSceneManager
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************
	
	private static HWSceneManager instance;
	
	private HWScene m_CurrentScene;
	
	//**************************************************************************
	// Singleton Methods
	//**************************************************************************
	
	// Constructor - Private
	// Only called when first initialising the manager
	private HWSceneManager()
	{
		// If ever something needs to be instantiated/loaded when this manager
		// is first created, do it here.
	}
	
	/**
	 * Gets access to the HWSceneManager. Creates a new one if it has not been
	 * accessed previously.
	 * 
	 * @return	The HWSceneManager
	 */
	public static synchronized HWSceneManager SharedManager()
	{
		if (instance == null)
		{
			instance = new HWSceneManager();
		}
		
		return instance;
	}
	
	//**************************************************************************
	// Methods
	//**************************************************************************
	
	/**
	 * Loads a scene based on it's identifier.
	 * This will be used with XML loading of a scene.
	 * 
	 * @param p_sID	The new scene's identifier
	 */
		
	public void LoadScene(String p_sID)
	{
		m_CurrentScene = new HWScene(p_sID);
	}
	
	/**
	 * Loads a new scene.
	 * 
	 * @param p_scene The new scene to be used
	 */
	public void LoadScene(HWScene p_scene)
	{
		System.out.println("Loading scene: " + p_scene.GetIdentifier());
		
		m_CurrentScene = p_scene;
	}
	
	/**
	 * Gets the current scene.
	 * 
	 * @return The current HWScene being managed by the scene manager
	 */
	public HWScene GetCurrentScene()
	{
		return m_CurrentScene;
	}
	
	/**
	 * Updates the current scene.
	 * 
	 * @param p_lElapsedTime The amount of time since the last update
	 */
	public void Update(long p_lElapsedTime)
	{
		m_CurrentScene.Update(p_lElapsedTime);
	}
	
	/**
	 * Draws the scene.
	 * 
	 * @param graphics The graphics context drawn upon
	 */
	public void Draw(Graphics2D graphics)
	{
		m_CurrentScene.Draw(graphics);
	}
}
