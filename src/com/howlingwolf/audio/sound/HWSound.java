package com.howlingwolf.audio.sound;

// Java imports
import com.howlingwolf.util.HWLoopingByteInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
 
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * The HWSound class is a container for sound samples. The samples are format-
 * agnostic and are stored as a byte array.
 * 
 * @author Dylan Sawyer
 */
public class HWSound
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private byte[] m_samples;
	private String m_sIdentifier;
	private AudioFormat m_AudioFormat;
	private boolean m_bLoop;
	private HWSoundFilter m_SoundFilter;
	
	private static final String SFX_FILEPATH = "sounds/sfx/";
	private static final String SFX_FILETYPE = ".wav";

	//**************************************************************************
	// Methods
	//**************************************************************************
	
	/**
	 * Creates a new Sound object with the given file name and audio format.
	 * 
	 * @param p_sID    SetIdentifier for the Sound object, as well as the file name
	 */
	public HWSound(String p_sID)
	{
		this(p_sID, null, false);
	}
	
	/**
	 * Creates a new Sound object with the given file name and audio format.
	 * Also allows for optional looping and filtering.
	 * 
	 * @param p_sID    SetIdentifier for the Sound object, as well as the file name
	 * @param p_filter The HWSoundFilter used to filter the sound
	 * @param p_bLoop  Whether the sound loops or not
	 */
	public HWSound(String p_sID, HWSoundFilter p_filter, boolean p_bLoop)
	{
		m_sIdentifier = p_sID;
		m_samples = LoadSamples(LoadAudioInputStream(m_sIdentifier));
		m_SoundFilter = p_filter;
		m_bLoop = p_bLoop;
	}
	
	// Helper method that gets the samples from an AudioInputStream
	private byte[] LoadSamples(AudioInputStream p_AudioStream)
	{
		if (p_AudioStream == null)
		{
			return null;
		}

		// Get the number of bytes to read
		int length = (int)(p_AudioStream.getFrameLength() * p_AudioStream.getFormat().getFrameSize());
		
		// Read the entire stream
		byte[] samples = new byte[length];
		DataInputStream is = new DataInputStream(p_AudioStream);
		try
		{
			is.readFully(samples);
		}
		catch (IOException ex)
		{
		}

		// Return the samples
		return samples;
	}
	
	// Helper method that loads an AudioInputStream from a file
	private AudioInputStream LoadAudioInputStream(String p_sFilename)
	{
		try
		{
			// Get the full file path to the audio file
			String filepath = SFX_FILEPATH + p_sFilename + SFX_FILETYPE;
			
			// Open the source file
			AudioInputStream source = AudioSystem.getAudioInputStream(getClass().getClassLoader().getResource(filepath));
			// Get the file's GetAudioFormat
			m_AudioFormat = source.getFormat();
			
			// Convert to playback format
			return AudioSystem.getAudioInputStream(m_AudioFormat, source);
		}
		catch (UnsupportedAudioFileException | IOException | IllegalArgumentException ex)
		{
			System.out.println("Could not load audio file...");
		}

		return null;
	}

    /**
	 * Returns this HWSound's samples as a byte array.
	 * 
	 * @return The samples of the sound
	 */
	public byte[] GetSamples()
    {
    	return m_samples;
    }
	
	/**
	 * Sets the HWSound object's identifier.
	 * 
	 * @param p_sID The filename/identifier for the HWSound object
	 */
	public void SetIdentifier(String p_sID)
	{
		m_sIdentifier = p_sID;
	}
	
	/**
	 * Gets the HWSound object's identifier.
	 * 
	 * @return The filename/identifier for the HWSound object
	 */
	public String GetIdentifier()
	{
		return m_sIdentifier;
	}
	
	/**
	 * The HWSound object's GetAudioFormat.
	 * 
	 * @return The GetAudioFormat in which the HWSound object's samples are encoded
	 */
	public AudioFormat GetAudioFormat()
	{
		return m_AudioFormat;
	}
	
	/**
	 * Gets an GetInputStream from the HWSound object's samples.
	 * Modifies it based on the HWSound's filter and whether it loops or not.
 This GetInputStream is only used when the HWSound needs to be played.
	 * 
	 * @return The GetInputStream to be used when the HWSound is being played
	 */
	public InputStream GetInputStream()
	{
		InputStream is;
		
		if (m_bLoop)
		{
			is = new HWLoopingByteInputStream(m_samples);
		}
		else
		{
			is = new ByteArrayInputStream(m_samples);
		}

		if (m_SoundFilter != null)
		{
			return new HWFilteredSoundStream(is, m_SoundFilter);
		}

		return is;
	}
}