package com.howlingwolf.audio.sound;

/**
 * Abstract class that is the basis for filtering sounds.
 * <p>
 Since HWSoundFilters may use internal buffering of samples, a new HWSoundFilter
 object should be created for every sound played. However, HWSoundFilters can
 be reused after they are finished by calling the Reset() method.
 <p>
 * Assumes all samples are 16-bit, signed, little-endian format.
 * 
 * @author Dylan Sawyer
 */
public abstract class HWSoundFilter
{
	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Resets this HWSoundFilter. Does nothing by default.
	 */
	public void Reset()
    {
    	// Do nothing
    }

    /**
	 * Gets the remaining size, in bytes, that this filter plays after the sound
	 * is finished. An example would be an echo that plays longer than its
	 * original sound. This method returns 0 by default.
	 * 
	 * @return The remaining size of the filter
	 */
	public int RemainingSize()
    {
    	return 0;
    }

    /**
	 * Filters an array of samples. Samples should be in 16-bit, signed, little-
	 * endian format.
	 * 
	 * @param p_samples The samples to be filtered
	 */
	public void Filter(byte[] p_samples)
    {
    	Filter(p_samples, 0, p_samples.length);
    }

    /**
	 * Filters an array of samples. Samples should be in 16-bit, signed, little-
	 * endian format. this method should be implemented by subclasses. Note that
	 * the offset and length are number of bytes, not samples.
	 * 
	 * @param p_samples The samples to be filtered
	 * @param p_iOffset The offset at which the filter begins
	 * @param p_iLength The point at which the filter ends
	 */
	public abstract void Filter(byte[] p_samples, int p_iOffset, int p_iLength);

    /**
	 * Convenience method for getting a 16-bit sample from a byte array. Samples
	 * should be in 16-bit, signed, little-endian format.
	 * 
	 * @param p_buffer Array containing the information to sample
	 * @param p_iPosition The position to sample
	 * @return The sample desired
	 */
	public static short GetSample(byte[] p_buffer, int p_iPosition)
    {
    	return (short)(((p_buffer[p_iPosition+1] & 0xff) << 8) | (p_buffer[p_iPosition] & 0xff));
    }

    /**
	 * Convenience method for setting a 16-bit sample from a byte array. Samples
	 * should be in 16-bit, signed, little-endian format.
	 * 
	 * @param p_buffer Array containing the information to sample
	 * @param p_iPosition The position to sample
	 * @param p_sample The sample to set
	 */
	public static void SetSample(byte[] p_buffer, int p_iPosition, short p_sample)
    {
    	p_buffer[p_iPosition] = (byte)(p_sample & 0xff);
    	p_buffer[p_iPosition+1] = (byte)((p_sample >> 8) & 0xff);
    }
}