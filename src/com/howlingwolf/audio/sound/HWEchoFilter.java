package com.howlingwolf.audio.sound;

/**
 * This is a HWSoundFilter that applies an emulated echo effect to a sound.
 * 
 * @author Dylan Sawyer
 */
public class HWEchoFilter extends HWSoundFilter
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private short[] m_DelayBuffer;
	private int m_iDelayBufferPosition;
	private float m_fDecay;

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates an EchoFilter with the specified number of delay smaples and the
	 * specified decay rate.
	 * <p>
	 * The number of delay samples specifies how long before the echo is
	 * initially heard. For a 1 second delay with mono, 44100Hz sound, use
	 * 44100 delay samples.
	 * <p>
	 * The decay value is how much the echo has decayed from the source. A
	 * decay value of 0.5 means the echo heard is half as loud as the source.
	 * 
	 * @param p_iNumDelaySamples Number of delay samples to be used
	 * @param p_fDecay The rate at which the echo should decay
	 */
	public HWEchoFilter(int p_iNumDelaySamples, float p_fDecay)
    {
    	m_DelayBuffer = new short[p_iNumDelaySamples];
    	m_fDecay = p_fDecay;
    }

	/**
	 * Gets the remaining size, in bytes, of samples that this filter can echo
	 * after the sound is done playing. Ensures that the sound will have
	 * decayed to below 1% of maximum volume (amplitude).
	 * 
	 * @return The remaining size of the samples that the filter can echo
	 */
	@Override
    public int RemainingSize()
    {
    	float finalDecay = 0.01f;
    	// Derived from Math.pow(m_fDecay, x) <= finalDecay
    	int numRemainingBuffers = (int)Math.ceil(Math.log(finalDecay) / Math.log(m_fDecay));
    	int bufferSize = m_DelayBuffer.length * 2;

    	return (bufferSize * numRemainingBuffers);
    }

	/**
	 * Clears this HWEchoFilter's internal delay buffer
	 */
	@Override
    public void Reset()
    {
    	for (int i = 0; i < m_DelayBuffer.length; i+=1)
    	{
    		m_DelayBuffer[i] = 0;
    	}

    	m_iDelayBufferPosition = 0;
    }

	/**
	 * Filters the sound samples to add an echo. The samples played are added to
	 * the sound in the delay buffer multiplied by the decay rate. The result is
	 * then stored in the delay buffer, so multiple echoes are heard.
	 * 
	 * @param p_samples The samples to be filtered
	 * @param p_iOffset The offset at which an echo begins
	 * @param p_iLength The point at which the echo ends
	 */
	@Override
	public void Filter(byte[] p_samples, int p_iOffset, int p_iLength)
	{
		for (int i = p_iOffset; i < p_iOffset + p_iLength; i+=2)
		{
			// Update the sample
			short oldSample = GetSample(p_samples, i);
			short newSample = (short)(oldSample + m_fDecay * m_DelayBuffer[m_iDelayBufferPosition]);
			SetSample(p_samples, i, newSample);

			// Update the delay buffer
			m_DelayBuffer[m_iDelayBufferPosition] = newSample;
			m_iDelayBufferPosition += 1;
			if (m_iDelayBufferPosition == m_DelayBuffer.length)
			{
				m_iDelayBufferPosition = 0;
			}
		}
	}
}