package com.howlingwolf.audio.sound;

import java.io.FilterInputStream;
import java.io.InputStream;
import java.io.IOException;

/**
 * This is a FilterInputStream that applies a HWSoundFilter to the underlying
 input stream.
 * 
 * @author Dylan Sawyer
 */
public class HWFilteredSoundStream extends FilterInputStream
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private static final int REMAINING_SIZE_UNKNOWN = -1;

	private HWSoundFilter m_SoundFilter;
	private int m_iRemainingSize;

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates a new FilteredSoundStream object with the specified InputStream
 and HWSoundFilter
	 * 
	 * @param p_is The input stream to be filtered
	 * @param p_filter The filter to apply to the input stream
	 */
	public HWFilteredSoundStream(InputStream p_is, HWSoundFilter p_filter)
    {
		super(p_is);
		m_SoundFilter = p_filter;
		m_iRemainingSize = REMAINING_SIZE_UNKNOWN;
    }

    // Overrides the FilterInputStream method to apply this filter whenever
    // bytes are read
	@Override
    public int read(byte[] p_samples, int p_iOffset, int p_iLength) throws IOException
    {
		// Read and filter the sound samples in the stream
		int bytesRead = super.read(p_samples, p_iOffset, p_iLength);

		if (bytesRead > 0)
		{
			m_SoundFilter.Filter(p_samples, p_iOffset, bytesRead);
			return bytesRead;
		}

		// If there are no remaining bytes in the sound stream, check if the filter
		// has any remaining bytes ("echoes").
		if (m_iRemainingSize == REMAINING_SIZE_UNKNOWN)
		{
			m_iRemainingSize = m_SoundFilter.RemainingSize();
			// Round down to the nearest multiple of 4 (integer division drops remainder)
			// (typical frame size)
			m_iRemainingSize = (m_iRemainingSize / 4) * 4;
		}

		if (m_iRemainingSize > 0)
		{
			p_iLength = Math.min(p_iLength, m_iRemainingSize);

			// Clear the buffer
			for (int i = p_iOffset; i < p_iOffset + p_iLength; i+=1)
			{
				p_samples[i] = 0;
			}

			// Filter the remaining bytes
			m_SoundFilter.Filter(p_samples, p_iOffset, p_iLength);
			m_iRemainingSize -= p_iLength;

			// Return
			return p_iLength;
		}
		else
		{
			// End of stream;
			return -1;
		}
    }
}