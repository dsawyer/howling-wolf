package com.howlingwolf.audio.sound;

import java.awt.Point;

/**
 * This is a HWSoundFilter that changes the volume of a sound based on its
 distance from the listener.
 * 
 * @author Dylan Sawyer
 */
public class HWLinearDistanceFilter extends HWSoundFilter
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	// Number of samples to shift when changing the volume (creates a smoother transition)
	private static final int NUM_SHIFTING_SAMPLES = 500;

	private Point m_source;
	private Point m_listener;
	private int m_iMaxDistance;
	private float m_fLastVolume;

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates a new LinearDistanceFilter object with the specified source and
	 * listener points. The points can be changed while this filter is running.
	 * 
	 * @param p_source The source's location
	 * @param p_listener The listener's location
	 * @param p_iMaxDistance The maximum distance between the two points at
	 *						 which the sound can still be heard
	 */
	public HWLinearDistanceFilter(Point p_source, Point p_listener, int p_iMaxDistance)
    {
    	m_source = p_source;
    	m_listener = p_listener;
    	m_iMaxDistance = p_iMaxDistance;
    	m_fLastVolume = 0.0f;
    }

	/**
	 * Filters the sound so that it gets quieter with distance.
	 * 
	 * @param p_samples The samples to filter
	 * @param p_iOffset The point at which the filter kicks in
	 * @param p_iLength The point where the filter effect ends
	 */
	@Override
    public void Filter(byte[] p_samples, int p_iOffset, int p_iLength)
    {
		if ((m_source == null) || (m_listener == null))
		{
			// Do Nothing, no calculations can be made
			return;
		}

		// Calculate the listener's distance from the sound source
		float dx = (m_source.x - m_listener.x);
		float dy = (m_source.y - m_listener.y);
		float distance = (float)Math.sqrt((dx * dx) + (dy * dy));

		// Set volume from 0 (no sound) to 1 (full volume)
		float newVolume = (m_iMaxDistance - distance) / m_iMaxDistance;
		if (newVolume < 0)
		{
			newVolume = 0;
		}

		// Set the volume of the sample
		int shift = 0;
		for (int i = p_iOffset; i < p_iOffset + p_iLength; i+=2)
		{
			float volume = newVolume;

			// Shift from the last volume to the new volume
			if (shift < NUM_SHIFTING_SAMPLES)
			{
				volume = m_fLastVolume + (newVolume - m_fLastVolume) + (shift / NUM_SHIFTING_SAMPLES);
				shift += 1;
			}

			// Change the volume of the sample
			short oldSample = GetSample(p_samples, i);
			short newSample = (short)(oldSample * volume);
			SetSample(p_samples, i, newSample);
		}

		m_fLastVolume = newVolume;
    }
}