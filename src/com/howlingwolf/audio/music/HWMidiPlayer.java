package com.howlingwolf.audio.music;

// Java imports
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.*;

/**
 * The HWMidiPlayer class controls playback of midi files.
 * 
 * @author Dylan Sawyer
 */
public class HWMidiPlayer implements MetaEventListener
{
	//**************************************************************************
	// MIDI Meta Message Types
	//**************************************************************************
	
	/**
	 * 2 Bytes worth of information giving the number of a sequence.
	 */
	public static final int META_SEQUENCE_NUMBER = 0;
	/**
	 * Variable length containing some text.
	 */
	public static final int META_TEXT = 1;
	/**
	 * Variable length containing a copyright notice.
	 */
	public static final int META_COPYRIGHT = 2;
	/**
	 * Variable length giving a track name.
	 */
	public static final int META_TRACK_NAME = 3;
	/**
	 * Variable length giving the name of an instrument in the current track.
	 */
	public static final int META_INSTRUMENT_NAME = 4;
	/**
	 * Variable length lyrics, usually a syllable per quarter note.
	 */
	public static final int META_LYRICS = 5;
	/**
	 * Variable length giving the text of a marker.
	 */
	public static final int META_MARKER = 6;
	/**
	 * Variable length, the text of a cue. Usually prompts for some action
	 * from the user.
	 */
	public static final int META_CUE_POINT = 7;
	/**
	 * 1 byte, a channel number.
	 * Following meta events will apply to this channel.
	 */
	public static final int META_CHANNEL_PREFIX = 32;
	/**
	 * An empty message that is at the end of a track.
	 * No other messages will follow.
	 */
	public static final int META_END_OF_TRACK = 47;
	/**
	 * 3 bytes, the number of microseconds per beat.
	 */
	public static final int META_SET_TEMPO = 81;
	/**
	 * 5 bytes, SMPTE time to denote playback offset from the beginning.
	 */
	public static final int META_SMPTE_OFFSET = 84;
	/**
	 * 4 bytes, time signature, metronome clicks, and size of a beat in
	 * 32nd notes.
	 */
	public static final int META_TIME_SIGNATURE = 88;
	/**
	 * 2 bytes, a key signature.
	 */
	public static final int META_KEY_SIGNATURE = 89;
	/**
	 * Variable length, contains something specific to the MIDI device manuf.
	 */
	public static final int META_SEQUENCER_SPECIFIC = 127;
	
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private Sequencer m_Sequencer;
	private boolean m_bPaused;

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates a new MidiPlayer object.
	 * Always follow this with the AddMetaEventListener() method.
	 */
	public HWMidiPlayer()
    {
    	try
    	{
    		m_Sequencer = MidiSystem.getSequencer();
    		m_Sequencer.open();
			
			Synthesizer synth = MidiSystem.getSynthesizer();
			synth.open();
			
			Soundbank soundbank;
			
			//Instrument[] instruments;
			
			try
			{
				// Basic, generic soundbank
				soundbank = MidiSystem.getSoundbank(getClass().getResource("/sounds/soundbanks/SYNTHGMS.SF2"));
				
				if (synth.isSoundbankSupported(soundbank))
				{
					Soundbank defSound = synth.getDefaultSoundbank();
					
					/*try
					{
						instruments = synth.getLoadedInstruments();
						
						for (Instrument instr : instruments)
						{
							System.out.println(instr);
						}
					}
					catch (Exception e)
					{
						System.out.println("No default loaded instruments");
					}*/
					
					synth.unloadAllInstruments(defSound);
					synth.loadAllInstruments(soundbank);
					
					/*instruments = null;
					
					try
					{
						instruments = synth.getLoadedInstruments();
						
						System.out.println(instruments.length + " Instruments loaded.");
						
						for (Instrument instr : instruments)
						{
							System.out.println(instr);
						}
					}
					catch (Exception e)
					{
						System.out.println("Did not load any instruments...");
					}*/
				}
				else
				{
					System.out.println("Not a supported Soundbank :/");
				}
			}
			catch (	InvalidMidiDataException | IOException ex)
			{
				Logger.getLogger(HWMidiPlayer.class.getName()).log(Level.SEVERE, null, ex);
			}
    	}
    	catch (MidiUnavailableException ex)
    	{
    		m_Sequencer = null;
    	}
    }
	
	/**
	 * Do this here to keep from leaking. Do immediately after instantiation.
	 */
	public void AddMetaEventListener()
	{
		m_Sequencer.addMetaEventListener(this);
	}

    /**
	 * Plays a sequence, optionally looping. This method returns immediately.
	 * The sequence is not played if it is invalid.
	 * 
	 * @param p_midi The {@link HWMidiFile} to be played
	 */
	public void Play(HWMidiFile p_midi)
    {
    	if ((m_Sequencer != null) && (p_midi != null))
    	{
    		try
    		{
    			m_Sequencer.setSequence(p_midi.GetSequence());
    			// Modify the TempoFactor if the tempo is specified
				if (p_midi.GetTempoInBPM() > HWMidiFile.UNSPECIFIED_BPM)
				{
					m_Sequencer.setTempoFactor(p_midi.GetTempoFactor(m_Sequencer.getTempoInBPM()));
				}
				// Set the loop start and end points, as well as how many times it should loop
				m_Sequencer.setLoopStartPoint(p_midi.GetLoopStart());
				m_Sequencer.setLoopEndPoint(p_midi.GetLoopEnd());
				m_Sequencer.setLoopCount(p_midi.GetLoopCount());
				
				// Start playing the sequence
    			m_Sequencer.start();
    		}
    		catch (InvalidMidiDataException ex)
    		{
    		}
    	}
    }

    // This method is called by the sound system when a meta event occurs.
	@Override
    public void meta(MetaMessage event)
    {
    	// Do something based on a meta event
    }

    /**
	 * Stops the sequencer and resets its position to 0.
	 */
	public void Stop()
    {
    	if ((m_Sequencer != null) && (m_Sequencer.isOpen()))
    	{
    		m_Sequencer.stop();
    		m_Sequencer.setMicrosecondPosition(0);
    	}
    }

    /**
	 * Closes the sequencer.
	 */
	public void Close()
    {
    	if ((m_Sequencer != null) && (m_Sequencer.isOpen()))
    	{
    		m_Sequencer.close();
    	}
    }

    /**
	 * Gets the sequencer.
	 * 
	 * @return The MidiPLayer's sequencer
	 */
	public Sequencer GetSequencer()
    {
    	return m_Sequencer;
    }

    /**
	 * Sets the paused state. Music may not immediately pause.
	 * 
	 * @param p_bPause Pauses music if true
	 */
	public void SetPaused(boolean p_bPause)
    {
    	if ((m_bPaused != p_bPause) && (m_Sequencer != null))
    	{
    		m_bPaused = p_bPause;
    		if (m_bPaused)
    		{
    			m_Sequencer.stop();
    		}
    		else
    		{
    			m_Sequencer.start();
    		}
    	}
    }

    /**
	 * Returns the paused state.
	 * 
	 * @return Music is paused if true
	 */
	public boolean IsPaused()
    {
    	return m_bPaused;
    }
}