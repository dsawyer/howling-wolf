package com.howlingwolf.audio.music;

import java.io.IOException;
import javax.sound.midi.*;

/**
 * The HWMidiFile class is a container for a midi file. It knows everything
 * necessary for complete midi playback.
 *
 * @author Dylan Sawyer
 */
public class HWMidiFile {
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	private float m_fTempoInBPM;
	private long m_lLoopStartPoint;
	private long m_lLoopEndPoint;
	private int m_iLoopCount;
	private Sequence m_Sequence;
	private String m_sIdentifier;
	//**************************************************************************
	// Global Variables
	//**************************************************************************
	/**
	 * Defines the default beginning of a midi track.
	 */
	public static final long BEGINNING_OF_TRACK = 0;
	/**
	 * Defines the default end of a midi track (full length).
	 */
	public static final long END_OF_TRACK = -1;
	/**
	 * Defines a flag to use the default tempo in the sequencer.
	 */
	public static final float UNSPECIFIED_BPM = -1;
	
	// The amount of microseconds in a second
	private static final long MICROSECONDS_PER_SECOND = 1000000;
	
	// Constants to use when loading a midi file
	private static final String BGM_FILEPATH = "sounds/music/";
	private static final String BGM_FILETYPE = ".mid";

	//**************************************************************************
	// Methods
	//**************************************************************************
	/**
	 * Creates a new MidiFile object with the given filename, and fills in the
	 * rest of the information if the sequence is succesfully created.
	 *
	 * @param p_sFilename The name of the midi file
	 * @param p_fBPM Beats Per Minute
	 * @param p_fLoopStart The time in seconds where the looping starts
	 * @param p_fLoopEnd  The time in seconds where the looping ends
	 * @param p_iLoops The number of loops the midi is to play
	 */
	public HWMidiFile(String p_sFilename, float p_fBPM, float p_fLoopStart, float p_fLoopEnd, int p_iLoops)
	{
		m_sIdentifier = p_sFilename;

		m_Sequence = LoadSequence(BGM_FILEPATH + p_sFilename + BGM_FILETYPE);

		if (m_Sequence != null)
		{
			m_fTempoInBPM = p_fBPM;
			float tps = TicksPerSecond();
			m_lLoopStartPoint = (long) (p_fLoopStart * tps);
			if (p_fLoopEnd > END_OF_TRACK)
			{
				m_lLoopEndPoint = (long) (p_fLoopEnd * tps);
			}
			else
			{
				m_lLoopEndPoint = END_OF_TRACK;
			}
			m_iLoopCount = p_iLoops;
		}
	}
	
	/**
	 * Creates a new MidiFile object with the given filename, and fills in the
	 * rest of the information if the sequence is succesfully created.
	 * 
	 * @param p_sFilename  The name of the midi file
	 * @param p_fLoopStart The time in seconds where the looping begins
	 * @param p_fLoopEnd   The time in seconds where the looping ends
	 * @param p_iLoops     The number of times the sequence should loop
	 */
	public HWMidiFile(String p_sFilename, float p_fLoopStart, float p_fLoopEnd, int p_iLoops)
	{
		this(p_sFilename, UNSPECIFIED_BPM, p_fLoopStart, p_fLoopEnd, p_iLoops);
	}

	/**
	 * Creates a new MidiFile object with the given filename. Everything else is
	 * given default values.
	 *
	 * @param p_sFilename The name of the midi file
	 */
	public HWMidiFile(String p_sFilename)
	{
		this(p_sFilename, BEGINNING_OF_TRACK, END_OF_TRACK, Sequencer.LOOP_CONTINUOUSLY);
	}

	// Loads a sequence from the file system. Returns null if an error occurs.
	private Sequence LoadSequence(String p_sFilename)
	{
		try
		{
			return MidiSystem.getSequence(getClass().getClassLoader().getResource(p_sFilename));
		}
		catch (InvalidMidiDataException | IOException ex)
		{
			return null;
		}
	}

	// Returns the number of ticks per second
	private float TicksPerSecond()
	{
		float multiplier = 0.0f;
		
		// Resolution is in Ticks(pulses) Per Quarter note
		if (m_Sequence.getDivisionType() == Sequence.PPQ)
		{
			try
			{
				// Get a sequencer as it is needed to get access to the tempo
				// in microseconds per quarter note
				Sequencer sequencer = MidiSystem.getSequencer();
				sequencer.setSequence(m_Sequence);
				
				// Get the number of quarter notes per second as the multiplier
				// using the sequencer's tempo in microseconds per quarter note.
				multiplier = MICROSECONDS_PER_SECOND / sequencer.getTempoInMPQ();
			}
			catch (MidiUnavailableException | InvalidMidiDataException e)
			{
			}
		} // Resolution is in Ticks per Frame
		// 24 frames per second
		else if (m_Sequence.getDivisionType() == Sequence.SMPTE_24)
		{
			multiplier = 24;
		}
		// 25 frames per second
		else if (m_Sequence.getDivisionType() == Sequence.SMPTE_25)
		{
			multiplier = 25;
		}
		// 30 frames per second
		else if (m_Sequence.getDivisionType() == Sequence.SMPTE_30)
		{
			multiplier = 30;
		}
		// 29.97 frames per second
		else if (m_Sequence.getDivisionType() == Sequence.SMPTE_30DROP)
		{
			multiplier = 29.97f;
		}

		return (m_Sequence.getResolution() * multiplier);
	}

	/**
	 * Gets the identifier for this midi file.
	 * 
	 * @return A string representing the HWMidiFile's identifier
	 */
	public String GetIdentifier()
	{
		return m_sIdentifier;
	}

	/**
	 * Returns the GetTempoFactor for use by a Sequencer.
	 *
	 * @param p_fSequencerBPM The sequencer's Beats Per Minute
	 * @return The tempo factor for use with the sequencer
	 */
	public float GetTempoFactor(float p_fSequencerBPM)
	{
		return (m_fTempoInBPM / p_fSequencerBPM);
	}

	/**
	 * Returns the tempo for the midi sequence in beats per minute.
	 *
	 * @return Tempo in Beats Per Minute
	 */
	public float GetTempoInBPM()
	{
		return m_fTempoInBPM;
	}

	/**
	 * Returns the point at which the {@link GetSequence} begins its loop.
	 *
	 * @return Loop's start point
	 */
	public long GetLoopStart()
	{
		return m_lLoopStartPoint;
	}

	/**
	 * Returns the point at which the {@link GetSequence} ends its loop.
	 *
	 * @return Loop's end point
	 */
	public long GetLoopEnd()
	{
		return m_lLoopEndPoint;
	}

	/**
	 * Returns the number of times the looping section should be played before
	 * the {@link GetSequence} finishes playing.
	 *
	 * @return The number of loops to play
	 */
	public int GetLoopCount()
	{
		return m_iLoopCount;
	}

	/**
	 * Returns the {@link GetSequence} for this midi file.
	 *
	 * @return The midi file's {@link GetSequence}
	 */
	public Sequence GetSequence()
	{
		return m_Sequence;
	}
}