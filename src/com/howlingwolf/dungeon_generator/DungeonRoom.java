package com.howlingwolf.dungeon_generator;

import javax.swing.*;

/**
 * Container class for a square portion (room) of a dungeon.
 * <p>
 * New values for sides, consider it like binary. Each side has 3 portions
 * that can be closed (0) or open (1). This allows for 8 posible
 * configurations for each side.
 * <p>
 * Left and Top sides
 * <p>
 * 1 2 3
 * 2
 * 3
 * <p>
 * Right and Bottom sides
 * <p>
 * 	   1
 * 	   2
 * 1 2 3
 * 
 * This makes it possible to have 256 (2^8) different room types as there are
 * 8 total portions that can be closed or open.
 * <p>
 * 1 2 3
 * 8   4
 * 7 6 5
 * <p>
 * In each of these rooms, the central portion does not matter. It will be
 * configurable (have multiple different layouts for each room type) once I
 * get some XML loading classes working.
 * 
 * CURRENTLY UNUSED - REQUIRES REWORKING
 * 
 * @author Dylan Sawyer
 */
public class DungeonRoom
{
	//*************************************************************************
	// Member variables
	//*************************************************************************

	// Variables for each side of the dungeon piece
    private int m_iTopSide;
    private int m_iBottomSide;
    private int m_iLeftSide;
    private int m_iRightSide;

	// The image associated with the "room"
	private ImageIcon m_Image;

	//*************************************************************************
	// Constants
	//*************************************************************************

	 

	/**
	 *
	 */
	public final static int SIDE_0 = 0; // closed, closed, closed
	/**
	 *
	 */
	public final static int SIDE_1 = 1; // closed, closed, open
	/**
	 *
	 */
	public final static int SIDE_2 = 2; // closed, open, closed
	/**
	 *
	 */
	public final static int SIDE_3 = 3; // closed, open, open
	/**
	 *
	 */
	public final static int SIDE_4 = 4; // open, closed, closed
	/**
	 *
	 */
	public final static int SIDE_5 = 5; // open, closed, open
	/**
	 *
	 */
	public final static int SIDE_6 = 6; // open, open, closed
	/**
	 *
	 */
	public final static int SIDE_7 = 7; // open, open, open

	//*************************************************************************
	// Methods
	//*************************************************************************

	// Default constructor
    /**
	 * Empty Constructor. Builds an empty room that is inaccessible.
	 */
	public DungeonRoom()
    {
    	// Set all sides to empty
		m_iTopSide = 0;
		m_iBottomSide = 0; //HORIZONTAL_EMPTY;
		m_iLeftSide = 5;
		m_iRightSide = 5; //VERTICAL_EMPTY;

		// Set image to default
		m_Image = new ImageIcon("0.png");
    }

    // Typical constructor, takes in all the side values and the image name for the dungeon piece
    /**
	 * Creates a room with the specified entrance/exit information and image.
	 * 
	 * @param p_iTop Top entrance/exit information
	 * @param p_iLeft Left entrance/exit information
	 * @param p_iRight Right entrance/exit information
	 * @param p_iBottom Bottom entrance/exit information
	 * @param p_sImageName Filename for the image
	 */
	public DungeonRoom(int p_iTop, int p_iLeft, int p_iRight, int p_iBottom, String p_sImageName)
    {
    	m_iTopSide = p_iTop;
    	m_iLeftSide = p_iLeft;
    	m_iRightSide = p_iRight;
    	m_iBottomSide = p_iBottom;

    	m_Image = new ImageIcon(getClass().getResource("/images/bubble/" + p_sImageName));
        //m_Image = new ImageIcon(getClass().getResource("/images/square/" + p_sImageName));
    }

    // Accessors for the 4 sides
    /**
	 * Gets the entrance/exit value for the left side of the room.
	 * 
	 * @return An int value representing the entrance/exit
	 */
	public int LeftSide()
    {
    	return m_iLeftSide;
    }

	/**
	 * Gets the entrance/exit value for the right side of the room.
	 * 
	 * @return An int value representing the entrance/exit
	 */
	public int RightSide()
    {
    	return m_iRightSide;
    }

    /**
	 * Gets the entrance/exit value for the top side of the room.
	 * 
	 * @return An int value representing the entrance/exit
	 */
	public int TopSide()
    {
    	return m_iTopSide;
    }

    /**
	 * Gets the entrance/exit value for the bottom side of the room.
	 * 
	 * @return An int value representing the entrance/exit
	 */
	public int BottomSide()
    {
    	return m_iBottomSide;
    }

    // Accessor for the dungeon piece's image
    /**
	 * Gets the image for this room.
	 * 
	 * @return An {@link ImageIcon} graphically representing the room 
	 */
	public ImageIcon DungeonImage()
    {
    	return m_Image;
    }

	// Method for displaying the object
	@Override
    public String toString()
    {
    	return "Top: " + m_iTopSide + ", Left: " + m_iLeftSide + ", Right: " + m_iRightSide + ", Bottom: " + m_iBottomSide;
    }
}