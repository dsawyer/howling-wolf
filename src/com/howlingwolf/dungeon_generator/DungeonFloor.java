package com.howlingwolf.dungeon_generator;

import java.util.ArrayList;
import java.util.Random;
import java.awt.Point;

/**
 * Generates a square dungeon filled with {@link DungeonRoom}.
 * <p>
 * Intelligently fills the dungeon so that all the rooms are connected.
 * 
 * CURRENTLY UNUSED - REQUIRES REWORKING
 * 
 * @author Dylan Sawyer
 */
public class DungeonFloor
{
	//*************************************************************************
	// Member variables
	//*************************************************************************
	private int m_iWidth, m_iHeight;

	private Random rng;

	// An array of all the different kinds of "rooms" there can be in the dungeon
	private DungeonRoom[] m_aDungeonRooms;
	// This array represents the layout of "rooms" in the dungeon
	private int[][] m_aiBoard;

	//*************************************************************************
	// Constants
	//*************************************************************************
	private final int NUM_ROOMS = 47;

	// Values for the top and bottom sides
	private final int HORIZONTAL_EMPTY = 0;
	private final int LEFT_DOT = 1;
	private final int RIGHT_DOT = 2;
	private final int LEFT_RIGHT_DOT = 3;
	private final int HORIZONTAL_LINE = 4;
	// Values for the left and right sides
	private final int VERTICAL_EMPTY = 5;
	private final int UPPER_DOT = 6;
	private final int LOWER_DOT = 7;
	private final int UPPER_LOWER_DOT = 8;
	private final int VERTICAL_LINE = 9;

	private final int SOLID_ROOM = 15;

	// New values for sides, consider it like binary. Each side has 3 portions
	// that can be closed (0) or open (1). This allows for 8 posible
	// configurations for each side.
	//
	// Left and Top sides
	// 1 2 3
	// 2
	// 3
	//
	// Right and Bottom sides
	//     1
	//     2
	// 1 2 3
	//
	// This makes it possible to have 256 (2^8) different room types as there are
	// 8 total portions that can be closed or open.
	//
	// 1 2 3
	// 8   4
	// 7 6 5
	//
	// In each of these rooms, the central portion does not matter. It will be
	// configurable (have multiple different layouts for each room type) once I
	// get some XML loading classes working.

	//*************************************************************************
	// Methods
	//*************************************************************************

	// Constructor
    /**
	 * Creates the dungeon based on the given number of rooms wide and high.
	 * 
	 * @param p_iWidth The number of rooms wide
	 * @param p_iHeight The number of rooms high
	 */
	public DungeonFloor(int p_iWidth, int p_iHeight)
    {
    	// Set the width and height of the dungeon
    	m_iWidth = p_iWidth;
    	m_iHeight = p_iHeight;

    	// Create all the different possible Rooms
    	FillDungeonRoomsArray();

    	// Set the size of the board
    	m_aiBoard = new int[m_iHeight][m_iWidth];
        
        // Make a random number generator with a particular seed
    	rng = new Random();
    	rng.setSeed(123456);
    }

	// Helper method that fills the array of dungeon Rooms...
	// Seperated to keep the constructor uncluttered
	private void FillDungeonRoomsArray()
	{
		m_aDungeonRooms = new DungeonRoom[]
		{
			new DungeonRoom(HORIZONTAL_EMPTY, VERTICAL_EMPTY, VERTICAL_EMPTY, HORIZONTAL_EMPTY, "0.png"),
			new DungeonRoom(HORIZONTAL_LINE, UPPER_DOT, UPPER_DOT, HORIZONTAL_EMPTY, "1.png"),
			new DungeonRoom(RIGHT_DOT, VERTICAL_EMPTY, VERTICAL_LINE, RIGHT_DOT, "2.png"),
			new DungeonRoom(HORIZONTAL_EMPTY, LOWER_DOT, LOWER_DOT, HORIZONTAL_LINE, "3.png"),
			new DungeonRoom(LEFT_DOT, VERTICAL_LINE, VERTICAL_EMPTY, LEFT_DOT, "4.png"),
			new DungeonRoom(HORIZONTAL_LINE, VERTICAL_LINE, UPPER_DOT, LEFT_DOT, "5.png"),
			new DungeonRoom(HORIZONTAL_LINE, UPPER_DOT, VERTICAL_LINE, RIGHT_DOT, "6.png"),
			new DungeonRoom(RIGHT_DOT, LOWER_DOT, VERTICAL_LINE, HORIZONTAL_LINE, "7.png"),
			new DungeonRoom(LEFT_DOT, VERTICAL_LINE, LOWER_DOT, HORIZONTAL_LINE, "8.png"),
			new DungeonRoom(HORIZONTAL_LINE, LOWER_DOT, LOWER_DOT, HORIZONTAL_LINE, "9.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, VERTICAL_LINE, VERTICAL_LINE, LEFT_RIGHT_DOT, "10.png"),
			new DungeonRoom(HORIZONTAL_LINE, VERTICAL_LINE, VERTICAL_LINE, LEFT_RIGHT_DOT, "11.png"),
			new DungeonRoom(HORIZONTAL_LINE, UPPER_LOWER_DOT, VERTICAL_LINE, HORIZONTAL_LINE, "12.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, VERTICAL_LINE, VERTICAL_LINE, HORIZONTAL_LINE, "13.png"),
			new DungeonRoom(HORIZONTAL_LINE, VERTICAL_LINE, UPPER_LOWER_DOT, HORIZONTAL_LINE, "14.png"),
			new DungeonRoom(HORIZONTAL_LINE, VERTICAL_LINE, VERTICAL_LINE, HORIZONTAL_LINE, "15.png"),
			new DungeonRoom(RIGHT_DOT, VERTICAL_EMPTY, UPPER_DOT, HORIZONTAL_EMPTY, "16.png"),
			new DungeonRoom(HORIZONTAL_EMPTY, VERTICAL_EMPTY, LOWER_DOT, RIGHT_DOT, "17.png"),
			new DungeonRoom(HORIZONTAL_EMPTY, LOWER_DOT, VERTICAL_EMPTY, LEFT_DOT, "18.png"),
			new DungeonRoom(LEFT_DOT, UPPER_DOT, VERTICAL_EMPTY, HORIZONTAL_EMPTY, "19.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, UPPER_DOT, UPPER_DOT, HORIZONTAL_EMPTY, "20.png"),
			new DungeonRoom(LEFT_DOT, UPPER_LOWER_DOT, VERTICAL_EMPTY, LEFT_DOT, "21.png"),
			new DungeonRoom(LEFT_DOT, UPPER_DOT, LOWER_DOT, RIGHT_DOT, "22.png"),
			new DungeonRoom(RIGHT_DOT, LOWER_DOT, UPPER_DOT, LEFT_DOT, "23.png"),
			new DungeonRoom(RIGHT_DOT, VERTICAL_EMPTY, UPPER_LOWER_DOT, RIGHT_DOT, "24.png"),
			new DungeonRoom(HORIZONTAL_EMPTY, LOWER_DOT, LOWER_DOT, LEFT_RIGHT_DOT, "25.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, UPPER_DOT, UPPER_LOWER_DOT, RIGHT_DOT, "26.png"),
			new DungeonRoom(RIGHT_DOT, LOWER_DOT, UPPER_LOWER_DOT, LEFT_RIGHT_DOT, "27.png"),
			new DungeonRoom(LEFT_DOT, UPPER_LOWER_DOT, LOWER_DOT, LEFT_RIGHT_DOT, "28.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, UPPER_LOWER_DOT, UPPER_DOT, LEFT_DOT, "29.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, UPPER_LOWER_DOT, UPPER_LOWER_DOT, LEFT_RIGHT_DOT, "30.png"),
			new DungeonRoom(HORIZONTAL_LINE, UPPER_LOWER_DOT, UPPER_DOT, LEFT_DOT, "31.png"),
			new DungeonRoom(HORIZONTAL_LINE, UPPER_DOT, UPPER_LOWER_DOT, RIGHT_DOT, "32.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, UPPER_DOT, VERTICAL_LINE, RIGHT_DOT, "33.png"),
			new DungeonRoom(RIGHT_DOT, LOWER_DOT, VERTICAL_LINE, LEFT_RIGHT_DOT, "34.png"),
			new DungeonRoom(RIGHT_DOT, LOWER_DOT, UPPER_LOWER_DOT, HORIZONTAL_LINE, "35.png"),
			new DungeonRoom(LEFT_DOT, UPPER_LOWER_DOT, LOWER_DOT, HORIZONTAL_LINE, "36.png"),
			new DungeonRoom(LEFT_DOT, VERTICAL_LINE, LOWER_DOT, LEFT_RIGHT_DOT, "37.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, VERTICAL_LINE, UPPER_DOT, LEFT_DOT, "38.png"),
			new DungeonRoom(HORIZONTAL_LINE, UPPER_LOWER_DOT, UPPER_LOWER_DOT, LEFT_RIGHT_DOT, "39.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, UPPER_LOWER_DOT, VERTICAL_LINE, LEFT_RIGHT_DOT, "40.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, UPPER_LOWER_DOT, UPPER_LOWER_DOT, HORIZONTAL_LINE, "41.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, VERTICAL_LINE, UPPER_LOWER_DOT, LEFT_RIGHT_DOT, "42.png"),
			new DungeonRoom(HORIZONTAL_LINE, VERTICAL_LINE, UPPER_LOWER_DOT, LEFT_RIGHT_DOT, "43.png"),
			new DungeonRoom(HORIZONTAL_LINE, UPPER_LOWER_DOT, VERTICAL_LINE, LEFT_RIGHT_DOT, "44.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, UPPER_LOWER_DOT, VERTICAL_LINE, HORIZONTAL_LINE, "45.png"),
			new DungeonRoom(LEFT_RIGHT_DOT, VERTICAL_LINE, UPPER_LOWER_DOT, HORIZONTAL_LINE, "46.png")
		};
	}

	// Fills the board with individual "rooms"
	/**
	 * Fills the dungeon with rooms. Checks afterwards to find the largest
	 * group of connected rooms, then empties the rest.
	 */
	public void FillBoard()
	{
		for (int y = 0; y < m_iHeight; y+=1)
		{
			for (int x = 0; x < m_iWidth; x+=1)
			{
				AddRoom(x, y);
			}
		}

		// Remove all the unreachable rooms, keeping only the largest section
		// of connected rooms for the floor
		RemoveUnreachableRooms();
	}

	// Keeps trying different Rooms until it finds one that fits
	private void AddRoom(int x, int y)
	{
		// Pick a Room at random
		int temp = rng.nextInt(NUM_ROOMS);

		boolean added = false;

		while (!added)
		{
			// Check the four sides to see if the Room is good
			if (CheckTopSide(x, y, temp) && CheckLeftSide(x, y, temp) &&
				CheckRightSide(x, y, temp) && CheckBottomSide(x, y, temp))
			{
				added = true;
			}
			else // Otherwise, try a new Room
			{
				temp = rng.nextInt(NUM_ROOMS);
			}
		}

		// Mark the position in the board as a specific Room
		m_aiBoard[y][x] = temp;
	}

	// Helper method that checks the top side of a new Room against
	// either the the bottom side of the Room above it or
	// checks to see if the top side is a horizontal line (if it is against the "wall")
	private boolean CheckTopSide(int x, int y, int temp)
	{
		// If the top side of the new Room matches the bottom side of the Room above it or
		// there are no Rooms above it and the top value is a horizontal line
		if ((y - 1 >= 0 && m_aDungeonRooms[temp].TopSide() == m_aDungeonRooms[m_aiBoard[y-1][x]].BottomSide()) ||
			(y - 1 < 0 && m_aDungeonRooms[temp].TopSide() == HORIZONTAL_LINE))
		{
			return true;
		}

		return false;
	}

	// Helper method that checks the left side of a new Room against
	// either the right side of a Room to the left of it or
	// checks to see if the left side is a horizontal line (if it is against the "wall")
	private boolean CheckLeftSide(int x, int y, int temp)
	{
		// If the left side of the new Room matches the right side of the Room to the left of it or
		// the are no Rooms to the left of it and its left value is a vertical line
		if ((x-1>=0 && m_aDungeonRooms[temp].LeftSide() == m_aDungeonRooms[m_aiBoard[y][x-1]].RightSide()) ||
			(x-1<0 && m_aDungeonRooms[temp].LeftSide() == VERTICAL_LINE))
		{
			return true;
		}

		return false;
	}

	// Helper method that checks the right side of a new Room for either
	// an empty space to the right or checks to see if the right side is
	// a horizontal line (if it is up against the "wall")
	private boolean CheckRightSide(int x, int y, int temp)
	{
		// If the new Room still has places to the left
		if (x + 1 < m_iWidth)
		{
			return true;
		}
		// Otherwise if the new Room is at the far right and it's right side is a vertical line
		else if (m_aDungeonRooms[temp].RightSide() == VERTICAL_LINE)
		{
			return true;
		}

		return false;
	}

	// Helper method that checks the bottom side of a new Room for either
	// an empty space underneath or checks to see if the bottom side is
	// a horizontal line (if it is up against the "wall")
	private boolean CheckBottomSide(int x, int y, int temp)
	{
		// The there are still empty spaces below the new Room
		if (y + 1 < m_iHeight)
		{
			return true;
		}
		// There are no empty spaces below the new Room and its bottom value is a horizontal line
		else if (m_aDungeonRooms[temp].BottomSide() == HORIZONTAL_LINE)
		{
			return true;
		}

		return false;
	}

	// Helper method that goes through all the rooms in the randomised
	// dungeon floor and groups them together if they are connected.
	// All rooms that do not connect to the largest group are changed
	// into solid rooms (no entrance/exit). This gets rid of all the
	// little extra unreachable pockets that appear during the generation.
	private void RemoveUnreachableRooms()
	{
		ArrayList<ArrayList<Point>> groupList;
		groupList = new ArrayList<>();
		int numGroups = 0;

		// Go through all the rooms
		for (int y = 0; y < m_iHeight; y+=1)
		{
			for (int x = 0; x < m_iWidth; x+=1)
			{
				// If the room is solid (no entrance/exit), move to the next room
				if (m_aiBoard[y][x] == SOLID_ROOM)
				{
					continue;
				}

				ArrayList<Point> tempList;
				int leftGroup = -1;
				boolean added = false;

				// If the room is connected to the left, add it
				// to that room's group
				if (HasLeftConnection(x, y))
				{
					for (int i = 0; i < numGroups; i+=1)
					{
						tempList = groupList.get(i);
						if (tempList.contains(new Point(x - 1, y)))
						{
							tempList.add(new Point(x, y));
							leftGroup = i;
							added = true;
							break;
						}
					}
				}

				// If the room is connected on top, add it to the group
				// on top. If it was also connected to the group to the left,
				// add the groups together (add the later numbered group to
				// the earlier numbered group).
				if (HasTopConnection(x, y))
				{
					for (int i = 0; i < numGroups; i+=1)
					{
						tempList = groupList.get(i);
						if (tempList.contains(new Point(x, y - 1)))
						{
							// Add the top group to the left group
							if ((leftGroup != -1) && (leftGroup < i))
							{
								ArrayList<Point> leftlist = groupList.get(leftGroup);
								leftlist.addAll(tempList);
								tempList.removeAll(tempList);
								added = true;
							}
							// Add the left group to the top group
							else if ((leftGroup != -1) && (leftGroup > i))
							{
								ArrayList<Point> leftlist = groupList.get(leftGroup);
								tempList.addAll(leftlist);
								leftlist.removeAll(leftlist);
								added = true;
							}
							else // Add the point to the top group
							{
								tempList.add(new Point(x, y));
								added = true;
								break;
							}
						}
					}
				}

				// If the room was not connected to any others to the left or
				// the top, add it to a new group.
				if (!added)
				{
					groupList.add(new ArrayList<Point>());
					ArrayList<Point> temp = groupList.get(numGroups);
					temp.add(new Point(x, y));
					numGroups+=1;
				}
			}
		}

		// Find the largest group
		int largestGroup = -1;
		int largestSize = 0;
		ArrayList<Point> temp;
		for (int i = 0; i < numGroups; i+=1)
		{
			temp = groupList.get(i);
			if (temp.size() > largestSize)
			{
				largestSize = temp.size();
				largestGroup = i;
			}
		}

		// Something went wrong and no groups were made
		if (largestGroup == -1)
		{
			System.out.println("No groups found.");
		}
		else
		{
			// Remove all other groups, make them into solid rooms
			for (int i = 0; i < numGroups; i+=1)
			{
				// Skip the largest group
				if (i == largestGroup)
				{
					continue;
				}

				temp = groupList.get(i);

				for (int j = 0; j < temp.size(); j+=1)
				{
					Point p = temp.get(j);
					m_aiBoard[p.y][p.x] = SOLID_ROOM;
				}
			}
		}
	}

	// Helper method to see if a room connects to one above it
	private boolean HasTopConnection(int x, int y)
	{
		if (y == 0)
		{
			return false;
		}

		if (m_aDungeonRooms[m_aiBoard[y][x]].TopSide() != HORIZONTAL_LINE)
		{
			return true;
		}

		return false;
	}

	// Helper method to see if a room connects to one to the left
	private boolean HasLeftConnection(int x, int y)
	{
		if (x == 0)
		{
			return false;
		}

		if (m_aDungeonRooms[m_aiBoard[y][x]].LeftSide() != VERTICAL_LINE)
		{
			return true;
		}

		return false;
	}

	//*************************************************************************
	// Accessor Methods
	//*************************************************************************
	/**
	 * Gets the {@link DungeonRoom} at the given coordinates.
	 * 
	 * @param x X coordinate of the room
	 * @param y Y coordinate of the room
	 * @return The {@link DungeonRoom} at the given coordinates
	 */
	public DungeonRoom RoomAt(int x, int y)
	{
		return m_aDungeonRooms[m_aiBoard[y][x]];
	}
}