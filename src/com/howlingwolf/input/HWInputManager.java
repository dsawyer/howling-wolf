package com.howlingwolf.input;

// Java imports
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.ArrayList;
import javax.swing.SwingUtilities;

// Event imports
import com.howlingwolf.event.HWEventManager;
import com.howlingwolf.event.HWEventType;
import com.howlingwolf.event.HWInputEvent;
import com.howlingwolf.event.HWKeyInputEvent;
import com.howlingwolf.event.HWMouseInputEvent;

/**
 * Manages keyboard and mouse input (events). Events are mapped to HWInputActions.
 * 
 * @author Dylan Sawyer
 */ 
public class HWInputManager implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	/**
	 * An invisible cursor.
	 */
	public static final Cursor INVISIBLE_CURSOR = Toolkit.getDefaultToolkit().createCustomCursor(Toolkit.getDefaultToolkit().getImage(""), new Point(0,0), "invisible");

	/**
	 * Mouse code.
	 */
	public static final int MOUSE_MOVE_LEFT = 0;
	/**
	 * Mouse code.
	 */
	public static final int MOUSE_MOVE_RIGHT = 1;
	/**
	 * Mouse code.
	 */
	public static final int MOUSE_MOVE_UP = 2;
	/**
	 * Mouse code.
	 */
	public static final int MOUSE_MOVE_DOWN = 3;
	/**
	 * Mouse code.
	 */
	public static final int MOUSE_WHEEL_UP = 4;
	/**
	 * Mouse code.
	 */
	public static final int MOUSE_WHEEL_DOWN = 5;
	/**
	 * Mouse code.
	 */
	public static final int MOUSE_BUTTON_1 = 6;
	/**
	 * Mouse code.
	 */
	public static final int MOUSE_BUTTON_2 = 7;
	/**
	 * Mouse code.
	 */
	public static final int MOUSE_BUTTON_3 = 8;

	private static final int NUM_MOUSE_CODES = 9;

	// Key codes are defined in java.awt.KeyEvent. Most of the codes (except for
	// some rare ones like "aalt graph") are less than 600.
	private static final int NUM_KEY_CODES = 600;

	private final HWInputAction[] m_aKeyActions = new HWInputAction[NUM_KEY_CODES];
	private final HWInputAction[] m_aMouseActions = new HWInputAction[NUM_MOUSE_CODES];

	private final Point m_pMouseLocation;
	private final Point m_pCentreLocation;
	private final Component m_Component;
	private Robot m_Robot;
	private boolean m_bIsRecentreing;

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates a new InputManager that listens to input from the specified
	 * component.
	 * 
	 * @param p_component The {@link Component} to listen to for input
	 */
	public HWInputManager(Component p_component)
    {
    	m_Component = p_component;
    	m_pMouseLocation = new Point();
    	m_pCentreLocation = new Point();
    }
	
	/**
	 * Sets all the listeners for the HWInputManager. Done outside the constructor
	 * to keep from leaking.
	 */
	public void SetListeners()
	{
		// Register Key and Mouse Listeners
    	m_Component.addKeyListener(this);
    	m_Component.addMouseListener(this);
    	m_Component.addMouseMotionListener(this);
    	m_Component.addMouseWheelListener(this);
		
		// Allow input of the TAB key and other keys normally used for focus traversal
    	m_Component.setFocusTraversalKeysEnabled(false);
	}

    /**
	 * Sets the cursor on this HWInputManager's input component.
	 * 
	 * @param p_cursor
	 */
	public void SetCursor(Cursor p_cursor)
    {
    	m_Component.setCursor(p_cursor);
    }

    /**
	 * Sets whether relative mouse mode is on or not. For relative mouse mode,
	 * the mouse is "locked" in the centre of the screen, and only the change in
	 * mouse movement is measured. In normal mode, the mouse is free to move
	 * about the screen.
	 * 
	 * @param p_mode Relative mode if true
	 */
	public void SetRelativeMouseMode(boolean p_mode)
    {
    	if (p_mode == IsRelativeMouseMode())
    	{
    		return;
    	}

    	if (p_mode)
    	{
    		try
    		{
    			m_Robot = new Robot();
    			RecentreMouse();
    		}
    		catch (AWTException ex)
    		{
    			// Couldn't create robot...
    			m_Robot = null;
    		}
    	}
    	else
    	{
    		m_Robot = null;
    	}
    }

    /**
	 * Returns whether or not relative mouse mode is on.
	 * 
	 * @return True if Relative mode is on
	 */
	public boolean IsRelativeMouseMode()
    {
    	return (m_Robot != null);
    }

	/**
	 * FindMappedActions a HWInputAction to a specific key. The key codes are defined in
 java.awt.KeyEvent. If the key already has a HWInputAction mapped to it, the
 new HWInputAction overwrites it.
	 * 
	 * @param p_action The {@link HWInputAction} to be mapped
	 * @param p_iKeyCode The key that activates the {@link HWInputAction}
	 */
	public void MapToKey(HWInputAction p_action, int p_iKeyCode)
	{
		m_aKeyActions[p_iKeyCode] = p_action;
	}

	/**
	 * FindMappedActions a HWInputAction to a specific mouse action. The mouse codes are defined
 here in HWInputManager. If the mouse action already has a HWInputAction mapped
 to it, the new HWInputAction overwrites it.
	 * 
	 * @param p_action The {@link HWInputAction} to be mapped
	 * @param p_iMouseCode The mouse action that activates the {@link HWInputAction}
	 */
	public void MapToMouse(HWInputAction p_action, int p_iMouseCode)
	{
		m_aMouseActions[p_iMouseCode] = p_action;
	}

	/**
	 * Clears all mapped keys and mouse actions to this HWInputAction.
	 * 
	 * @param p_action The {@link HWInputAction} to clear
	 */
	public void ClearMap(HWInputAction p_action)
	{
		for (int i = 0; i < m_aKeyActions.length; i+=1)
		{
			if (m_aKeyActions[i] == p_action)
			{
				m_aKeyActions[i] = null;
			}
		}

		for (int i = 0; i < m_aMouseActions.length; i+=1)
		{
			if (m_aMouseActions[i] == p_action)
			{
				m_aMouseActions[i] = null;
			}
		}

		p_action.Reset();
	}

	/**
	 * Gets a List of names of the keys and mouse actions mapped to this
	 * {@link HWInputAction}. Each entry in the List is a String.
	 * 
	 * @param p_action The {@link HWInputAction} to check
	 * @return A {@link List} of Strings
	 */
	public List<String> FindMappedActions(HWInputAction p_action)
	{
		ArrayList<String> list;
		list = new ArrayList<>();

		for (int i = 0; i < m_aKeyActions.length; i+=1)
		{
			if (m_aKeyActions[i] == p_action)
			{
				list.add(GetKeyName(i));
			}
		}

		for (int i = 0; i < m_aMouseActions.length; i+=1)
		{
			if (m_aMouseActions[i] == p_action)
			{
				list.add(GetMouseName(i));
			}
		}

		return list;
	}

	/**
	 * Resets all HWInputActions so they appear like they haven't been pressed.
	 */
	public void ResetAllGameActions()
	{
		for (int i = 0; i < m_aKeyActions.length; i+=1)
		{
			if (m_aKeyActions[i] != null)
			{
				m_aKeyActions[i].Reset();
			}
		}

		for (int i = 0; i < m_aMouseActions.length; i+=1)
		{
			if (m_aMouseActions[i] != null)
			{
				m_aMouseActions[i].Reset();
			}
		}
	}

	/**
	 * Gets the name of a Key code.
	 * 
	 * @param keyCode The key code to check
	 * @return The name of the key code
	 */
	public static String GetKeyName(int keyCode)
	{
		return KeyEvent.getKeyText(keyCode);
	}

	/**
	 * Gets the name of a Mouse code.
	 * 
	 * @param mouseCode The mouse code to check
	 * @return The name of the mouse code
	 */
	public static String GetMouseName(int mouseCode)
	{
		switch (mouseCode)
		{
			case MOUSE_MOVE_LEFT: return "Mouse Left";
			case MOUSE_MOVE_RIGHT: return "Mouse Right";
			case MOUSE_MOVE_UP: return "Mouse Up";
			case MOUSE_MOVE_DOWN: return "Mouse Down";
			case MOUSE_WHEEL_UP: return "Mouse Wheel Up";
			case MOUSE_WHEEL_DOWN: return "Mouse Wheel Down";
			case MOUSE_BUTTON_1: return "Mouse Button 1";
			case MOUSE_BUTTON_2: return "Mouse Button 2";
			case MOUSE_BUTTON_3: return "Mouse Button 3";
			default: return "Unkown mouse code " + mouseCode;
		}
	}

	/**
	 * Gets the X position of the mouse.
	 * 
	 * @return X position of the mouse
	 */
	public int GetMouseX()
	{
		return m_pMouseLocation.x;
	}

	/**
	 * Gets the Y position of the mouse.
	 * 
	 * @return Y position of the mouse
	 */
	public int GetMouseY()
	{
		return m_pMouseLocation.y;
	}

	// Uses the Robot class to try and position the mouse in the centre of the screen.
	// Note that use of the Robot class may not be available on all platforms.
	private synchronized void RecentreMouse()
	{
		if ((m_Robot != null) && (m_Component.isShowing()))
		{
			m_pCentreLocation.x = m_Component.getWidth() / 2;
			m_pCentreLocation.y = m_Component.getHeight() / 2;
			SwingUtilities.convertPointToScreen(m_pCentreLocation, m_Component);
			m_bIsRecentreing = true;
			m_Robot.mouseMove(m_pCentreLocation.x, m_pCentreLocation.y);
		}
	}

	// Gets the HWInputAction for a given KeyEvent
	private HWInputAction GetKeyAction(KeyEvent event)
	{
		int keyCode = event.getKeyCode();
		
		return GetKeyAction(keyCode);
	}
	
	/**
	 * Gets the HWInputAction for a given key code.
	 * 
	 * @param keyCode The key code for a particular input
	 * @return The HWInputAction associated with a given key code
	 */
	public HWInputAction GetKeyAction(int keyCode)
	{
		if (keyCode < m_aKeyActions.length)
		{
			return m_aKeyActions[keyCode];
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * Gets the HWInputAction based on the given name.
	 * 
	 * @param name The name representing the HWInputAction
	 * @return The HWInputAction associated with the given name
	 */
	public HWInputAction GetKeyAction(String name)
	{
		for (int i = 0; i < m_aKeyActions.length; i+=1)
		{
			if (m_aKeyActions[i] != null)
			{
				if (m_aKeyActions[i].GetIdentifier().equalsIgnoreCase(name))
				{
					return m_aKeyActions[i];
				}
			}
		}
		
		return null;
	}

	/**
	 * Gets the mouse code for the button specified in this MouseEvent.
	 * 
	 * @param event The {@link MouseEvent} to check
	 * @return The mouse code for the given event
	 */
	public static int GetMouseButtonCode(MouseEvent event)
	{
		switch (event.getButton())
		{
			case MouseEvent.BUTTON1: return MOUSE_BUTTON_1;
			case MouseEvent.BUTTON2: return MOUSE_BUTTON_2;
			case MouseEvent.BUTTON3: return MOUSE_BUTTON_3;
			default: return -1;
		}
	}

	// Gets the HWInputAction for a given MouseEvent (button)
	private HWInputAction GetMouseButtonAction(MouseEvent event)
	{
		int mouseCode = GetMouseButtonCode(event);
		
		return GetMouseButtonAction(mouseCode);
	}
	
	/**
	 * Gets the HWInputAction associated with a given mouse button code.
	 * 
	 * @param mouseCode The code of the HWInputAction
	 * @return The HWInputAction associated with the mouse code
	 */
	public HWInputAction GetMouseButtonAction(int mouseCode)
	{
		if (mouseCode != -1)
		{
			return m_aMouseActions[mouseCode];
		}
		else
		{
			return null;
		}
	}
	
	// Listener interface implementations

	// KeyListener interface...
	@Override
	public void keyPressed(KeyEvent e)
	{
		//WEEventManager.SharedManager().AddEvent(new HWKeyInputEvent(e, HWInputEvent.PRESSED));
		
		HWInputAction action = GetKeyAction(e);
		if (action != null)
		{
			action.Press();
		}
		// Make sure the key isn't processed for anything else
		e.consume();
	}

	// KeyListener interface...
	@Override
	public void keyReleased(KeyEvent e)
	{
		//WEEventManager.SharedManager().AddEvent(new HWKeyInputEvent(e, HWInputEvent.RELEASED));
		
		HWInputAction action = GetKeyAction(e);
		if (action != null)
		{
			action.Release();
		}
		// Make sure the key isn't processed for anything else
		e.consume();
	}

	// KeyListener interface...
	@Override
	public void keyTyped(KeyEvent e)
	{
		// Make sure the key isn't processed for anything else
		e.consume();
	}

	// MouseListener interface...
	@Override
	public void mousePressed(MouseEvent e)
	{
		//WEEventManager.SharedManager().AddEvent(new HWMouseInputEvent(e));
		
		HWInputAction action = GetMouseButtonAction(e);
		if (action != null)
		{
			action.Press();
		}
	}

	// MouseListener interface...
	@Override
	public void mouseReleased(MouseEvent e)
	{
		HWInputAction action = GetMouseButtonAction(e);
		if (action != null)
		{
			action.Release();
		}
	}

	// MouseListener interface...
	@Override
	public void mouseClicked(MouseEvent e)
	{
		// Do nothing
	}

	// MouseListener interface...
	@Override
	public void mouseEntered(MouseEvent e)
	{
		mouseMoved(e);
	}

	// MouseListener interface...
	@Override
	public void mouseExited(MouseEvent e)
	{
		mouseMoved(e);
	}

	// MouseListener interface...
	@Override
	public void mouseDragged(MouseEvent e)
	{
		mouseMoved(e);
	}

	// MouseMotionListener interface...
	@Override
	public synchronized void mouseMoved(MouseEvent e)
	{
		// This event is from recentreing the mouse - ignore it
		if (m_bIsRecentreing && (m_pCentreLocation.x == e.getX()) &&
			(m_pCentreLocation.y == e.getY()))
		{
			m_bIsRecentreing = false;
		}
		else
		{
			int dx = e.getX() - m_pMouseLocation.x;
			int dy = e.getY() - m_pMouseLocation.y;
			MouseHelper(MOUSE_MOVE_LEFT, MOUSE_MOVE_RIGHT, dx);
			MouseHelper(MOUSE_MOVE_UP, MOUSE_MOVE_DOWN, dy);

			if (IsRelativeMouseMode())
			{
				RecentreMouse();
			}
		}

		m_pMouseLocation.x = e.getX();
		m_pMouseLocation.y = e.getY();
	}

	// MouseWheelListener interface...
	@Override
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		MouseHelper(MOUSE_WHEEL_UP, MOUSE_WHEEL_DOWN, e.getWheelRotation());
	}

	// Helper method for dealing with the mouse movements
	private void MouseHelper(int codeNeg, int codePos, int amount)
	{
		HWInputAction action;
		if (amount < 0)
		{
			action = m_aMouseActions[codeNeg];
		}
		else
		{
			action = m_aMouseActions[codePos];
		}

		if (action != null)
		{
			action.Press(Math.abs(amount));
			action.Release();
		}
	}
}