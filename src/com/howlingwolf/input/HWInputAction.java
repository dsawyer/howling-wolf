package com.howlingwolf.input;

/**
 * Causes an action to be made when a key/mouse event is triggered.
 * Abstracts the input from the action and allows the input manager
 * to create its own key bindings.
 * 
 * @author Dylan Sawyer
 */
public final class HWInputAction
{
	//**************************************************************************
	// Member Variables
	//**************************************************************************

	/**
	 * Normal SetBehaviour. The IsPressed() method returns true as long as the key
	 * is held down.
	 */
	public static final int NORMAL = 0;

	/**
	 * Initial press behaviour. The IsPressed() method returns true only after
	 * the key is first pressed, and not again until it has been released and
	 * pressed once more.
	 */
	public static final int DETECT_INITIAL_PRESS_ONLY = 1;

	private static final int STATE_RELEASED = 0;
	private static final int STATE_PRESSED = 1;
	private static final int STATE_WAITING_FOR_RELEASE = 2;

	private int m_iBehaviour;
	private int m_iAmount;
	private int m_iState;
	
	//**************************************************************************
	// Member Variables
	//**************************************************************************
	
	private String m_sName;
	private boolean m_bIsPressed;

	//**************************************************************************
	// Methods
	//**************************************************************************

    /**
	 * Creates a GameAction with normal behaviour.
	 * 
	 * @param name Identifier for the GameAction
	 */
	public HWInputAction(String name)
    {
		this(name, NORMAL);
    }

    /**
	 * Creates a GameAction with the specified behaviour.
	 * 
	 * @param name Identifier for the GameAction
	 * @param behaviour The type of behaviour of the GameAction
	 */
	public HWInputAction(String name, int behaviour)
    {
    	m_sName = name;
    	m_iBehaviour = behaviour;
    	Reset();
    }
	
	/**
	 * Sets the behaviour of a HWInputAction.
	 * 
	 * @param behaviour The new behaviour of the HWInputAction
	 */
	public void SetBehaviour(int behaviour)
	{
		m_iBehaviour = behaviour;
	}
	
	/**
	 * Gets the behaviour of a HWInputAction.
	 * 
	 * @return The HWInputAction's behaviour
	 */
	public int GetBehaviour()
	{
		return m_iBehaviour;
	}
	
	/**
	 * Resets the amount of the HWInputAction, but sets the state to wait for
 release.
	 */
	public void PartialReset()
	{
		m_iState = STATE_WAITING_FOR_RELEASE;
		m_iAmount = 0;
	}

    /**
	 * Gets the name/identifier of this HWInputAction.
	 * 
	 * @return The {@link String} identifier of this HWInputAction
	 */
	public String GetIdentifier()
    {
    	return m_sName;
    }

    /**
	 * Resets this HWInputAction so that it appears like it hasn't been pressed.
	 */
	public void Reset()
    {
    	m_iState = STATE_RELEASED;
    	m_iAmount = 0;
    }

    /**
	 * Taps this HWInputAction. Same as calling Press() followed by Release()
	 */
	public synchronized void Tap()
    {
    	Press();
    	Release();
    }

    /**
	 * Signals that the key was pressed.
	 */
	public synchronized void Press()
    {
    	Press(1);
    }

    /**
	 * Signals that the key was pressed a specified number of times, or that
	 * the mouse moved a specified distance.
	 * 
	 * @param amount Number of times the key was pressed or distance the mouse
	 *				 was moved
	 */
	public synchronized void Press(int amount)
    {
    	if (m_iState != STATE_WAITING_FOR_RELEASE)
    	{
    		m_iAmount += amount;
    		m_iState = STATE_PRESSED;
    	}
    }

    /**
	 * Signals that the key was released.
	 */
	public synchronized void Release()
    {
    	m_iState = STATE_RELEASED;
    }

    /**
	 * Returns whether the key was pressed or not since the last check.
	 * 
	 * @return True if the key was pressed since the last check, false if it
	 *		   was not
	 */
	public synchronized boolean IsPressed()
    {
    	return (Amount() != 0);
    }

    /**
	 * For keys, this is the number of times the key was pressed since it was
	 * last checked.
	 * <p>
	 * For mouse movement, this is the distance moved.
	 * 
	 * @return The number of times the key was pressed since the last check or
	 *		   the distance the mouse moved.
	 */
	public synchronized int Amount()
    {
    	int retVal = m_iAmount;
    	if (retVal != 0)
    	{
    		if (m_iState == STATE_RELEASED)
    		{
    			m_iAmount = 0;
    		}
    		else if (m_iBehaviour == DETECT_INITIAL_PRESS_ONLY)
    		{
    			m_iState = STATE_WAITING_FOR_RELEASE;
    			m_iAmount = 0;
    		}
    	}

    	return retVal;
    }
}