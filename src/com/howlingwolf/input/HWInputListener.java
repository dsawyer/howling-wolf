package com.howlingwolf.input;

// Java imports
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.Component;
import java.awt.Point;

// Event imports
import com.howlingwolf.event.HWEventManager;

/**
 * A class that deals with all the basic input listener interfaces. Relays
 input events to the HWEventManager.
 * 
 * @author Dylan Sawyer
 */
public class HWInputListener  implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener
{
	// Contains the location of the centre of the drawing component and
	// the location of the mouse
	private Point m_pMouseLocation;
	private Point m_pCentreLocation;
	
	// The component that is being used to receive the initial input events
	private Component m_Component;
	
	/**
	 * Creates a new HWInputListener for the given component.
	 * 
	 * @param p_component	The component that listens for input
	 */
	public HWInputListener(Component p_component)
	{
		m_Component = p_component;
    	m_pMouseLocation = new Point();
    	m_pCentreLocation = new Point();
	}
	
	/**
	 * Sets the all the native Java listeners for this listener. This method is
	 * used to avoid leaking in the constructor.
	 */
	public void SetListeners()
	{
		// Register Key and Mouse Listeners
    	m_Component.addKeyListener(this);
    	m_Component.addMouseListener(this);
    	m_Component.addMouseMotionListener(this);
    	m_Component.addMouseWheelListener(this);
		
		// Allow input of the TAB key and other keys normally used for focus traversal
    	m_Component.setFocusTraversalKeysEnabled(false);
	}
	
	//**************************************************************************
	// KeyListener interface
	//**************************************************************************
	@Override
	public void keyPressed(KeyEvent e)
	{
		// Make sure the key isn't processed for anything else
		e.consume();
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		// Make sure the key isn't processed for anything else
		e.consume();
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		// Make sure the key isn't processed for anything else
		e.consume();
	}

	//**************************************************************************
	// MouseListener interface
	//**************************************************************************
	@Override
	public void mousePressed(MouseEvent e)
	{
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		mouseMoved(e);
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		mouseMoved(e);
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		mouseMoved(e);
	}

	//**************************************************************************
	// MouseMotionListener interface
	//**************************************************************************
	@Override
	public synchronized void mouseMoved(MouseEvent e)
	{
		
	}
	
	//**************************************************************************
	// MouseWheelListener interface
	//**************************************************************************
	@Override
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		
	}
}
