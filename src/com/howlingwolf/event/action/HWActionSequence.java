package com.howlingwolf.event.action;

// Java imports
import java.util.ArrayList;

/**
 * This allows a sequence of actions to be run one after another, instead of
 * running them in parallel.
 * 
 * @author Dylan Sawyer
 */
public class HWActionSequence
{
	private final ArrayList<HWAction> m_Actions;
	
	/**
	 * Creates a simple empty object.
	 */
	public HWActionSequence()
	{
		m_Actions = new ArrayList<>();
	}
	
	/**
	 * Creates a sequence with only one action.
	 * 
	 * @param p_action	The action that begins the sequence
	 */
	public HWActionSequence(HWAction p_action)
	{
		m_Actions = new ArrayList<>();
		m_Actions.add(p_action);
	}
	
	/**
	 * Creates a sequence from a list of actions.
	 * 
	 * @param p_actions	A list of actions to performed in sequence
	 */
	public HWActionSequence(ArrayList<HWAction> p_actions)
	{
		m_Actions = new ArrayList<>();
		
		if (p_actions != null)
		{
			for (HWAction action : p_actions)
			{
				m_Actions.add(action.Clone());
			}
		}
	}
	
	/**
	 * Creates a copy of this sequence of actions.
	 * 
	 * @return	A duplicate of this sequence
	 */
	public HWActionSequence Clone()
	{
		return new HWActionSequence(GetActions());
	}
	
	/**
	 * Adds an action to the end of the sequence.
	 * 
	 * @param p_action	The action to be added
	 */
	public void AddAction(HWAction p_action)
	{
		m_Actions.add(p_action);
	}
	
	/**
	 * Removes an actions from the sequence regardless of where it is.
	 * 
	 * @param p_action	The action to be removed
	 */
	public void RemoveAction(HWAction p_action)
	{
		m_Actions.remove(p_action);
	}
	
	/**
	 * Notifies the object that is performing an action that the action has
	 * just begun being processed.
	 */
	public void ActionBegan()
	{
		m_Actions.get(0).GetParent().ActionBegan(m_Actions.get(0));
	}
	
	/**
	 * Notifies the object that was performing the action that it has completed
	 * and removes it from the sequence.
	 */
	public void ActionCompleted()
	{
		m_Actions.get(0).GetParent().ActionCompleted(m_Actions.get(0));
		m_Actions.remove(0);
	}
	
	/**
	 * Gets the sequence of actions.
	 * 
	 * @return	The sequential list of actions
	 */
	public ArrayList<HWAction> GetActions()
	{
		return m_Actions;
	}
	
	/**
	 * Checks to see if there are any actions left in the sequence.
	 * 
	 * @return	True if there is at least one action, false otherwise
	 */
	public boolean HasAction()
	{
		return !m_Actions.isEmpty();
	}
	
	/**
	 * Checks to see if a given action ID matches the action that is currently
	 * being performed.
	 * 
	 * @param p_ID	The action ID being checked
	 * @return		True if the first action matches the ID, false otherwise
	 */
	public boolean IsFirstAction(String p_ID)
	{
		return m_Actions.get(0).GetIdentifier().equals(p_ID);
	}
	
	/**
	 * Updates the first action in the sequence.
	 * 
	 * @param p_lElapsedTime	The time since the last update
	 */
	public void Update(long p_lElapsedTime)
	{
		if (!m_Actions.isEmpty())
		{
			m_Actions.get(0).Update(p_lElapsedTime);
		}
	}
}