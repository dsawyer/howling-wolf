package com.howlingwolf.event.action;

// Event imports
import com.howlingwolf.event.HWEventManager;
import com.howlingwolf.event.HWActionEvent;

/**
 * Basic action. More of an interface/superclass than something that will be
 * instantiated itself.
 * 
 * @author Dylan Sawyer
 */
public abstract class HWAction
{
	private String m_sIdentifier;
	private long m_lDuration;
	private long m_lTime;
	
	private HWActionListener m_Parent;
	
	private boolean m_bRunning;
	
	/**
	 * Variable for clarifying when sending an action began message.
	 */
	public static final boolean ACTION_BEGAN = false;

	/**
	 * Variable for clarifying when sending an action completed message.
	 */
	public static final boolean ACTION_COMPLETED = true;
	
	/**
	 * Simple variable for default durations of actions.
	 */
	public static final long DEFAULT_DURATION = 0;
	
	/**
	 * Creates a new HWAction with the default duration.
	 * 
	 * @param p_sID	The identifier for the action
	 */
	public HWAction(String p_sID)
	{
		this(p_sID, DEFAULT_DURATION);
	}
	
	/**
	 * Creates a new HWAction with the given identifier and duration.
	 * 
	 * @param p_sID		The identifier for the action
	 * @param p_lLength	The duration of the action
	 */
	public HWAction(String p_sID, long p_lLength)
	{
		m_lTime = 0;
		m_sIdentifier = p_sID;
		m_lDuration = p_lLength;
		m_bRunning = false;
	}
	
	/**
	 * Creates an exact copy of the action.
	 * 
	 * @return	A functional duplicate of this particular action
	 */
	public abstract HWAction Clone();
	
	/**
	 * Gets the identifier for the action.
	 * 
	 * @return	The String identifier of this action
	 */
	public final String GetIdentifier()
	{
		return m_sIdentifier;
	}
	
	/**
	 * Sets the identifier for the action.
	 * 
	 * @param p_sID	The new identifier for this action
	 */
	public final void SetIdentifier(String p_sID)
	{
		m_sIdentifier = p_sID;
	}
	
	/**
	 * Gets the duration of the action. This defines the length or time that the
	 * action requires to complete.
	 * 
	 * @return	The duration of this action
	 */
	public final long GetDuration()
	{
		return m_lDuration;
	}
	
	/**
	 * Sets the duration of the action. This defines the length or time that the
	 * action requires to complete.
	 * 
	 * @param p_lLength	The new duration of this action
	 */
	public final void SetDuration(long p_lLength)
	{
		m_lDuration = p_lLength;
	}
	
	/**
	 * Gets the amount of time that the action has been running.
	 * 
	 * @return	The current elapsed time for the action
	 */
	public final long GetTime()
	{
		return m_lTime;
	}
	
	/**
	 * Sets the action's time.
	 * 
	 * @param p_lTime	The new time form which the action will continue
	 */
	public final void SetTime(long p_lTime)
	{
		m_lTime = p_lTime;
	}
	
	/**
	 * Checks to see if the action is currently running.
	 * 
	 * @return	True if the action is running, false otherwise
	 */
	public final boolean IsRunning()
	{
		return m_bRunning;
	}
	
	/**
	 * Starts the action running and sends a message to the event manager
	 * notifying it of the action beginning.
	 */
	public final void Start()
	{
		m_bRunning = true;
		HWEventManager.SharedManager().AddEvent(new HWActionEvent(GetIdentifier(), ACTION_BEGAN));
	}
	
	/**
	 * Pauses the action if it was running, sets it to running again if it was
	 * paused.
	 */
	public final void Pause()
	{
		m_bRunning = !m_bRunning;
	}
	
	/**
	 * Ends the action and notifies the event manager that the action has ended.
	 */
	public final void Stop()
	{
		m_bRunning = false;
		// Send some kind of ActionStopped message
		HWEventManager.SharedManager().AddEvent(new HWActionEvent(GetIdentifier(), ACTION_COMPLETED));
	}
	
	/**
	 * Sets the parent of this action (the object that is using/performing the
	 * action).
	 * 
	 * @param p_parent	The object performing this action
	 */
	public final void SetParent(HWActionListener p_parent)
	{
		m_Parent = p_parent;
	}
	
	/**
	 * Gets the object that is performing this action.
	 * 
	 * @return	The parent of this action
	 */
	public HWActionListener GetParent()
	{
		return m_Parent;
	}
	
	@Override
	public String toString()
	{
		return ("Action: " + GetIdentifier() + "\nDuration: " + GetDuration() + "\n");
	}
	
	/**
	 * Updates the action's time if it is running. Does nothing if the action is
	 * currently paused. If the active time has reached the action's duration,
	 * then the event manager is notified that the action has reached its end.
	 * 
	 * @param p_lElapsedTime	The time since the last update
	 */
	public void Update(long p_lElapsedTime)
	{
		if (IsRunning())
		{
			m_lTime += p_lElapsedTime;

			if (GetTime() > GetDuration())
			{
				HWEventManager.SharedManager().AddEvent(new HWActionEvent(GetIdentifier(), ACTION_COMPLETED));
			}
		}
	}
}