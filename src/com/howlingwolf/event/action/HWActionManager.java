package com.howlingwolf.event.action;

// Java imports
import com.howlingwolf.event.HWEvent;
import java.util.ArrayList;

// Event imports
import com.howlingwolf.event.HWEventListener;
import com.howlingwolf.event.HWEventManager;
import com.howlingwolf.event.HWEventType;
import com.howlingwolf.event.HWActionEvent;

/**
 * Keeps track of current actions. Keeps a list of actions as well as any action
 * sequences that are currently running.
 * 
 * @author Dylan
 */
public class HWActionManager implements HWEventListener
{
	private final ArrayList<HWAction> m_RunningActions;
	private final ArrayList<HWActionSequence> m_ActionSequences;
	
	private static HWActionManager instance;
	
	private HWActionManager()
	{
		m_RunningActions = new ArrayList<>();
		m_ActionSequences = new ArrayList<>();
	}
	
	/**
	 * Gets a handle to the singleton instance of the action manager.
	 * 
	 * @return	The action manager
	 */
	public HWActionManager SharedManager()
	{
		if (instance == null)
		{
			instance = new HWActionManager();
		}
		
		return instance;
	}
	
	/**
	 * Initialises the manager to register it as a listener for action events
	 * with the event manager.
	 */
	public void Init()
	{
		HWEventManager.SharedManager().RegisterListener(HWEventType.ACTION_BEGAN, this);
		HWEventManager.SharedManager().RegisterListener(HWEventType.ACTION_COMPLETED, this);
	}
	
	/**
	 * Updates the action sequences and the other actions that are currently
	 * running.
	 * 
	 * @param p_lElapsedTime	The amount of time since the last update
	 */
	public void Update(long p_lElapsedTime)
	{
		for (HWActionSequence sequence : m_ActionSequences)
		{
			sequence.Update(p_lElapsedTime);
		}
		
		for (HWAction action : m_RunningActions)
		{
			action.Update(p_lElapsedTime);
		}
	}
	
	//**************************************************************************
	// HWEventListener methods
	//**************************************************************************
	
	@Override
	public String GetIdentifier()
	{
		return "Action Manager";
	}
	
	@Override
	public void ProcessEvent(HWEvent p_event)
	{
		HWActionEvent aEvent = (HWActionEvent)p_event;
		
		// If an action has been completed
		if (aEvent.GetType() == HWEventType.ACTION_COMPLETED)
		{
			// Check to see if the action that completed is within a sequence
			for (HWActionSequence sequence : m_ActionSequences)
			{
				if (sequence.IsFirstAction(aEvent.GetActionID()))
				{
					sequence.ActionCompleted();
					
					// If completing the action empties the sequence, remove it
					if (!sequence.HasAction())
					{
						m_ActionSequences.remove(sequence);
					}
				}
			}
			
			// If the action is just a regular one, remove it from the list
			for (HWAction action : m_RunningActions)
			{
				if (action.GetIdentifier().equals(aEvent.GetActionID()))
				{
					action.GetParent().ActionCompleted(action);
					m_RunningActions.remove(action);
				}
			}
		}
		// If an action is just getting started
		else if (aEvent.GetType() == HWEventType.ACTION_BEGAN)
		{
			// Check to see if the action that completed is within a sequence
			for (HWActionSequence sequence : m_ActionSequences)
			{
				if (sequence.IsFirstAction(aEvent.GetActionID()))
				{
					sequence.ActionBegan();
				}
			}
			
			// If the action is just a regular one, remove it from the list
			for (HWAction action : m_RunningActions)
			{
				if (action.GetIdentifier().equals(aEvent.GetActionID()))
				{
					action.GetParent().ActionBegan(action);
				}
			}
		}
	}
}