package com.howlingwolf.event.action;

/**
 * This is just an interface for objects that use actions to implement.
 * 
 * @author Dylan
 */
public interface HWActionListener 
{

	/**
	 * Tells the object that an action began.
	 * 
	 * @param p_action	The action that began
	 */
	public void ActionBegan(HWAction p_action);

	/**
	 * Tells the object that an action has completed. This will typically
	 * involve destroying the object and removing it from the active update
	 * list.
	 * 
	 * @param p_action	The action that completed
	 */
	public void ActionCompleted(HWAction p_action);
}
