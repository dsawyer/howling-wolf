package com.howlingwolf.event;

/**
 * Contains information that an Animation can pass on to a listener when
 * necessary.
 * 
 * @author Dylan Sawyer
 */
public class HWAnimationCompletedEvent implements HWEvent
{
	private final String m_sIdentifier;
	
	/**
	 * Creates a new HWAnimationCompletedEvent for the animation with the given
	 * identifier.
	 * 
	 * @param p_sIdentifier	The ID of the animation
	 */
	public HWAnimationCompletedEvent(String p_sIdentifier)
	{
		m_sIdentifier = p_sIdentifier;
	}
	
	/**
	 * Gets the identifier for the animation that has completed.
	 * 
	 * @return	The ID of the animation
	 */
	public String GetAnimationIdentifier()
	{
		return m_sIdentifier;
	}
	
	@Override
	public HWEventType GetType()
	{
		return HWEventType.ANIMATION_COMPLETED;
	}
}
