package com.howlingwolf.event;

// Java imports
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;

/**
 * Manages all passing of events to registered listeners.
 * 
 * @author Dylan Sawyer
 */
public class HWEventManager
{
	private final EnumMap<HWEventType, ArrayList<HWEventListener>> m_Listeners;
	
	private final ArrayList<ArrayList<HWEvent>> m_EventQueues;
	
	private int m_iActiveQueue;
	
	private static HWEventManager instance;
	
	private static final int NUM_QUEUES = 2;
	
	private HWEventManager()
	{
		m_Listeners = new EnumMap<>(HWEventType.class);
		
		m_EventQueues = new ArrayList<>();
		
		for (int i = 0; i < NUM_QUEUES; i+=1)
		{
			m_EventQueues.add(new ArrayList<HWEvent>());
		}
		
		m_EventQueues.trimToSize();
		m_iActiveQueue = 0;
	}
	
	/**
	 * Returns the Singleton instance of this manager. This provides a universal
	 * access to the manager in a way that keeps only one instance of it at all
	 * times.
	 * 
	 * @return	The instance of this manager
	 */
	public static HWEventManager SharedManager()
	{
		if (instance == null)
		{
			instance = new HWEventManager();
		}
		
		return instance;
	}
	
	/**
	 * Registers a HWEventListener to the list for the type of event it wants to
	 * listen for.
	 * 
	 * @param p_type		The type of event the listener wants
	 * @param p_listener	The listener
	 * @return				True if the listener was registered, false otherwise
	 */
	public boolean RegisterListener(HWEventType p_type, HWEventListener p_listener)
	{
		// If there is not yet a set of listeners for the given event, create
		// a new list for those listeners and add the given listener to it.
		if (!m_Listeners.containsKey(p_type))
		{
			m_Listeners.put(p_type, new ArrayList<HWEventListener>());
			m_Listeners.get(p_type).add(p_listener);
			return true;
		}
		
		for (HWEventListener listener : m_Listeners.get(p_type))
		{
			if (listener.GetIdentifier().equalsIgnoreCase(p_listener.GetIdentifier()))
			{
				// Remove the old, replace with the new
				m_Listeners.get(p_type).remove(listener);
				m_Listeners.get(p_type).add(p_listener);
				// The listener is already registered, so alert the caller that it was
				// not registered
				return false;
			}
		}
		
		// Register the listener if it hasn't been registered before
		m_Listeners.get(p_type).add(p_listener);
		return true;
	}
	
	/**
	 * Removes a listener for the given type, as it is no longer needed.
	 * 
	 * @param p_type		The type of the listener
	 * @param p_listener	The listener
	 * @return				True if the listener was removed, false otherwise
	 */
	public boolean RemoveListener(HWEventType p_type, HWEventListener p_listener)
	{
		if (!m_Listeners.containsKey(p_type))
		{
			// Type doesn't even exist in the map yet
			return false;
		}
		
		for (HWEventListener listener : m_Listeners.get(p_type))
		{
			if (listener.GetIdentifier().equalsIgnoreCase(p_listener.GetIdentifier()))
			{
				// If is registered, remove it
				m_Listeners.get(p_type).remove(p_listener);
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Passes an event to all the listeners that are listening for that type of
	 * event.
	 * 
	 * @param p_event	The event that needs to be passed to the listeners
	 * @return			True if the event was processed, false otherwise
	 */
	public boolean TriggerEvent(HWEvent p_event)
	{
		boolean processedEvent = false;
		
		for (HWEventListener listener : m_Listeners.get(p_event.GetType()))
		{
			listener.ProcessEvent(p_event);
			processedEvent = true;
		}
		
		return processedEvent;
	}
	
	/**
	 * Adds an event to the queue to be processed at the next logical update.
	 * 
	 * @param p_event	The event to be added
	 * @return			False if there are no listeners for the event's type,
	 *					True otherwise
	 */
	public boolean AddEvent(HWEvent p_event)
	{
		// If there area no listeners for the given event type,
		// don't add it to the list to be processed
		if (!m_Listeners.containsKey(p_event.GetType()))
		{
			return false;
		}
		
		// Add to the queue that will be processed
		m_EventQueues.get(m_iActiveQueue).add(p_event);
		return true;
	}
	
	/**
	 * Removes an event from the active queue.
	 * 
	 * @param p_event	The event to be removed from the active queue
	 * @return			True if the event was removed, false otherwise
	 */
	public boolean RemoveEvent(HWEvent p_event)
	{
		if (!m_Listeners.containsKey(p_event.GetType()))
		{
			return false;
		}
		
		return m_EventQueues.get(m_iActiveQueue).remove(p_event);
	}
	
	/**
	 * Removes events of the given type.
	 * 
	 * @param p_type	The type of events to remove
	 * @param p_all		True if all the events of the given type are to be
	 *					removed, False if only the first one
	 * @return			True if the removal was successful, false otherwise
	 */
	public boolean RemoveEvent(HWEventType p_type, boolean p_all)
	{
		boolean success = false;
		
		if (!m_Listeners.containsKey(p_type))
		{
			return false;
		}
		
		ArrayList<HWEvent> eventQueue;
		
		eventQueue = m_EventQueues.get(m_iActiveQueue);
		
		for (HWEvent event : eventQueue)
		{
			if (event.GetType() == p_type)
			{
				eventQueue.remove(event);
				success = true;
				// If you don't want to remove all of the events of the given
				// type, jump out now
				if (!p_all)
				{
					return true;
				}
			}
		}
		
		return success;
	}
	
	/**
	 * Processes all the events in the active queue.
	 * 
	 * @param p_lElapsedTime	The amount of time since the last update
	 * @param p_lMaxTime		The maximum amount of time this update can use
	 *							(currently going unused)
	 * @return					True if all the events in the active queue were
	 *							processed, false otherwise
	 */
	public boolean Update(long p_lElapsedTime, long p_lMaxTime)
	{
		// Swap active queues and clear the new queue after the swap
		int queueToProcess = m_iActiveQueue;
		m_iActiveQueue = (m_iActiveQueue + 1) % NUM_QUEUES;
		m_EventQueues.get(m_iActiveQueue).clear();
		
		// Get the iterator to go through the queue of events
		Iterator<HWEvent> itr = m_EventQueues.get(queueToProcess).iterator();
		
		while(itr.hasNext())
		{
			// Get the next event and remove it from the list
			HWEvent event = itr.next();
			itr.remove();
			
			// Get the listeners for the type of the event currently being
			// processed, and make them process the event
			for (HWEventListener listener : m_Listeners.get(event.GetType()))
			{
				listener.ProcessEvent(event);
			}
			
			// Check to see if a set amount of time has run out
			
			// If so, break out of this loop
		}
		
		// Check to see if the events were all processed
		boolean queueFlushed = m_EventQueues.get(queueToProcess).isEmpty();
		// If not, then add them to the beginning of the other
		if (!queueFlushed)
		{
			System.out.println("Queue was not empty.");
			
			while(!m_EventQueues.get(queueToProcess).isEmpty())
			{
				// Get the last one
				HWEvent event = m_EventQueues.get(queueToProcess).remove(m_EventQueues.get(queueToProcess).size() - 1);
				// Add it to the front of the other queue
				m_EventQueues.get(m_iActiveQueue).add(0, event);
			}
		}
		
		return queueFlushed;
	}
}