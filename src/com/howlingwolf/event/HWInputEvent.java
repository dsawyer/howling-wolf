package com.howlingwolf.event;

/**
 * Basic outline for all the types of input events that can be passed on
 * through the event manager. Key events will be the most prominent for the
 * current engine's goals, but mouse events will be useful for future
 * possibilities.
 * 
 * @author Dylan Sawyer
 */
public class HWInputEvent implements HWEvent
{
	// Value that flags whether the event is a key event or a mouse event
	private final boolean m_bIsKeyEvent;

	/**
	 * Value defining for the type of input event.
	 */
	public static final boolean KEY_EVENT = true;

	/**
	 * Value defining for the type of input event.
	 */
	public static final boolean MOUSE_EVENT = false;

	/**
	 * Value defining for the type of interaction of the input.
	 */
	public static final boolean PRESSED = true;

	/**
	 * Value defining for the type of interaction of the input.
	 */
	public static final boolean RELEASED = false;
	
	/**
	 * Creates a new HWInputEvent defining whether it is mouse or keyboard input.
	 * 
	 * @param p_bIsKey	True if a keyboard event, False if a mouse event
	 */
	public HWInputEvent(boolean p_bIsKey)
	{
		m_bIsKeyEvent = p_bIsKey;
	}
	
	/**
	 * Gets the flag for whether the event is a keyboard or mouse event.
	 * 
	 * @return	True if s a keyboard event, False if a mouse event
	 */
	public boolean IsKeyEvent()
	{
		return m_bIsKeyEvent;
	}
	
	@Override
	public HWEventType GetType()
	{
		return HWEventType.INPUT;
	}
}
