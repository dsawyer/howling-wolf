package com.howlingwolf.event;

// Java imports
import java.awt.event.MouseEvent;

/**
 * Contains the information about any kind of mouse input.
 * 
 * @author Dylan Sawyer
 */
public class HWMouseInputEvent extends HWInputEvent
{
	private final long m_lTimestamp;
	private final MouseEvent m_MouseEvent;
	
	/**
	 * Creates a new HWMouseInputEvent that represents a mouse action.
	 * 
	 * @param event	The MouseEvent (mouse action) being performed
	 */
	public HWMouseInputEvent(MouseEvent event)
	{
		// Mouse event, mark it as such
		super(MOUSE_EVENT);
		
		m_MouseEvent = event;
		m_lTimestamp = System.currentTimeMillis();
	}
	
	/**
	 * Gets the associated MouseEvent (native Java mouse interface).
	 * 
	 * @return	The MouseEvent being performed
	 */
	public MouseEvent GetEvent()
	{
		return m_MouseEvent;
	}
	
	/**
	 * Gets the time at which the event took place.
	 * 
	 * @return	The time (in system milliseconds) when the event took place
	 */
	public long GetTimestamp()
	{
		return m_lTimestamp;
	}
}
