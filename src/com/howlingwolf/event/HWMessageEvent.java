package com.howlingwolf.event;

// Java imports
import java.util.ArrayList;

/**
 * The contents of a message.
 * 
 * @author Dylan Sawyer
 */
public class HWMessageEvent implements HWEvent
{
	private final ArrayList<String> m_Message;
	
	/**
	 * Creates a new HWMessageEvent that is used to pass a message from one
	 * object to another.
	 * 
	 * @param p_message	The message that needs to be passed
	 */
	public HWMessageEvent(ArrayList<String> p_message)
	{
		m_Message = p_message;
	}
	
	/**
	 * Creates a new HWMessageEvent that is used to pass a message from one
	 * object to another. This one passes only a single String.
	 * 
	 * @param p_sMessage	The message that needs to be passed
	 */
	public HWMessageEvent(String p_sMessage)
	{
		m_Message = new ArrayList<>();
		m_Message.add(p_sMessage);
	}
	
	/**
	 * Gets the message.
	 * 
	 * @return	An ArrayList of Strings that form the message
	 */
	public ArrayList<String> GetMessage()
	{
		return m_Message;
	}
	
	@Override
	public HWEventType GetType()
	{
		return HWEventType.MESSAGE;
	}
}
