package com.howlingwolf.event;

/**
 * Empty interface that any HWEvent must implement to designate itself as an
 * HWEvent. It must provide it's given HWEventType.
 * 
 * @author Dylan Sawyer
 */
public interface HWEvent
{
	/**
	 * The type of the event.
	 * 
	 * @return The type of the event
	 */
	public HWEventType GetType();
}
