package com.howlingwolf.event;

/**
 * All the different types of events.
 * 
 * @author Dylan Sawyer
 */
public enum HWEventType
{

	/**
	 * A message has been passed.
	 */
	MESSAGE,

	/**
	 * An animation has completed.
	 */
	ANIMATION_COMPLETED,

	/**
	 * An action has begun.
	 */
	ACTION_BEGAN,

	/**
	 * An action has completed.
	 */
	ACTION_COMPLETED,

	/**
	 * An input has been received.
	 */
	INPUT,

	/**
	 * A transition has been encountered.
	 */
	TRANSITION
}
