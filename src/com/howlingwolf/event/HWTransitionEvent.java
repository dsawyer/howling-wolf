// Engine imports
package com.howlingwolf.event;

/**
 *
 * @author Dylan Sawyer
 */
public class HWTransitionEvent implements HWEvent
{
	@Override
	public HWEventType GetType()
	{
		return HWEventType.TRANSITION;
	}
}
