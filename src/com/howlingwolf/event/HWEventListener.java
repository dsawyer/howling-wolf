package com.howlingwolf.event;

/**
 * Interface that provides the basic interface for processing events.
 * 
 * @author Dylan Sawyer
 */
public interface HWEventListener
{

	/**
	 * This provides a simple interface that takes in the event, sorts out what
	 * type it is, and then uses the information it provides to do whatever the
	 * listener needs to do.
	 * 
	 * @param p_event	The HWEvent subclass that needs to be processed
	 */
	public void ProcessEvent(HWEvent p_event);
	
	/**
	 * Gets the identifier of listener. As most/all listeners will be some form
	 * of subclass of the HWScene or HWSceneObject classes, this is a little
	 * redundant. It merely provides an interface for the event manager to use
	 * without having to know about the scene stuff.
	 * 
	 * @return	The ID of the listener
	 */
	public String GetIdentifier();
}