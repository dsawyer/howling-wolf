package com.howlingwolf.event;

/**
 * An event that revolves around the basic actions.
 * 
 * @author Dylan Sawyer
 */
public class HWActionEvent implements HWEvent
{
	private final HWEventType m_Type;
	private final String m_sID;
	
	/**
	 * Creates a new HWActionEvent, which contains information on whether the
	 * action with the given ID has begun or completed.
	 * 
	 * @param p_sID			The ID of the action
	 * @param p_bCompleted	True if the action has completed, false otherwise
	 */
	public HWActionEvent(String p_sID, boolean p_bCompleted)
	{
		if (p_bCompleted)
		{
			m_Type = HWEventType.ACTION_COMPLETED;
		}
		else
		{
			m_Type = HWEventType.ACTION_BEGAN;
		}
		
		m_sID = p_sID;
	}
	
	/**
	 * Gets the ID of the action that is involved with the event.
	 * 
	 * @return	The identifier for the action that has begun or completed
	 */
	public String GetActionID()
	{
		return m_sID;
	}
	
	@Override
	public HWEventType GetType()
	{
		return m_Type;	
	}
}
