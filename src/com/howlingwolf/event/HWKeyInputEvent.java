package com.howlingwolf.event;

// Java imports
import java.awt.event.KeyEvent;

/**
 * Contains the information about a key being pressed or released.
 * 
 * @author Dylan Sawyer
 */
public class HWKeyInputEvent extends HWInputEvent
{
	private final long m_lTimestamp;
	private final KeyEvent m_KeyEvent;
	private final boolean m_bPressed;
	
	/**
	 * Creates a new HWKeyInputEvent that represents a button being pressed.
	 * 
	 * @param event		The KeyEvent (key) being pressed/released
	 * @param pressed	True if the key is being pressed, False if released
	 */
	public HWKeyInputEvent(KeyEvent event, boolean pressed)
	{
		// Key event, mark it as such
		super(KEY_EVENT);
		
		m_KeyEvent = event;
		m_lTimestamp = System.currentTimeMillis();
		m_bPressed = pressed;
	}
	
	/**
	 * Gets the KeyEvent (native Java key interface) for the key tied to this
	 * input event.
	 * 
	 * @return	The key being pressed/released
	 */
	public KeyEvent GetEvent()
	{
		return m_KeyEvent;
	}
	
	/**
	 * Gets the time at which the event took place.
	 * 
	 * @return	The time (in system milliseconds) when the event took place
	 */
	public long GetTimestamp()
	{
		return m_lTimestamp;
	}
	
	/**
	 * Gets the flag for whether the key was pressed or released.
	 * 
	 * @return	True if the key was pressed, False if released
	 */
	public boolean GetPressed()
	{
		return m_bPressed;
	}
}
